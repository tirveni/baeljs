/*
	 2017-12-25
	 xutils.js, JS Utilities for BAEL

    This file is part of BAEL
    Copyright (C) 2017,  Tirveni Yadav,Amit Bondwal

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	 Extracted standard utilities(dom,time,date,progress,pagination) from bael.js

*/


// ********   Constant Variables ********

//Progress Bar for waiting
var $progress_clock = "<div class='preloader-wrapper active red-text'>"
      							+ "<div class='circle-clipper left'>"
        									+ "<div class='circle'></div>"
    								+ " </div>I"
								+ "</div>"; 

var $progress_bar = "<div class='progress'><div class='indeterminate'></div></div>";

//REST Page Current Div
var $rpage_current = "#api_page_current";

var $g_sign_pass =" &#x2611 "
var $g_sign_fail =" &#x2612 ";

var $zebra_a = " " + "  " + " ";
var $zebra_b = " " + " grey lighten-3 " + " ";

//Used in Bill txns, Change colors for debugging
var $btx1_color = " white blue-text ";
var $btx2_color = " white black-text  ";
var $btx3_color = " white blue-text ";
var $btx4_color = " white accent-1 ";

var $div_entity_more			= '#x_extra_more';
var $div_entity_more_below = '#x_extra_more_below';
var $rest_msg_out 	= "#rest_messages";

//Constants Order Type
var $c_order_cn		= 'CREDIT_NOTE';
var $c_order_dn		= 'DEBIT_NOTE';
var $c_order_disc		= 'DISCOUNT';
var $c_order_po		= 'PURCHASE_ORDER';
var $c_order_bo		= 'BIZORDER';

//Constants Appid
var $c_appid_retail			= 'RETAIL';
var $c_appid_restaurant 	= 'RESTAURANT';
var $c_appid_acquire 		= 'ACQUIRE';
var $c_appid_foodtakeout 	= 'FOODTAKEOUT';

var $c_ajax_timeout_milisec			= 10000;//MiliSeconds

var $mst_boolean_dropbox 		=  "<option value='' disabled >Choose Yes / No</option>"
      									+ "<option value='1'>Yes</option>"
      								+ "<option value='0' selected>No  &#9745; </option>";


/****  Error Handling Functions *****/

/*
* Empty div Rest_Message
* FX: bx_empty_rest_messages
* 
*/
function g_empty_rest_messages()
{
	var $div_mr = "#rest_messages";
	$($div_mr).empty();
		
}

/*
*
* Removes the Toast Class(popups)
* FX: bx_toast_empty
*/
function bx_toast_empty($div_id)
{
	$(".toast").remove();
}


/*
* FX:bx_display_error(fail_or_success,msg,error_code)
* Displays: Success if first argument is greater than zero.
*
*/
function bx_display_error($make_empty,$message,$error_code)
{
	
	var $div_err = "#rest_messages";   
	if($make_empty > 0 && $message)
	{
			$($div_err).empty();
 			$($div_err).append($message);	
 			return ;	
		
	}
	else if($make_empty > 0)
	{
			$($div_err).empty();
 			return ;	
	}
	else
	{
			$($div_err).empty();
			var $str_err = "<span class='red-text'>&nbsp; &#9746 "+ $message +"."
 								+ "</span>";
			Materialize.toast($str_err, 3000);
 			$($div_err).append($str_err);	
	}
	
	return ;
}

/*
* FX: bx_extract_err_msg(xhr,$str_fn_name)
* Returns: Error Msg

*/
function bx_extract_err_msg(xhr,$fn)
{
	var $err_msg = "xhr: unknown error " + $fn;	
	var $jr;

	if (xhr.readyState == 0) 
	{
         $err_msg = "Network Connection Failure.";
   }
	else if(xhr.responseText)
	{
		$jr 		= JSON.parse(xhr.responseText) ;
		if($jr.error)
		{
			$err_msg = $jr.error;
		}  
		
	}  

	return $err_msg;
									
}

/*
* FX: gx_max_length(max_length)
* Adds ellipsis after truncated string.
* 
*/

function gx_max_length(max_length)
{
    return function truncateToNearestSpace(idx, text)
    {
        // this may chop in the middle of a word
        text.trim();
        var $truncated = text.substr(0, max_length);
        $truncated = $truncated + "...";

		  return $truncated;
    }
}

/*
* Integer Increment Function for a Div
* @returns: Integer
* @params: div:#ID
* Used to Order-Item increment in Cart
* FX:g_incrementInput
*/
function g_incrementInput(id)
{
    var $y = parseInt(($( "#"+id).text()))+1;
    $( "#"+id).css( "backgroundColor", "yellow" );
    //var $y1 = $y + 1;
    $( "#"+id).text($y);
    $( "#"+id).css( "backgroundColor", "red" );
	 return $y;
}

/*
* Integer Decrement unction for a Div
* @returns: Integer
* @params: div:#ID
* Used to Order-Item increment in Cart
* FX: g_decrementInput
*
*/
function g_decrementInput(id)
{
    var $y = parseInt(($( "#"+id).text()))-1;
    $( "#"+id).text($y);
    //$( "#"+id).css( "backgroundColor", "yellow" );
    return $y;
}

/*
* FX: x_getform_input: to fetch all values from a form
* @params: form div id
* returns: hash. 
* <form id='divx'></form>
*/

function x_getform_input($div_formid)
{
		var $input_h = {};
		$.each($($div_formid).serializeArray(), function(i, xfield) 
		{
		    $input_h[xfield.name] = xfield.value;
		});

		return $input_h;
}


/*
* FX:progress_append(div_id,message,retry_function) 
* @params: div:#id, Message, Retry_function
*  display Progress Bar to a div, with a Message.
* 
*/
function progress_append($div_id,$message,$function_retry)
{
	var $msg = $message || "Loading...";
	var $fnx = "";
	if($function_retry)
	{
			$fnx = "  <span class='btn red' onClick='"+ $function_retry +"' > Retry </span>";
	}	
	
	bx_progress_empty($div_id);
	var $msg_progress = "<span class='msg_progress red-text'>"
		+ $msg + $progress_clock + $fnx + "</span>";
	
	$($div_id).append($msg_progress);

}

/*
*
* FX: bx_progress_empty
* Removes Progress Animation from the Div.
*
*/
function bx_progress_empty($div_id)
{
	$($div_id).find('.msg_progress').remove();	
}



/*
* Set Title of The Page
* FX: bx_set_title(string)
*/

function bx_set_title($in_value)
{
	if($in_value)
	{
		document.title = $in_value;
	}
	return $in_value;		
}

/*** Date and Time ***/

/*
* FX: g_choosen_date(date)
*
* Returns: choosen date
* Use with DatePicker.js, like This
* <input type="hidden"    class='xindate' />
* <input type="date" id='in_date' class='xindate datepicker' size='12'/>
*
*/
function g_choosen_date($xinput)
{
	var $xdate;
	if($xinput)
	{
		//$xdate = $(".xindate").data($xinput);
		//How to handle both: todo.
		$xdate = $(".xindate").val($xinput);
	}	
	else
	{
		$xdate = $("#in_date").val(); 
	}
	
	return $xdate;
	
}

/*
* FX: g_currenttime(): GET Current Time from AJAX Request
* 
* Get Epoch of Currenttime
* Returns time Epoch of Server through REST API
* Hits Ajax Call: /time and puts in the #current_time
* 
*/
function	g_currenttime() 
{
	return	$.getJSON( "/time", 
			function( time ) 
				{
					$('#current_time').html( time );
				})
}

/*
* FX:g_current_date Get Current Date from DOM
*
*/

function g_current_date()
{
	var $current_date = $("#current_date").val();
	
	return $current_date;
	
}

/*
* FX: g_time_remove_colon: Removes : from a Time 
* Returns: HH:MM::SS to HHmmSS
*
*/

function g_time_remove_colon($in_time)
{
		var $time_u = $in_time.replace(/:/g,"");
		return $time_u;	
}

/*
* FX: g_dates_increasing(date_a,date_b)
* 
* g_dates_increasing(lower,greater)
* lower,greater: yyyy-mm-dd
* 
*/
function g_dates_increasing($dt_a,$dt_b)
{
		
	var $in_x = $dt_a + "," + $dt_b;
	//Materialize.toast($in_x,5000);

	var $a_ymd;var $b_ymd;	
	if($dt_a && $dt_b)
	{
		$a_ymd = $dt_a.replace(/-/g,"");
		$b_ymd = $dt_b.replace(/-/g,"");
	}		
	var $o_x = $a_ymd + "," + $b_ymd;	
	//Materialize.toast($o_x,5000);


	var $is_ok;
	if($a_ymd > $b_ymd)
	{
		$is_ok = 0;
		Materialize.toast("Dates should be in increasing order.",5000);
	}	
	else
	{
		$is_ok = 1;
		//Materialize.toast("Dates are Increasing",3000);		
	}		


	return $is_ok;	
	
}


/*
* FX: g_datepicker_init
*
* DatePicker INIT.
* Materialize
*/
function g_datepicker_init()
{

 $('.datepicker').pickadate
 ({
    selectMonths: 	true,  	
    selectYears: 		3,       
    format: 			'yyyy-mmm-dd',
    formatSubmit: 	'yyyy-mm-dd',
 });
  	
}


/*
* FX: g_timepicker_init()
*
* TimePicker INIT.
* 
* Materialize
*/
function g_timepicker_init()
{
		$('.timepicker').pickatime
		({
  				format: 'h:i A',
  				formatLabel: '<b>h</b>:i <!i>a</!i>',
		      interval: 10,
  				formatSubmit: 'HH:i',
  				closeOnSelect: true,
  				closeOnClear: true,
  				hiddenName: true
 		}); 
 	
}

/*
* Collapsible Init
* FX: g_collapse_init()
*
*/
function g_collapse_init()
{
  $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : true,
    });
  });
        
}

/*
*
* FX: gx_input_fields_init
* If you are having trouble with the labels overlapping prefilled content
*/
function gx_input_fields_init()
{
	Materialize.updateTextFields();
}

/*
* FX: g_drop_true_false
*
* Uses this instead of Checkbox.
* Disable/Active
* Checkbox input doesn't work in Ajax with materialize for some reason.
* @params: input_field_name,current_status,String to Display
*
*/
function g_drop_true_false($input_name,$default_val,$input_choose_str)
{
	var $str_option;
	if($default_val == 't' || $default_val == '1')
	{
		$str_option ="  <option value='0' >Disable</option>"
		    +"  <option value='1'  selected >Active</option>"
	}
	else if($default_val == 'f' || $default_val == '0')
	{
		$str_option ="  <option value='0' selected >Disable</option>"
		    +"  <option value='1'  >Active</option>"
	}
	else
	{
		$str_option ="  <option value='0' >Disable</option>"
		    +"  <option value='1'  >Active</option>"
	}
	
	if(!$input_choose_str)
	{
		$input_choose_str = 'Enable/Disable';
	}
	
	var $x_drop = 
	"<select class='browser-default' name='"+ $input_name +"'>"
		    +"  <option value='' disabled >"+ $input_choose_str +"</option>"
		    + $str_option
    +"</select>";
						    
	return $x_drop;						    
	
}


/*
* Print a Div. (HTML Print)
* FX: g_print_div(print_id)
*
* Input: ID of Div (Default: '#idprint')
* 
*/
function g_print_div(in_div_id) 
{

     div_id = in_div_id || 'idprint';
 var contents = document.getElementById(div_id).innerHTML;
            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            frame1.style.position = "absolute";
            frame1.style.top = "-1000000px";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html>');

        frameDoc.document.write
("<link rel='stylesheet' type='text/css' href='/static/css/print.css') %]' />");

        frameDoc.document.write('<body >');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>')
        //    frameDoc.document.write('<html><head><title>DIV Contents</title>');
        //    frameDoc.document.write('</head><body>');
        //    frameDoc.document.write(contents);
        //    frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                document.body.removeChild(frame1);
            }, 500);
            return false;

          
}

/*** Pagination Functions ****/

/*
* FX:g_rest_pgx_group
*
* Pagination Functions 
* GroupID,Group Name
*/
function g_rest_pgx_group($inpage)
{
	var $in_groupid;
	var $in_groupname;
	var $prev_groupname;
	var $next_groupname;

	var $prev_page = $inpage - 1;
	var $next_page = $inpage + 1;
				
	var $div_p_group_vcount_prev 		= "#p_group_name_"+$prev_page;
	var $div_p_group_vcount_prev_val = $( $div_p_group_vcount_prev ).val();
	var $prev_groupname 					= $div_p_group_vcount_prev_val;

	
	var $div_p_group_vcount_next = "#p_group_name_"+$next_page;
	var $div_p_group_vcount_next_val = $( $div_p_group_vcount_next ).val();
	var $next_groupname = $div_p_group_vcount_next_val;
				
	var $div_p_group_vcount = "#p_group_idcount_"+$inpage;
	var $div_p_group_vcount_val = $( $div_p_group_vcount ).val();
	$in_groupid = $div_p_group_vcount_val;
		
	var $div_p_group_ncount = "#p_group_name_"+$inpage;
	var $div_p_group_ncount_val = $( $div_p_group_ncount ).val();
	$in_groupname = $div_p_group_ncount_val;

	$( "#pgx_name_display_current" ).empty("");	
	$( "#pgx_name_display_current" ).append($in_groupname);
	
	$( "#pgx_name_display_prev" ).empty("");
	$( "#pgx_name_display_prev" ).append($prev_groupname);

	$( "#pgx_name_display_next" ).empty("");
	$( "#pgx_name_display_next" ).append($next_groupname);
	
	return $in_groupid;
}

/*
* FX: bgx_current_display(string)
*
* @name bgx_current_display
* Change the Middle (Main) string of Pgx to input.
* @params: string
*/

function bgx_current_display($in_str)
{
	if($in_str)
	{
		$( "#pgx_name_display_current" ).empty("");	
		$( "#pgx_name_display_current" ).append($in_str);
	}
	return $in_str;
}

/*
* FX: bgx_display_pgx
*
* Display Pagination Div.
* 
* @params: api method
* 
*/

function bgx_display_pgx($v_method)
{
	var $dvx;
	$dvx  	= 
	 				" <div>" 
					+ "<input id='fnx_rest' type='hidden' value=' " 
						+ $v_method 
						+" ' />"
					+ "<input id='fnx_rest_item_count' type='hidden' value='10' /> </div>"

	 				+ " <ul class='row button-group' id='rest_pagination'> "
	 
	 				+  "<input id='api_page_current' type='hidden' value='1'>"
	 				
   			   +	"<li class='col s4 btn white blue-text waves-effect " 
    	  				+	" waves-light page_back z-depth-1'>"
 						+ "<b>"
    	  				+ "&#8592; &nbsp; " 						
 						+ "<span id='pgx_name_display_prev' class='hide-on-med-and-down'></span>"
    	  				+ "</b>"
    	  				+  "</li>"

   				+ "	<li class='col s4 btn white green-text' >"
			 			+	"	<b>"	
			 			+	"	<span id='rest_pagination_text'></span>"
			 			+	"	<span id='pgx_name_display_current'></span>"
			 			+	"		</b>"
			 			+	"</li> "
			 		 
		 			+ "<li class='col s4 btn white blue-text waves-effect " 
		    	  		+ " waves-light page_ahead z-depth-1'> "
		    	  		+ "<b>"
		    	  		+ "<span  id='pgx_name_display_next' class='hide-on-med-and-down'></span> "
		    	  		+ "&nbsp; &#8594;"  
		    	  		+ "</b>"
    			  		+ "</li> "
    			  		
    			  + "<ul>"; 

	return $dvx; 

 
}
/*
* FX: bx_pgx_btns_init()
*
* Pgx_init: page_back,page_ahead are initialized
* 
*/
function bx_pgx_btns_init()
{

		//Back <> Forward btns Pagination
		$('body').on('click', '.page_back', function () 
		{
		 	rpage_decrement();
		});
		
		$('body').on('click', '.page_ahead', function () 
		{
			rpage_increment();
		
		});	 

}

/*
* FX: bgx_page_nf_pg: display page_number in middle.
* Mustache Template PGX
* Page number is displayed in middle.
* next and previous are used for fwd and back.
* @params: ({page,next,previous},method)
* Careful: $method has to be defined in the Params Not in Hash
* 
*/
function bgx_page_nf_pg($h_sx,$method)
{
	
	var $mst_pgx_np_method = "<div class='card row yellow lighten-3'>"
						+ "<div class='row'>"

							+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=" + $method +"('1','{{previous}}');"							
									+ "> "
									+ "<< </span>"

							+ "<span class='btn col l4 m4 s6 red'>"
								+ " {{#page}} {{page}} {{/page}}" 
								+ " {{^page}} 1 {{/page}}"
							+ "	</span>"
							
							+ "<input id='current_page' "
								+ " {{#page}} value='{{page}}' {{/page}}" 
								+ " {{^page}} value='1' {{/page}}"
								+ " type='hidden' />"

							+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=" + $method +"('1','{{next}}');" 	 
									+ "> "
									+ ">> </span>"
	  						+ "</div>"
	  					+"</div>";
	  					
  
	var $div_pgx = Mustache.to_html($mst_pgx_np_method, $h_sx);
	return $div_pgx;				
 					

}

/*
* FX: bgx_pagination
* @params: ({page,next,previous,add_div},method)
* Page number is displayed in middle.

* Method should run like this: x_method(div_add,url)
* next and previous are used for fwd and back.
*
* Careful: $method has to be defined in the Params Not in Hash
* 
*/
function bgx_pagination($h_sx,$method)
{
	
	var $mst_pgx_np_method = "<div class='panel' id='pgx_pagination'>"
						+ "<div class='row white'>"

							+ "<span class='btn col l4 m4 s3 yellow lighten-3 black-text' "
									+ 	"onClick=" + $method +"('{{add_div}}','{{previous}}');"							
									+ "> "
									+ "<< </span>"

							+ "<span class='btn col l4 m4 s6 red'>"
								+ " {{#page}} {{page}} {{/page}}" 
								+ " {{^page}} 1 {{/page}}"
							+ "	</span>"
							
							+ "<input id='current_page' "
								+ " {{#page}} value='{{page}}' {{/page}}" 
								+ " {{^page}} value='1' {{/page}}"
								+ " type='hidden' />"

							+ "<span class='btn col l4 m4 s3  yellow lighten-3 black-text' "
									+ 	"onClick=" + $method +"('{{add_div}}','{{next}}');" 	 
									+ "> "
									+ ">> </span>"
	  						+ "</div>"
	  					+"</div>";
	  					
  
	var $div_pgx = Mustache.to_html($mst_pgx_np_method, $h_sx);
	return $div_pgx;				
	  					
}

/**** Pagination Ends ****/

/********* FX:: City Utils ******/
/*
* FX: gx_add_city 
*
*/
function gx_add_city($div_id)
{
	
	var $country_code = $("#countrycode").val() || $("#icountry").val();
	Materialize.toast($country_code,10);
	$($div_id).append($progress_clock);
	
	var $base_url     = "/g/address/states/1?countrycode="+$country_code;	
	
	if($country_code && $base_url)
	{
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"GET",
						dataType: 	"json",
					  error: function (xhr, status, error) 
			 		  {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_id).empty();					
			        },
					 success: function( xdata)
						{
							$($div_id).empty();	
							gx_cityform(xdata.states,$div_id);
						},
				}	
		);
	}
	else
	{
		Materialize.toast("Country is not available",10);
	}
	
}

/*
* FX: gx_cityform: Creates Form for new city. States in Dropdown Box.
*
*
*/
function gx_cityform($xstates,$div_id)
{

	var $form_city_hide_btn		= "  onClick=bx_div_class_hide('" + ".add_city" + "'); ";
	var $form_city_add_btn		= "  onClick=g_save_city('" + "#add_new_city" + "'); ";
	var $form_city_show_state	= "  onClick=gx_new_state_flip('" + ".new_state_in" + "'); ";

	var $form_close_btn = "<label "  + $form_city_hide_btn
	  								+ "class='btn red'>X</label>";

	var $mst_form_heading	= "<div class='row amber lighten-4'>"
									+ "<div class='col  l8 m8 s8 center-align city_label'>{{new_city_label}}</div>"
									+ "<div class='col  l4 m4 s4 right-align'>"
											+ $form_close_btn 
									+ "</div>"
								+ "</div>";		
	
	var $in_city =    "<div class='col l6 m6 s12   old_state_in hide_if_nostates' >"
									+ "<span>If state is not available in the drop down </span>"
							+ "</div>"
							+ "<div class='col l6 m6 s12 old_state_in hide_if_nostates' >"
	  							+ "<label "+ $form_city_show_state 
	  							+ " class='btn orange white-text'>Add a new State/Region</label>"
							+ "</div>"
							+"<div class='col l4 m4 s12' >"
								+ "Name of City/District "
							+"</div>"
							+ "<div class='col l8 m8 s12' >"
		  						+ "<input name='new_city' id='new_city' />"
		  					+ "</div>"
		  					
		  					+ "<div class='col l4 m4 s6 right-align' >"
	  							+ "<label "+ $form_city_add_btn 
	  							+ " class='btn green'>Save City Info</label>"
	  						+ "</div>"
	  						+ "<div class='col l3 m4 s4 right-align' >"
	  						+ $form_close_btn 
	  						+ "</div>"
	  					;

	var $new_state =
				 "<div class='col l12 m12 s12 new_state_in right-align hide_if_nostates' >"
	  							+ "<label "+ $form_city_show_state 
	  							+ " class='btn red'>New State not required </label>"
				+ "</div>"	 
				+	"<div class='col l4  m4 s12 new_state_in' >State/Region Code</div>"
				+  "<div class='col l8 m8  s12 new_state_in'> "
   	   		+ "<input name='new_statecode' id='new_statecode' size='2' maxlength='2' />"
   	   	+"</div>"
				+ "<div class='col l4 m4 s12 new_state_in'>State/Region Name </div>"
  				+ "<div class='col l8 m8 s12 new_state_in'>"
	  				+"<input name='statename' id='statename' />"
  				+"</div>"
  				;

	$str_div = "<div class='row '>";		


	
	if($xstates)
	{

			$h_xlabel = {new_city_label:'Add City',};
			var $form_heading = Mustache.to_html($mst_form_heading, $h_xlabel);  		

			$str_div = $form_heading;		
			
			$str_div += "<div class='row '>";
			$str_div += "<div class='col l4 m4 s12 old_state_in' >State/Region</div> "
				+"<div class='col l8 m8 s12 old_state_in'> ";
			var $div_drop = gx_states($xstates);
			
	  		$str_div += $div_drop;
	  		$str_div += "</div>";
	  		$str_div += $new_state;	
	  		$str_div += $in_city;
	  		$str_div += "</div>";
	
			$($div_id).append($str_div);  	
			bx_div_class_hide(".new_state_in");	
  	}	
  	else
  	{
		$h_xlabel = {new_city_label:'Add Region and City',};
		var $form_heading = Mustache.to_html($mst_form_heading, $h_xlabel);  		
  		
		$str_div += $form_heading;		  		
  		$str_div += $new_state;
	  	$str_div += $in_city;
		$str_div += "</div>";	
		
		$($div_id).append($str_div);  		
  		bx_div_class_hide(".hide_if_nostates");	
		Materialize.toast("Regions are not available for selected country.",2000);
		  		
  	}
  	
	/* 
	$($div_id).empty().
			append($( "<div/>", 
 			{
    				"class": "",
    				"html": items.join( "" )
 				} 
 			)
 	);
 	*/
  	
	
	//gx_input_fields_init();
	
}

/*
* FX:

*/

function gx_new_state_flip($in_div_class)
{
	var $is_visible = $($in_div_class).is(':visible');
	if($is_visible)
	{
		bx_div_class_hide($in_div_class);
		bx_div_class_show(".old_state_in");
	}
	else
	{
		bx_div_class_show($in_div_class);	
		bx_div_class_hide(".old_state_in");	
	}
}



/*
* FX: gx_flip_hide_show

*/

function gx_flip_hide_show($in_div_class)
{
	var $is_visible = $($in_div_class).is(':visible');
	if($is_visible)
	{
		bx_div_class_hide($in_div_class);
	}
	else
	{
		bx_div_class_show($in_div_class);		
	}
}

/*
* FX: gx_States(xstates): Creates Select Drop down for States
* 
*/

function gx_states($xstates)
{
	
	if($xstates)
	{
			var $str_drop;	
			$str_drop = "<Select class='browser-default' name='state' id='statecode'>"
								+ "<option value=''>Select </option>";
			
	  		$.each( $xstates, function( index, xval ) 
	  		{
	  			var $name  = xval.statename;
	  			var $code  = xval.statecode;
	  			var $ccode = xval.countrycode;

	  			var $x = "<option value=' " + $code + " ' "  
	  								+ " >" 
	  								+ $name + ", " + $ccode 
	  						+ "</option>"; 

	  			$str_drop += $x;

	  		});
	  		
	  		$str_drop += "</select>";

			return $str_drop;
	}			
	
}

/*
* FX: g_save_city
* POST: g/address/city
* Data: country_code,state_code,name
*
*/
function g_save_city($div_id)
{
	
	var $name			= $('#new_city').val();
	var $country_code = $("#countrycode").val() || $("#icountry").val();		
	var $state_code 	= $("#statecode").val();
	
	var $new_state    = $("#new_statecode").val();
	var $state_name   = $("#statename").val();
	/* 
		Materialize.toast($country_code,5000);
		Materialize.toast($new_state,5000);
		Materialize.toast($state_name,5000);
		Materialize.toast($name,5000); 
	*/

	var $h_cx;
	var $is_ready = 0;
	
	if($new_state && $state_name && $name && $country_code)
	{
		$h_cx = {
					name:				 $name,
					country_code:	 $country_code,
					new_state_code: $new_state,
					new_state_name: $state_name,
				};
		$is_ready = 1;		
	}
	else if($name && $country_code && $state_code)
	{
		$h_cx = {name:$name,country_code:$country_code,state_code:$state_code};
		$is_ready = 1;
	}

	if($is_ready > 0)
	{

		var $save_url = "/g/address/city";
		$.ajax( 
					{ 
						url: 			$save_url ,
						type: 		"POST",
						dataType: 	"json",
						data:			$h_cx,
						success: function( xdata)
						{
							Materialize.toast("Saved City",3000);
							var $istate_code	 = xdata.state_code;
							var $icountry_code = xdata.country_code;
							var $icity_code 	 = xdata.city_code;
							var $iname			 = xdata.name;
							
							var $icode = $icountry_code+":"+$istate_code+":"+$icity_code;

							$("#sin_citycode").val($icode);
							$("#icity_autocomplete").val($iname);
							gx_input_fields_init();
							$($div_id).empty();
							//name,country_code,state_code,city_code
						},
				}	
		);

			
	}	
	else
	{
			Materialize.toast("Fields are missing",3000);
	}
	
}


