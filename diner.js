/*
	 2015-12-25
	 Diner.js, JS Utilities for BAEL's Diner & FoodTakeOut

    This file is part of BAEL
    Copyright (C) 2016,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/

/*
Dependency: Mustache.js

*/

var $sign_kit_item_new = " &#x2794; ";
var $sign_kit_item_cooking = "&#x2615;"

var $dl_arrow_right = "&#10132;";

/* Order   */

/*
* FX: dlx_display_neworderbtn: Display New Order Button
* Used in only for FoodTakeout
*
*/
function dlx_display_neworderbtn()
{
   var $appid = $('#in_appid').val();
	var $fn_click = "onClick=dlx_create_fto_neworder();"	   
   //$('#fto_neworder').append($appid);
	if($appid == $c_appid_foodtakeout)
  	{
  			var $nbtn = "<a class='btn orange' id='fto_neworder' "+ $fn_click +" >+ Create an Order</a>";
  			$('#fto_neworder').append($nbtn);
  	}
  	

}

/*
* FX:dlx_create_fto_neworder Create New FoodTakeOut New Order.
* After success, takes you to New Order
* 
*/

function dlx_create_fto_neworder()
{
   var $appid = $('#in_appid').val();
   
   var $wait_msg = $progress_clock + "<p span='green'>Please wait, while order is being created.</p>"
   $('#fto_neworder').append($wait_msg);

   var $spa_sx = $("#sx_type").val();
// USed through Orders List


   
	if($appid == $c_appid_foodtakeout)
  	{
			var $url = "/pickup/order/creation";

			$.post( $url, 
			function(response) 
			{
  				var $orderid = response.orderid;
  				if($orderid && $spa_sx )
  				{
						g_display_existing_order($orderid);  					
  				}
  				else if($orderid)
  				{
					window.location = '/foodtakeout/order/'+$orderid;
				}	
			} 
			, 'json') ;
	
  	}
  	//IF
  	

  	
  	
}

//Update OrderList

/*
* 
* FX: dlx_display_menu: Display Menu for diner/order and diner/menu
* @param: Data
* @param: Gets the BizOrderID val.
* Add Menu to @tabmenu
*
*/
function dlx_display_menu(data)
{
	  	var items = [];
  		var $prev_groupid;
		/*
      var $idx_first = data.idx_first;
      var $idx_last = data.idx_last;
      var $idx_str = $idx_first + "-" + $idx_last;
      var $rest_pg_txt = "#rest_pagination_text";	
      $($rest_pg_txt).empty();
      $($rest_pg_txt).append($idx_str);
      */
		var $bizorderid = $("#bizorderid").val() || $("#bizorderid").text(); 			
  		
  		
	  $.each( data.items, function( key, val ) 
	  {

	  		 var $m_group_name = val.group_name;
	  		 var $m_groupid = val.groupid;
	  		 if(!$prev_groupid)
	  		 {
	  		 	var $x_begin   = "BEGIN";
			 	//items.push($x_begin);	  		 	
	  		 }
		  		 	
			 if($m_groupid != $prev_groupid)
			 {
 	  		   var $x_group   = "<div >"  
	  		 				+	"<div class='xmenu_group_name btn-large "
	  		 							+ " white green-text card col l12 m12 s12'><u>"
	  		 							+ $m_group_name
	  		 							+ "</div></u>";
			 	items.push($x_group);
			 }	 
			 $prev_groupid = $m_groupid;		   
          $m_mi = dl_div_menuitem(val,$bizorderid);
		    items.push($m_mi);
  		})

  $( "<div/>", 
  {
    "class": "row",
    html: items.join( "" )
  }).appendTo( "#tabmenu" );

	
}

/*
* FX: dl_display_menu_order: This is with Cache
* This is used where data is cached. for guest users. 
* 
*/
function dl_display_menu_order_cache($incount,$inpage)
{
	var $page =  1;
	var $rows =  30;		

   var $div_xt = "#pgx_display_type"; 
   var $display_type = $($div_xt).val(); 	
	var $in_groupid ;
	if($display_type != 'pages')
	{
	   $in_groupid  = g_rest_pgx_group($inpage);
	}	
  
	//Try to get First Group if Not found
	if(!$in_groupid && $display_type != 'pages')
	{
		var $new_page = 1;
		$in_groupid = g_rest_pgx_group($new_page); 
		
		//var $me = "<span class='red-text'>No More Data "
 								+ "</span>";
		//$('#tabmenu').empty();
		//$('#tabmenu').append($me);
		
		//Run when groups are available no groupid found.
      rpage_reset();
      
	}		
   
	var $in_branchid = bgx_get_branchid();
	var $slash = "/";
		
	//#tabMenu
	var $div_tm = "#tabmenu";
   $($div_tm).empty();
   
	var $div_id_msg = "Please wait while the Items are fetched....";
	//$( "#xyz" ).append($in_groupid);
	try 
	{
		progress_append($div_tm,$div_id_msg);
	}
	catch(err)
	{
		$( "#xyz" ).append(err);
	}
	
	$( "#xyz" ).css( "backgroundColor", "green" );	   
	var $base_url;
	var $appid = $('#in_appid').val();
	var $url;

	if($in_groupid && $appid == $c_appid_restaurant)	
	{
  		$base_url = "/g/restaurant/menu/list/" + $in_branchid + $slash + $page + $slash + $in_groupid;
  		$url = $base_url;	
	}
  	else if($appid == $c_appid_restaurant)
  	{
  		$page = $inpage || 1;
  		$base_url = "/g/restaurant/menu/list/" + $in_branchid + $slash + $page;
  		$url = $base_url;	
  	}
	else if($in_groupid && $appid == $c_appid_foodtakeout)	
  	{
  		$base_url = "/g/pickup/menu/list/" + $in_branchid + $slash + $page + $slash + $in_groupid;
  		$url = $base_url;	
  	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$page = $inpage || 1;
  		$base_url = "/g/pickup/menu/list/" + $in_branchid + $slash + $page;
		$url = $base_url;		
  	}	

	if($in_groupid)
	{		
		$.ajax( 
					{ 
						url: $url ,
						type: "GET",
						dataType: "json",
						headers: { 'groupid':$in_groupid },
						statusCode: 
						{
    								404: function() 
    								{
				 							rpage_reset(0);    									
    								},
    					},		
 						error: function (xhr,status,error) 
 						{
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
							$($div_tm).empty();
							$($div_tm).append($error_msg);					
 							
 							var $me = "<span class='red-text'>No Items for "
 								+ $in_groupname + "</span>";
 							$('#tabmenu').empty();
 							$('#tabmenu').append($me);
            		},
						success: function( data )
						{
							$($div_tm).empty();
		  					dlx_display_menu(data);
						},
				}	
		);
	}
	else
	{
		$.ajax( 
					{ 
						url: $url ,
						type: "GET",
						dataType: "json",
						statusCode: 
						{
    								404: function() 
    								{
				 							rpage_reset(0);    									
    								},
    					},		
 						error: function (xhr,status,error) 
 						{
 							var $me = "<span class='red-text'>No Items for page:"
 								+ $inpage + "</span>";
 							$('#tabmenu').empty();
 							$('#tabmenu').append($me);

            		},
						success: function( data )
						{
							$($div_tm).empty();
		  					$('#pgx_name_display_current').empty();
		  					if($page > 0)
		  					{
		  						$('#pgx_name_display_current').append($page);
		  					}		
		  					dlx_display_menu(data);
						},
				}	
		);
		
	}

	
};

/*
* FX: dl_display_menu_order: This is the original without cache
* Used after user of a branch is logged in.
* 
*/
function dl_display_menu_order($incount,$inpage)
{
	var $page =  1;
	var $rows =  30;		
	
   var $in_groupid = g_rest_pgx_group($inpage); 
	//$( "#xyz" ).css( "backgroundColor", "red" );

	
	//#tabMenu
	var $div_tm = "#tabmenu";
   $($div_tm).empty();
   
	var $div_id_msg = "Please wait while the Items are fetched....";
	//$( "#xyz" ).append($in_groupid);
	try 
	{
		progress_append($div_tm,$div_id_msg);
	}
	catch(err)
	{
		$( "#xyz" ).append(err);
	}
	
	$( "#xyz" ).css( "backgroundColor", "green" );	   
	var $base_url;
	var $appid = $('#in_appid').val();
  	if($appid == $c_appid_restaurant)
  	{
  		$base_url = "/restaurant/menu/list";
  	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$base_url = "/pickup/menu/list";  	
  	}	

	var $slash = "/";
	var $url = $base_url + $slash + $rows + "/" + $page; 
	
	if(!$in_groupid)
	{

		var $new_page = 1;
		$in_groupid = g_rest_pgx_group($new_page); 
		
		var $me = "<span class='red-text'>No More Data "
 								+ "</span>";
		$('#tabmenu').empty();
		$('#tabmenu').append($me);
      rpage_reset();
	}		
	

	if($in_groupid)
	{		
		$.ajax( 
					{ 
						url: $url ,
						type: "GET",
						dataType: "json",
						headers: { 'groupid':$in_groupid },
 						error: function (xhr,status,error) 
 						{
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);
									$($div_tm).empty();
									$($div_tm).append($error_msg);					
 							
		 							var $me = "<span class='red-text'>No Items for "
 											+ $in_groupname + "</span>";
 									$('#tabmenu').empty();
 									$('#tabmenu').append($me);
 									Materialize.toast("No Menu Items");
 						
            		},
						success: function( data )
						{
							$($div_tm).empty();
		  					dlx_display_menu(data);
						},
				}	
		);
	}

	
};

/* Menu Item Diner/Order */
var $mst_menuitem =  
 	  						"<div class='row'  "
	  		 							+ " href='' >"
		  		 			+ "<span class='col m7 s8 hide-on-large-only "
		  		 								+ " left-align black-text '>"
		  		 								+ "{{{cart_symbol}}}" + "{{name}}"
									+ "</span>"
		  		 				+"<span class='col l12 hide-on-med-and-down "
		  		 								+ "center black-text card-title icode' id='item_{{product_code}}' >"
		  		 								+ "{{name}}"
									+ "</span>"	
										
							+ "<span class='col m5 s4 left-align hide-on-large-only'>"
						 							+ "@ {{price}}"   
									+"</span>"
									+ "<span class='col l12 center-align hide-on-med-and-down'>"
						 							+ "@ {{price}} {{cc}} {{unit_type}}"   
									+"</span>"

			  		 		+ "<span class='col l12 hide-on-med-and-down "
			  		 								+ "center black-text '>"
			  		 								+ "{{{cart_symbol}}}" 
									+ "</span>"	

	  		 				+ 	"</div>";
var $mst_mitemb = "<div class=''>"
	  		 				+	"<div class='xmenu_item btn-large btn-flat waves-effect waves-green "
	  		 							+ " grey lighten-3 black-text card col m12 s12 hide-on-large-only' "
	  		 							+ "{{{fn_click}}}"
	  		 							+ ">"
										+ "{{{str_item}}}"
	  		 						+ 	"</div>"	  		 							

	  		 				+	"<div class='waves-effect waves-green border"
	  		 							+ " grey lighten-3 black-text card col l4 hide-on-med-and-down' "
	  		 							+ "{{{fn_click}}}"
	  		 							+ ">"
										+ "{{{str_item}}}"
	  		 						+ 	"</div>"
	  		 				+	"</div>";


/*
* FX: dl_div_menuitem(val,bizorderid)
* @returns: String Containing div
* @params: val,bizorderid
* 
* Uses: mst_menuitem,mst_mitemb
*
* To fix: in Small, longer names are not displayed fully.
*
* item_pc: is used for display in order section
*
*/
function dl_div_menuitem(val,bizorderid)
{
	  		 var $m_pc      	= val.product_code;
	  		 var $m_bid     	= val.branchid;
	  		 var $m_pname   	= val.product_name;
	  		 var $m_price   	= val.price;
	  		 var $m_cc      	= val.currency_code;
	  		 var $m_units 		= val.unit;
	  		 var $m_group_name = val.group_name;
	  		 var $m_groupid 	= val.groupid;

			 var $fn_click 	= ""; 
		    var $cart_symbol = "";
		    
		    if(bizorderid)
		    {
				var $menu_cart_units_id   = "xsym_pcu_" + $m_pc; 
			 	$cart_symbol = "<span class='btn-floating green white-text center sym_menu_units '  "
											+ "id='"+ $menu_cart_units_id +"' > "
											+ "+ </span> ";
	  		 	$fn_click = "onClick=dl_order_push_in_cart('"+ $m_pc +"');"													
			 }
  		 
			 //var $m_units = "/" + val.unit;	  		 

	  		 var $shf = "'#shuffle'";
	  		 var $fnc = 
	  		 		"<script>$(#"
	  		 		+ $m_pc 
	  		 		+ ").click(function(){ "
	  		 		+ "	$( "+ $shf +" ).append( '<p>red</p>' ); "
	  		 		+ "return false; } );"
	  		 		+ "</script>";
	  		 var $all = [$m_pc,$m_pname]		;
	  		 
		    var $h_mitem = { 
		    						'product_code':	$m_pc,
									'name':				$m_pname,
									'price':				$m_price,
									'cc':					$m_cc,
									'unit_type':		$m_units,
									'cart_symbol':		$cart_symbol,		
							 	 };
			 //Menuitem	  		 							
	  		var $m_a = Mustache.to_html($mst_menuitem, $h_mitem);

			 //This is for small/med/large
			var $h_mitemb = {
				 					fn_click: $fn_click,
									str_item: $m_a,			 						
			 				 	  };
			var $x_a = Mustache.to_html($mst_mitemb, $h_mitemb);
		   return $x_a;	
	
}




//Display Menu
/*

Returns: GroupID Current

*/


/*
* FX: dl_display_menuo: Display Menu for Displaying only.
* 
* Used by Diner/menu
*
* @params: data
* Adds to div: #tabmenu
*/
function dl_display_menuo(data)
{
  		var items = [];

	   $.each( data.items, function( key, val ) {
	  		 var $m_pc  		= val.product_code;
	  		 var $m_bid 		= val.branchid;
	  		 var $m_pname  	= val.product_name;
	  		 var $m_price 		= val.price;
	  		 var $m_a   = "<div class='border card col l4 m6 s12 z-depth-2 yellow lighten-3' id="+$m_pc+">"
	  		 					+ 		"<div class='card-content'><span class='card-title black-text'>"
	  		 								+ $m_pname + "</span>"
	  		 					+ 		"<div>" + "Product Code: " + $m_pc + "</div>"
	  		 					+	 	"<div>" + "Price: " + $m_price + "</div>"
	  		 				   +	"</div>"
	  		 				   + "</div>";
		    items.push($m_a);
  		})
  		//ForEach
 
  $('#tabmenu').empty();
  
  $( "<div/>", 
  {
    "class": "row",
    html: items.join( "" )
  }).appendTo( "#tabmenu" );
 	
	
}

/*
* FX:dx_move_bill: Move to Bill single Page
* @param: billid
*/
function dx_move_bill($billid)
{
		if($billid)
		{
				$("#sx_type").val('bill');
				$("#sx_value").val($billid);
				$("#billid").val($billid);
				move_pager('bill');
		}		
}



/*
* FX: dl_make_bill: Make Bill for a bizorder
* @params: appid,bizorderid
*  
* Used By: Diner/bill
* Write Function
* Depending on context shifts to Bill page OR Bill SPA
* 
*/
function dl_make_bill(appid,bizorderid)
{
	var $url;
	var $redirect_url;
	if(appid == $c_appid_restaurant)
	{
			$url ="/restaurant/order/bill/";
			$redirect_url = "/diner/bill/";
	} 
	else if(appid == $c_appid_foodtakeout)
	{
			$url ="/pickup/order/bill/";
			$redirect_url = "/foodtakeout/bill/";
	}

	var $bizrole = bgx_get_bizrole();
	
	var $progress = "Please Wait..." + $progress_clock;
	var $div_id = '#bizorder';
	$("#x_bill").prop("disabled", true);
	var $div_id_msg = "Please wait while the bill is generated....";
	progress_append($div_id,$div_id_msg);
	
	var $order_id = bizorderid;
	var $sx_type = $('#sx_type').val();

	//Block for Roles.
	if($bizrole == 'SERVER' || $bizrole == 'COOK')
	{
		$.post( $url + $order_id, function(response) 
		{
			bx_toast_empty();
			$(".xmsg").remove();			
			//Remove all previous popups

			var $bill_id = response.billid;

			var $x_msg = "<span class='xmsg red-text'>Bill has been generated.</span>";
			Materialize.toast($x_msg, 10000);	
			$($div_id).append($x_msg);

			bx_progress_empty($div_id);
			//End			
		} , 'json') ;
	}
	else if($sx_type)
	{
		$.post( $url + $order_id, function(response) 
		{
			var $bill_id = response.billid;
			$('#sx_type').val('bill');
			$("#billid").val($bill_id);
			move_pager('bill');			
		} , 'json') ;
		
	}
	else
	{
		$.post( $url + $order_id, function(response) 
		{
			var $bill_id    = response.billid;
			window.location = $redirect_url+$bill_id;
		} , 'json') ;
	}		

	$("#x_bill").prop("disabled", false);

}

/*
* FX: dl_display_diner_allbills: Display Bills with pagination
* @params: incount,inpage,indate
* Fetches data and calls dl_display_bills
*
*/
function dl_display_diner_allbills(incount,inpage)
{

  var $fnx;
  var $appid = $('#in_appid').val();
  if($appid)
  {
 		 var $url;
  		if($appid == $c_appid_restaurant)
  		{
  			$url = "/restaurant/bill/list";
  		}
  		else if($appid == $c_appid_foodtakeout)
  		{
  			$url = "/pickup/bill/list";  	
  		}
  		$fnx = $url ;
	}	
	
	var $in_rest = "/restaurant/bill/list";
	var $in_page = inpage || 1;
	var $in_count = incount;		

	var $in_date 			  = $('#fnx_date').val() || $('#i_date').val() || $("#in_date").val();
	Materialize.toast($in_date,1000);	
	
	var $search_orderid    = $('#fnx_bill_orderid').val()    || 	$('#i_orderid').val();
	var $search_billnumber = $('#fnx_bill_billnumber').val() || 	$('#i_billid').val();;
	   	
	//var $in_m = $in_rest + "/20/1";	
	var $in_m = $fnx + "/" + $in_page + "/" + $in_count ;
	var $rest_pg_txt = "#rest_pagination_text";

	$.ajax( 
				{ 
				url: $in_m ,
				type: "GET",
				dataType: "json",
				headers: { 
								'indate':		$in_date,
							   'orderid':		$search_orderid,
							   'billnumber':	$search_billnumber,							   
							  },
 				error: function (xhr,status,error) 
 				{
 						var $me = "<span class='red-text'>No Bills for Page:"
 								+ $in_page + "</span>";
 						$('#div_diner_allbills').empty();
 						$('#div_diner_allbills').append($me);
            },
				success: function( data )
				{
					dl_display_bills(data);
				},
					//GetJSON Function_data

	});
	//ajax

}// Function_dda

/*
* FX: dl_display_bills: Display Bills 
* @params: data
* Adds each bill to div:#div_diner_allbills
*
*/
function dl_display_bills(data)
{
   	var items = [];
   		       
	    var $idx_first = data.idx_first;
	    var $idx_last = data.idx_last;
	    var $idx_str = $idx_first + "-" + $idx_last;
	    var $rest_pg_txt = "#rest_pagination_text";	  		
       $($rest_pg_txt).empty();
		 $($rest_pg_txt).append($idx_str);
   		       
		 $.each( data.items, function( key, val ) 
		 {
		 			 var $div_abill =	dlx_list_abill(val);
              	 items.push($div_abill);
       });
       //ForEach
		
		var $m_tc = " </tbody>  </table>";
		items.push($m_tc);	

  		$('#div_diner_allbills').empty();

  		$( "<div/>",  { "class": "row",html: items.join("") }).
  			appendTo( "#div_diner_allbills" );
					
}

/*
* FX: dlx_list_abill: Diner Bill div of Bill List.
*
*/

function dlx_list_abill(val)
{


	          var $m_billid  		= val.billid;
	          var $m_appid			= val.appid;
	          var $m_url  			= val.url;
	          var $m_txnid		   = val.transactionid;
	          var $m_bizorderid	= val.orderid;
				 var $m_tablenumber	   = val.tablenumber;
				 var $str_table;			          
	                                   
	          var $m_cancel	= val.cancelled_at;
	          var $str_status;
	          var $str_payment; 
	          var $str_cancel = '<span></span>';

             if($m_cancel)
             {
             		$str_cancel = " <span class='red-text'><b>(X)</b></span> ";
             		$str_status = $str_cancel;
             }        
				 else if($m_txnid > 0)
				 {
				 		$str_payment = "<span class='green-text bold'>PAYMENT</span>";
				 		$str_status = $str_payment;
				 }
				 else
				 {
				 		$str_status = "<span class='red-text '><b>PENDING</b></span>";;
				 }
				 
				 if($m_tablenumber)
				 {
				 		$str_table = ", T-"+$m_tablenumber;
				 }
				 else
				 {
				 		$str_table = "";
				 }
                         
                                          
      		var $str_order ="<span><b>OID-" + $m_bizorderid + "</b> (" + $m_appid + ")</span>";
                      var $m_userid			= val.userid ;
                      var $m_customerid   = val.customerid;
                      var $str_user = "<p>U: "+ $m_userid + "</p>";
						    if(!$m_userid && $m_customerid)
						    {
						    	$str_user = "<p>C: "+ $m_customerid + "</p>";
						    }

				var $fn_click = "onClick=dx_move_bill('"+$m_billid +"');";
				var $sx_type = $("#sx_type").val();
				var $info_btn = "<a class='btn blue white-text' href=/diner/bill/" 
						      	+ 		$m_billid  + "> Info</a> ";

				if($sx_type)
				{
						var $info_btn = "<a class='btn-floating red white-text' id='abill' "
									+ $fn_click +" >" 
								+ $dl_arrow_right +  "</a>";			
				}	   
		


                   var $m_billdate  	= val.billdate;
                   var $m_days_bill 	= val.days_bill;
            var $div_abill   = 
              "<div class='card border col l4 m6 s12'>" +
              		" <div class='card-content black-text'> "
		      
		      + "<span class='card-title black-text'>" 
		      + 	"B-"+	$m_days_bill + "</span>"
		      + "<p>" + $m_billdate + $str_table +"</p>" 
		      + 	$str_order				      
		      +  $str_user
		      +  $str_status
		       					      
            + "<div class='card-action'>"
            	+ $info_btn 
		      +  "</div>"
		      
		      + "</div></div>";
		      
		      return $div_abill;
	
}

/****** Kitchen
*
* FX: dlx_display_kitchen: Display Kitchen Item Status 
*
* @params: data,output_div
* 
*/
function dlx_display_kitchen(data,$out_div)
{
	  $output_div = $out_div || '#list_kitchen_begin';
  		var $items = [];
  		var $prev_stageid;
		var $count = 0;
  		
	  $.each( data, function( key, val ) {

		    var $m_stageid = val.stageid;
		    /*
		    $prev_stageid = $m_stageid;
		    if(!$prev_stageid)
		    {
 	  		   var $x_hr   = "<div class='col btn-large lime  black-text  l12 m12 s12'>BEGIN</div>";  
			 	items.push($x_hr);
		    }
			 else if( $m_stageid != $prev_stageid && $m_stageid == 'COOKING' )
			 {
 	  		   var $x_hr   = "<div class='col btn-large lime  black-text  l12 m12 s12'>" + $m_stageid +	"</div>";  
			 	items.push($x_hr);
			 }	
			 else if( $m_stageid != $prev_stageid && $m_stageid == 'SERVED' )
			 {
 	  		   var $x_hr   = "<div class='col btn-large green black-text  l12 m12 s12'>" + $m_stageid +	"</div>";  
			 	items.push($x_hr);
			 }
			 */	
  			 $count++;
	  	    var $div_kit = dlx_div_kitchenitem(val,$count);
		    $items.push($div_kit);
  		})

  		return $items;
  
}

/*
* Used to Display in Kitchen: Order Product
*
*/
var $mst_div_kitchen_item   = "<div class='card-panel {{row_color}} row border'>"
	  		 
	  		 					+ "<div class='col l2 m6 s12 left-align'>" 
	  		 							+ "{{{symbol}}} &nbsp; {{product_name}}" 
	  		 							+ "</div>"

								+ "<div class='col l3 m6 s12 '>"
										+ 	"x {{units}} {{{diff}}} "
										+ "</div>"

	  		 					+ "<div class='col l2 m6 s6 ' id="
	  		 							+ "{{id_tnumber}} > "
	  		 							+ "O-"
	  		 							+ "{{order_number}} @ "
										+ "{{str_tnumber}}  " 
										+ "</div>"

								+ "<div class='col l3 m6 s6 right card-action '> "
										+ "{{{btn_change}}}"
	  		 							+ "</div> "
	  		 						 	  		 					
	  		 			+	"</div>";

/*
* FX: dlx_div_kitchenitem: creates div for kitchenitem.
*
* @params: val,count
*
*
*/
function dlx_div_kitchenitem(val,$count)
{
		    var $m_stageid 		= val.stageid;
	  		 var $m_tnumber  		= val.tablenumber;
	  		 var $m_product_name = val.product_name;
	  		 var $m_ordernumber  = val.ordernumber;
	  		 var $m_order_cancelled = val.order_cancelled;	  		 
	  		 var $m_units        = val.units;
			 var $m_units_plus   = val.units_increased;
			 var $m_units_minus  = val.units_decreased;
			 var $m_appid			= val.appid;
			 var $str_diff	      = '<span></span>';		
			 var $str_tnumber;

			if($m_appid == $c_appid_restaurant)
			{
				$str_tnumber =	 "T-" + $m_tnumber ;
			}			 
			else
			{
				$str_tnumber =	 $m_appid ;
			}			 	 
				 
	  		 							
		    if($m_units_plus > 0)
		    {
		    	  $str_diff = "<span class='btn white green-text border'> + " + $m_units_plus + "</span>";
		    }	  		 
		    if($m_units_minus > 0)
		    {
		    	  $str_diff = "<span class='btn white red-text border'>  - " + $m_units_minus + "</span>";
		    }
	  		 
	  		 var $m_product_code = val.product_code;
	  		 var $m_orderid = val.orderid;
	  		 var $m_epoch   = val.creation_epoch;

          var $m_bgn =  "'" + $m_orderid + "','" 
	  		 		   	  		+ $m_product_code + "','"
	  		 		   	  		+ $m_epoch + "'"; 
	  		 

		    var $fn_click;
		    $prev_stageid		= $m_stageid;
			 var $link_color	= 'white lighten-5 blue-text';	
			 
			 var $link_remove = "<span class='red-text'> Remove </span>";
			 var $link_begin  = ">> Start ";
			 var $link_finish = "Finish => ";
			 
			 var $symbol 		= '>>' ;
			 var $link_txt   	= $link_begin;

			 if($m_order_cancelled )
			 {
		    	$fn_click	= "onClick=dl_diner_kitchen_ready(" + $m_bgn+");";
		    	$link_txt	= $link_remove;		    				 	
				$symbol		= "<span class='icon_cancelled'></span> ";
			 }
			 else if($m_stageid =='BEGIN')
			 {
			 	$fn_click 	= "onClick=dl_diner_kitchen_inprogress(" + $m_bgn+");";
				$symbol		= $sign_kit_item_new;
			 }
		    else if($m_stageid == 'COOKING' )
		    {
		    		$link_color = 'white lighten-4 black-text';
		    		$link_txt	= $link_finish;
		    	   $fn_click	= "onClick=dl_diner_kitchen_ready(" + $m_bgn+");";
			 		$symbol		= $sign_kit_item_cooking;		    	   		    		
		    }		    
		    else if($m_stageid == 'SERVED' )
		    {
		    		$link_color = 'green flat';
		    		$link_txt	= 'SERVED';
		    		$symbol		= "|||";
		    }		    

			 var 	$row_color = 'white blue-text';
			 if( ($count%2) == 0)
			 {
			 		$row_color = 'yellow lighten-5 black-text';
			 }			    

			var $btn_change;
			$btn_change = "<span class='btn "+ $link_color +"' "
					+ $fn_click +" >"+ $link_txt 
					+ "</span>"; 

			var $h_kitem = {
									row_color		: $row_color,
									id_tnumber		: $m_tnumber,
									order_number	: $m_ordernumber,
									str_tnumber		: $str_tnumber,
									symbol			: $symbol,
									product_name	: $m_product_name,
									units				: $m_units,
									diff				: $str_diff,
									btn_change		: $btn_change,
								}			;	  		 	

		$div_kit = Mustache.to_html($mst_div_kitchen_item, $h_kitem);							
		return $div_kit;
	
}
	  		 

/*
* FX: dlx_kitchen_navbar
* Kitchen Navbar
*
*/
function dlx_kitchen_navbar()
{
	var $gkn;

	$gkn = "<div class='navbar-fixed fixed_bottom'>"
			+ "<nav class='white menu_top '>"
     		+ "<div class='nav-wrapper row'>"
			+ "<a class='col l4 m4 s6 btn orange white-text '" 
				+ " href='#kitchen_begin' >"
	     		+ $sign_kit_item_new + "  New " 
        		+ "</a>"        
         + "<a class='col l4 m4 s6 btn red white-text ' "
         	+ " href='#kitchen_cooking' > "
        		+ $sign_kit_item_cooking + " Cooking  "
        		+ "</a>"        
      	+ "</div>"  
			+ "</nav>"
			+ "</div>"
	
	
	return $gkn;	
}

/*
* FX: dlx_div_kitchen
* Creates Kitchen Div With Progress
* 
*
*/
function dlx_div_kitchen()
{
	var $gdk = 'abc';
	$gdk = "<a name='kitchen_begin'></a>"
			 + "		<div class='row'>"
					+ "<div class='col s12 m12 l12 btn orange white-text'>"
					+ "New "+ $sign_kit_item_new +" </div> </div>"					
			 + "<div align='center' id='list_kitchen_begin'> "
			 	+ $progress_clock
  			 + "</div>"
  			 + " "
			 +	"<a name='kitchen_cooking'></a>"
			 + "		<div class='row'> <div class='col s12 m12 l12 btn red white-text'>"
						+ "Cooking"+ $sign_kit_item_cooking + "</div> </div>"					
			 + "<div align='center' id='list_kitchen_cooking'> "
				 + $progress_clock
  			 + "				</div> "; 
  			 
	  return $gdk;	
}

/*
* FX: dl_diner_kitchen_inprogress
*
* Kitchen InProgress div for a Product
* @params: OrderID,product_code,Epoch
* 
*/
function dl_diner_kitchen_inprogress(orderid,product_code,epoch)
{
   $( '#result' ).css( "backgroundColor", "red" );
   $( '#result' ).append(orderid);	
	$( '#result' ).append(product_code);
	$( '#result' ).append(epoch);
//	$.put( "/restaurant/kitchen/inprogress/"+orderid+"/"+product_code+"/"+epoch );
  
 	var $progress_url = "/restaurant/kitchen/inprogress/";
   	var $urlp =  $progress_url + orderid + "/" + product_code + "/" + epoch;

	$.ajax(
      {
        	url: $urlp,
        	type: 'PUT',
        	headers: { "Content-Type": "application/json", },
        	success: function (data){ Materialize.toast("Product is cooking",3000); },
	   });
	
	dl_display_diner_kitchen_begin();
	dlx_display_diner_kitchen_cooking();
	   

}

//setInterval(diner_kitchen_inprogress, 10000);

/*
* FX: dl_diner_kitchen_ready: Kitchen Ready Product div for a Product.
* @params: OrderID,product_code,Epoch
* 
*/
function dl_diner_kitchen_ready(orderid,product_code,epoch)
{
  	$( '#result' ).css( "backgroundColor", "green" );
	$( '#result' ).append(orderid);
   $( '#result' ).append(product_code);
   $( '#result' ).append(epoch);
	//$.put( "/restaurant/kitchen/ready/"+orderid+"/"+product_code+"/"+epoch;
	var $ready_url = "/restaurant/kitchen/ready/";
   var $urlr =  $ready_url + orderid + "/" + product_code + "/" + epoch;

   $.ajax(
                {
                url: $urlr,
                type: 'PUT',
                headers: { "Content-Type": "application/json", },
                success: function (data) { Materialize.toast("Product is ready",3000); }
               });
	dl_display_diner_kitchen_begin();
	dlx_display_diner_kitchen_cooking();

}


/*
* FX: dl_display_kitchen_begin: Kitchen Begin section 
* @params: incount,inpage
* Output div: #list_kitchen_begin
* 
*/
function dl_display_diner_kitchen_begin(incount,inpage)
{

	var $in_rest = "/restaurant/kitchen";
	var $in_page = inpage || 1;
	var $in_count = incount || 100;		
	var $rest_pg_txt = "#rest_pagination_text";	
   var $in_m = $in_rest + "/" + $in_count + "/" + $in_page ; 
   var $out_div = '#list_kitchen_begin';
   
	$.ajax( 
				{ 
				url: $in_m ,
				type: "GET",
				dataType: "json",
				headers: { 'instatus':'BEGIN' },				
 				error: function (xhr,status,error) 
 				{
					var $me = "<span class='red-text'>Unknown Error.Please try again...</span>";
					bx_toast_empty();
					
					var $error_msg = bx_extract_err_msg(xhr);
					Materialize.toast($error_msg, 5000);
					
 					if($error_msg)
 					{
 							$me = $error_msg;
 					}
 								
					$($out_div).empty();
 					$($out_div).append($me);
            },
				success: function( data )
				{
					items = dlx_display_kitchen(data.items,$out_div);
					$($output_div).empty();
 
  					$( "<div/>", 
  							{  						"class": "panel",
    								html: items.join( "" )
  								}).appendTo( $output_div );
				},
		//Function Success

	});
	 

}

/*
* FX: dlx_display_diner_kitchen_cooking: Kitchen Cooking section
* 
* @params: incount,inpage
*
* Output div: #list_kitchen_cooking
* 
*/
function dlx_display_diner_kitchen_cooking(incount,inpage)
{

	var $in_rest = "/restaurant/kitchen";
	var $in_page = inpage || 1;
	var $in_count = incount || 100;		
	var $rest_pg_txt = "#rest_pagination_text";	
   var $in_m = $in_rest + "/" + $in_count + "/" + $in_page ; 
   var $out_div = '#list_kitchen_cooking';

	$.ajax( 
				{ 
				url: $in_m ,
				type: "GET",
				dataType: "json",
				headers: { 'instatus':'COOKING' },								
 				error: function (xhr,status,error) 
 				{
					var $me = "<span class='red-text'>Unknown Error.Please try again...</span>";
					bx_toast_empty();
					
					var $error_msg = bx_extract_err_msg(xhr);
					Materialize.toast($error_msg, 5000);
					
 					if($error_msg)
 					{
 							$me = $error_msg;
 					}
 								
					$($out_div).empty();
 					$($out_div).append($me);
 						
            },
				success: function( data )
				{
					items = dlx_display_kitchen(data.items,$out_div);
					$($out_div).empty();
 
  					$( "<div/>", 
  							{  						"class": "panel",
    								html: items.join( "" )
  								}).appendTo( $out_div );
  					
				},
		//Function Success

	});

}


/*
* FX: dl_diner_table_order_creation: Create Order from a table
*
* @params: tablenumber
* If success: either display Order(SPA)/shift to Order Page.
*/
function dl_diner_table_order_creation(tablenumber)
{
	var $tbn = tablenumber;
	var $orderid ;
	var $sx_type  = $("#sx_type").val();
	//console.log($tbn)
	var $in_m = "/restaurant/order/creation/"+$tbn;
	
	var $wait_msg = "Order is being created";
	Materialize.toast($wait_msg,10000);
	
	$.ajax( 
				{ 
				url: $in_m ,
				type: "POST",
				dataType: "json",
				timeout: 10000,
 				error: function (xhr,status,error) 
 				{
 						bx_toast_empty();
 						Materialize.toast("Order creation failed. Try again.",3000);
            },
				success: function( data )
				{
						var $oid = data.orderid;
						$orderid = $oid;
					   $("#bizorderid").val($orderid);
					   if( !($sx_type) && $orderid > 0)
						{	
							//console.log( $orderid );	
								window.location = '/diner/orderdiner/'+$orderid;
						}	
						else if($orderid )
						{
								$("#sx_type").val('bizorder');
								$("#sx_value").val($orderid);
								move_pager('bizorder');							
						}
				}	,//success
				
	}); 
			   						    
}//Fn end

/*
* FX: dl_display_diner_table: Display Diner table
*
* @params: incount,pages
* begin fn display_tables
* Output Div: #div_diner_table
*
*/
function dl_display_diner_table(incount,inpage)
{
	var $in_rest = "/restaurant/table/list";
	var $in_page = inpage || 1;
	var $in_count = incount;		
	var $in_date = $('#fnx_date').val();
	var $rest_pg_txt = "#rest_pagination_text";	
   var $in_m = $in_rest + "/" + $in_count + "/" + $in_page ;

	$.ajax( 
				{ 
				url: $in_m ,
				type: "GET",
				dataType: "json",
				headers: { 'indate':$in_date },
 				error: function (xhr,status,error) 
 				{
 						var $me = "<span class='red-text'>No Tables for Page:"
 								+ $in_page + "</span>";
 						$('#div_diner_table').empty();
 						$('#div_diner_table').append($me);
            },
				success: function( data )
				{
					items = dl_diner_tables(data);
			 		$('#div_diner_table').empty();
 
  					$( "<div/>", 
  						{ "class": "row", html: items.join( "" )  }
  							).
  							appendTo( "#div_diner_table" );
				  	// div append end
	
					
				},
		//Function Success

	});
	//ajax

}
//Fn End Display_tables	

/*
* FX: dl_diner_tables: Display Tables based
* 
* @params: data
* dl_diner_tables(data)
* Returns: String
*/
function dl_diner_tables(data)
{
 		var $items = [];
  		
	  $.each( data.items, function( key, val ) 
	  {
			 var $m_a = dl_div_atable(val);
		    $items.push($m_a);
  		})
	   //Each
	  rpage_display(data);
	  return $items;
	
}
/*
* FX: dl_div_atable(val)
* @returns: string (div for a table)

* Each Div has a
*	Link to Create An order for the Table.
*	Link to Existing Pending Order
*/
 
function dl_div_atable(val)
{
         var $m_tnumber  = val.tablenumber;
         var $m_tlocation = val.location;

         var $link_table_orders = "/diner/order/" + $m_tnumber;

         var $fn_click = "onClick=dl_diner_table_order_creation("+ $m_tnumber +");"      
         var $m_corders  = val.count_orders;
         var $link_table_orders = "/diner/order/" + $m_tnumber;
         var $sx_type = $("#sx_type").val();
         var $str_table_free = "Table is Free";
         var $str_newbtn = "<label " + $fn_click
                        + " class='btn waves-effect waves-light green' href='' >"
                        + " " + "+ New Order</label>";
                                      
                                      
         var $str_orders = "<span class='chip white black-text'>" + $str_table_free + "</span>";
         if($m_corders > 0 )
         {
              	$str_orders = "<a class='waves-effect chip black-text' href="
                                    + $link_table_orders
                                    + ">Orders <span class='chip red white-text'> "
                                    + $m_corders  + "<span></a>";

               if($sx_type)
               {
                      var $link_orders = " onClick=dx_move_tables('"+$m_tnumber +"'); ";              
                      $str_orders = 
                                "<a class='waves-effect yellow lighten-5 black-text' "
                                        + $link_orders
                                        + ">Orders <span class='chip red white-text'> " 
                                        + $m_corders  + "<span></a>";   
               } 
                                    
             	var $str_newbtn = "<label " + $fn_click
                                   + " class='btn waves-effect waves-light red' href='' >"
                                   + " " + "+ Add Another Order</label>";
                                                                    
          }

			var $m_a   = "<div class='border card col l4 m5 s12 z-depth-2  ' id="+$m_tnumber+">"
                                                                + "<div class='card-content '>"
                                                                +               "<span class='card-title black-text'>"
                                                                +               "T-" + $m_tnumber
                                                                +               "</span>"
                                                                + "<p>Location: " + $m_tlocation + "</p>"
                                                                + $str_orders
                                                                + "</div>"
                                                                + "<div class='card-action'>"
                                                                		+ $str_newbtn
                                                                + "</div>"
                                                                +       "</div>";
                return $m_a;
}




/*function dl_div_atable(val)
{
	  		 var $m_tnumber  = val.tablenumber;
	  		 var $m_tlocation = val.location;

	  		 var $m_corders  			= val.count_orders;
	  		 var $fn_click 			= "onClick=dl_diner_table_order_creation("+ $m_tnumber +");"
	  		 
	  		 var $sx_type  			= $("#sx_type").val();
	  		 var $link_table_orders = "/diner/order/" + $m_tnumber;	  		 

	  		 var $link_table_orders = "/diner/order/" + $m_tnumber;
	  		 var $str_table_free		= "Table is Free";
			 var $str_newbtn 			= "<label " + $fn_click 
	  		 							+ " class='btn waves-effect waves-light green' href='' >"
	  		 							+ " " + "+ New Order</label>";
	  		 										   		 
			 var $str_orders = "<span class='chip yellow lighten-5'>" + $str_table_free + "</span>";
			  	
			 if($m_corders > 0 )
			 {
			 	$str_orders =
					"<a class='waves-effect yellow lighten-5 black-text' href="
			 										+ $link_table_orders 
			 									+ ">Orders <span class='chip red white-text'> " 
							 					+ $m_corders  + "<span></a>";	
					  if($sx_type)
					 {
					 	var $link_orders = " onClick=dx_move_tables('"+$m_tnumber +"'); ";	   	
			 			$str_orders = 
									"<a class='waves-effect yellow lighten-5 black-text' "
												+ $link_orders
			 									+ ">Orders <span class='chip red white-text'> " 
							 					+ $m_corders  + "<span></a>";	
			 		 } 
							 						
							 						 	 
	 		   var $str_newbtn = "<label " + $fn_click 
	  		 							+ " class='btn waves-effect waves-light red' href='' >"
	  		 							+ " " + "+ Add Another Order</label>";
	  		 										   		 
			 }	  		 
	  		 

	  		 var $m_a   = "<div class='border card col l4 m5 s12 z-depth-2 yellow lighten-5 ' id="+$m_tnumber+">"
	  		 					+ "<div class='card-content '>"
	  		 					+ 		"<span class='card-title black-text'>"
	  		 					+ 		"T-" + $m_tnumber 
	  		 					+ 		"</span>"
	  		 					+ "<p>Location: " + $m_tlocation + "</p>"
	  		 					+ $str_orders
								+ "</div>"
								+ "<div class='card-action'>"
								+ $str_newbtn
	  		 					+ "</div>" 	  		 					
	  		 					+	"</div>";	
	  	return $m_a;	 					
}*/

/*
* FX: dx_move_tables
* Move to SPA tables
*
*/
function dx_move_tables($tnumber)
{
		if($tnumber)
		{
				$("#sx_type").val('orders');
				$("#sx_value").val($tnumber);
				move_pager('orders');
		}		
}

/*
* FX: dl_div_tables: Display Progress while moving to tables
*/
function dl_div_tables()
{
   var $gdm = "ABC";
	$gdm = "<div class='row section'>"
					+ "<div >"
					+ "<div align='center' id='div_diner_table'>"
						+ $progress_clock
					+ "</div>"
					+ "</div>";
					+ "</div>"; 
	return $gdm; 				
}

/*
* FX: dl_div_progress: Display Progress CLock
*
*
*/
function dl_div_progress()
{
	$gdm = "<div class='row section'>"
					+ $progress_clock
				+ "</div>"; 
	return $gdm; 				
}

/*
* FX: dl_rest_pgx: Display Rest Pagination
*
*
*/
function dl_rest_pgx($v_method)
{
	var $dvx;


	 $dvx  	= 
	 				" <div>" 
					+ "<input id='fnx_rest' type='hidden' value=' " 
						+ $v_method 
						+" ' />"
					+ "<input id='fnx_rest_item_count' type='hidden' value='10' /> </div>"
					
	 				+ " <ul class='row button-group' id='rest_pagination'> "
	 
	 				+  "<input id='api_page_current' type='hidden' value='1'>"
   			   +	"<li class='col s4 btn yellow lighten-3 black-text waves-effect " 
    	  				+	" waves-light page_back z-depth-3'>"
 						+ "<b>"
 						+ "<span id='pgx_name_display_prev' class='hide-on-med-and-down'></span>"
    	  				+ "&#8592; "
    	  				+ "</b>"
    	  				+  "</li>"

   				+ "	<li class='col s4 btn red white-text' >"
			 			+	"	<b>"	
			 			+	"	<span id='rest_pagination_text'></span>"
			 			+	"	<span id='pgx_name_display_current'></span>"
			 			+	"		</b>"
			 			+	"</li> "
			 		 
		 			+ "<li class='col s4 btn yellow lighten-3 black-text waves-effect " 
		    	  		+ " waves-light page_ahead z-depth-3'> "
		    	  		+ "<b>"
		    	  		+ "&#8594; "  
		    	  		+ "<span  id='pgx_name_display_next' class='hide-on-med-and-down'></span> "
		    	  		+ "</b>"
    			  		+ "</li> "
    			  		
    			  + "<ul>"; 

	return $dvx; 

 
}

/*
* FX: dl_div_menu: Create div to handle menu.
*
*/
function dl_div_menu()
{
   var $gdm = "ABC";
	$gdm = "<div class='section' id='bizorder_menu'>"
					+ "<div class='card-panel'>"
					+ "<div class='card-title'><h4 class='green-text'>Menu</h4></div>"
					+ "<div id='tabmenu_top'></div>"
					+ "<div id='tabmenu'>"
						+ $progress_clock
					+ "</div>"
					+ "</div>";
					+ "</div>"; 
	return $gdm; 				
}



/*
* FX: dl_bizorder_finalize: Finalize Order
* Send Selected Products to the ORder.
* MethodType: POST Data: can handle multiple products through json.
* 
*/
function dl_bizorder_finalize()
{
   var $progress = $progress_clock;
   $("#bizorder").append($progress);

   $c_bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();

	var $base_url;
	var $appid = $('#in_appid').val();
  	if($appid == $c_appid_restaurant)
  	{
  		$base_url = "/restaurant/order/product/"+$c_bizorderid;
  	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$base_url = "/pickup/order/product/"+$c_bizorderid;  	
  	}	

	var $ran = 0;

	var $fitems = [];
	$fitems = fbx_form_in_items();//GEt the Prod and Units
	
 	$("#bizorder").append("Order is being updated....");
	 
	//ForEach End

	var $url = $base_url;


	$.ajax(
	{
         url:  		$url,

         type: 				'POST',
			data:      			{'items':JSON.stringify($fitems)},
    		traditional:		true,
			dataType: 			"json",

	 		success: function(xdata)
       	{
       	  var orderid = xdata.orderid;
       	  $("#bizorder").empty();
	  		  $ran = dl_display_anorder($c_bizorderid);
         },
         error: function(xhr, status, err)
         {
			              //   console.error(this.props.url, status, err.toString());
				$("#bizorder").empty();
				$("#bizorder").append("Error");
         },
      }
    );
		//Ajax End

	 //$( "#result" ).css( "backgroundColor", "blue" );
	 //Why twice?
	  if($ran < 1)
	  {
				dl_display_anorder($c_bizorderid);
	  } 	  
	  $("#cart").html("");
	  $( '.cart_count' ).html("");
	  //$( "#bizorder" ).css( "backgroundColor", "yellow" );	
	  //$( '#shuffle' ).append( '<p>red</p>' );
	  return false;

	
}

/*
* FX: dl_bizorder_finalize_old: Sends Item to an Order, Each Request can send a single Product only.
* MethodType: PUT
* Redundant now.
*/
function dl_bizorder_finalize_old()
{
   var $progress = $progress_clock;
   $("#bizorder").append($progress);

   $c_bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();
   
  $( ".cart_row" ).each(function()
  {
	    $item_pc = $( this ).find(".item_pc").text();
   	 $item_unitx= $( this ).find(".item_unit").text();
		 //In below line we are removing plus minus sign using parseInt
	    $item_unit= parseInt($item_unitx);

    	 $order_item = $c_bizorderid+"/"+$item_pc+"/"+$item_unit;
    	 //$( this ).find(".item_pc").css( "backgroundColor", "blue" );
    	 $( this ).find(".item_unit").css( "backgroundColor", "red" );
    	 
	var $base_url;
	var $appid = $('#in_appid').val();
  	if($appid == $c_appid_restaurant)
  	{
  		$base_url = "/restaurant/order/product";
  	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$base_url = "/pickup/order/product";  	
  	}	

		var $url = $base_url +"/"+$order_item;
   	//$( "#result" ).append("<p>/restaurant/order/"+$item +"</p>" );

//Here Ajax post function starts
		$.ajax(
		{
         url: $url,
         type: 'PUT',
         headers: {
            // Accept : "application/json",
         "Content-Type": "application/json"
       },

 		   success: function(data)
         {
      //                                              console.log(data.orderid);
       	  var orderid = data.orderid;
       	  $("#bizorder").html("");
       	  dl_display_bizorder({});
           //console.log(orderid);
           return false;
           //e.preventDefault();
           

		     //  window.location = '/diner/orderdiner/'+orderid;
    			//this.setState({data: data});
         }.bind(this),

         error: function(xhr, status, err)
         {
			              //   console.error(this.props.url, status, err.toString());
         }.bind(this)
       }
       );

  	}

  	);
  	
		  //$( "#result" ).css( "backgroundColor", "blue" );  
		  dl_display_anorder($c_bizorderid);
		  $("#cart").html("");
		  $( '.cart_count' ).html("");
		  //$( "#bizorder" ).css( "backgroundColor", "yellow" );	
		  //$( '#shuffle' ).append( '<p>red</p>' );
  		  return false;	

	
}


/*
* FX: dl_display_anorder: Display An Order
*
* @params: bizorderid
*
*/
function dl_display_anorder($x_bizorderid)
{
	var $url;
	var $appid = $('#in_appid').val();

	
  	if($appid == $c_appid_restaurant)
  	{
  		$url = "/restaurant/order/";
  	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$url = "/pickup/order/";  	
  	}	
	
	//var $x_bizorderid = $( "#bizorderid" ).text();
	var $in_m = $url + $x_bizorderid;

	$.ajax( 
				{ 
				url: 			$in_m ,
				type: 		"GET",
				dataType: 	"json",
 				error: function (xhr,status,error) 
 				{
 						var $me = "<span class='red-text'>oops something went wrong. Please try again.</span>";
						$("#rest_messages").append($me);
            },
				success: function( datafc )
				{
				   $("#more_btn_below").empty();       	  
 					dl_display_bizorder(datafc);

					var $days_order;
					$days_order 	 = datafc.days_order;
					var $bizorderid = datafc.orderid;
					var $str_dor = " O-" + $days_order; 
					$(".display_bo").empty();
					$(".display_bo").append($str_dor);
					$(".sym_menu_units").text("+");//Menu Item Sign
					return $days_order;
				},
		//Function Success

	});
	//ajax

}

/*
* FX: dl_bill_anorder: Bill an Order
*
* @params: fetches orderid,appid.
*
*/
function dl_bill_anorder()
{
		var $appid 			= $('#in_appid').val();
	   var $bizorderid	= $( "#bizorderid" ).text() || $( "#bizorderid" ).val();
		dl_make_bill($appid,$bizorderid);			
}

/*
* Button to Create Bill: Order to Bill
* 
*/
$('body').on('click', '#x_bill', function ()
{
	dl_bill_anorder();
});

/*
* Button to Add Items to Order
* Finalize Button -> Put Items in the Order through AJAX.
*
*/
$('body').on('click', '#x_toggle', function ()
{
	dl_bizorder_finalize();		
});

//section ends here

/*
* FX: dl_sticky_dinerorder: NavBar for DinerOrder
*
*
*/
function dl_sticky_dinerorder() 
{

var $div_sticky = 
		"<div class='navbar-fixed fixed_bottom'>"
			+ "<nav class='blue-grey lighten-4 line_height center' role='navigation'>"
				+ "<ul class='row'>"
			     	+ "<a class='white red-text col  l6 m6 s6 waves-effect' href='#mag_order'>"
				     		+ "	Order"
				    		+ "	<span class='xorder_count badge white-text bold'></span>"
				    		+ "	</a>"
				   + "<a class='grey lighten-3 green-text col l6 m6 s6 waves-effect ' href='#mag_menu'>"	
				    		+		"Menu"
				    		+	"</a>"  			

					+	"	</ul>"
			+  "</nav>"
		+  "</div>";

		return $div_sticky;

}

/*
Templates for Buttons of Place Order,Make Bill in diner/order Page.
*/
var $div_btn_finalize = "<div class='row di_bizorder_btn' id='di_order_finalize'>"
										+ "<div class='col s12 btn-large   red waves-effect waves-light ' "
										+ " id='x_toggle' href=''>"
									+ "Confirm Order</div>"
								+ "</div>";

var $div_btn_make_bill = "<div class='row di_bizorder_btn' id='di_order_bill'>"
								+ "<div class='col l12 m12 s12 btn-large green waves-effect waves-light ' "
									+ " id='x_bill' href=''>"
									+ "Make Bill</div>"
								+ "</div>";

/*
* FX: di_is_btn_bill_order: Button: Bill and Order in SPA
* @params: input 
*
*/

function di_is_btn_bill_order($input)
{

  var $x_method = $input || 'none';	

  var $div_action = '#bizorder_action';	  
  var $is_buyer = $("#buyerid").val();
  //Guest is ordering home delivery for guest.js

  	//Remove Bill and Order Button Both.
  	$(".di_bizorder_btn").remove();	

	if($x_method      	== 'bill' && $is_buyer)
	{
			$($div_action).append($div_btn_gmake_bill);
	}
	else if($x_method 	== 'order' && $is_buyer)
   {
			$($div_action).append($div_btn_gfinalize);
   }
	else if($x_method 	== 'confirm' && $is_buyer)
   {
			$($div_action).append($div_btn_gbill_confirm);
   }
	else if($x_method      	== 'bill')
	{
			$($div_action).append($div_btn_make_bill);
	}
	else if($x_method 		== 'order')
   {
			$($div_action).append($div_btn_finalize);
   }

	//$("#error_msg").append($method);

}

/*
* FX: dl_display_order_section: display Order Section
* 
*
*/
function dl_display_order_section()
{
	var $gos = "<div class='section' >"

					+ "<div class='order_count right' >"
					+"</div>"

					+ "<div class='card'>"
			
						+ "	<div class='card-content'>"
				  			+ " <div class='card-title activator grey-text text-darken-4 " 
											+"	 icon_menu_more display_bo'>  "
							+ "</div>"
							+ "</div>"
					
						+ "<div class='card-reveal'>"
						  			+ " <div class='card-title grey-text text-darken-4 " 
											+"	 icon_close display_bo'>  "
									+ "</div>"
  							+ "<span id='bizorder_extra'>Order details are not available</span>"
 							+ "</div>"

						+ "<div id='bizorder' class='card-content'>"
							+ $progress_clock
							+ "</div>"

						+ "<div id='bizorder_action' class='card-action' >"
							+ $div_btn_make_bill
						+ "</div>"	
					+ "</div>"
		+ "<div>";

	return $gos;	
	
}

/*
*
* FX: dl_display_cart_section:* display Cart Section 
*
*/

function dl_display_cart_section()
{
	var $gos = "<a name='mag_cart'></a>"
					+ "<div class='row section' >"
					+ "	<div class='card-panel'>"
					+ "<h4 >Cart<span class='cart_count'></span></h4>"
					+ "<div id='cart'>"
					+ "</div>"
					+ $div_btn_finalize
					+ "</div><div>"
					+ "</div>";

	return $gos;	
	
}

/*
* Decrement Button for Ordered Product
* 
*/

$('body').on('click', '.btn_decrement_oitem', function ()
 	{
     	//$(this).closest("div").css( "background-color", "red" );
     	//$(this).closest("div").find('.item_unit').css( "background-color", "white" );
     	var $div_unit		= $(this).closest("div").find('.item_unit');
     	var $vx				= parseInt($div_unit.text());
     	if($vx > 0 )
     	{
		     	var $vx_new = $vx-1;
		     	$div_unit.text($vx_new);
     	}
		di_is_btn_bill_order('order');
	}
	
);


/*
* Increment Button for Ordered Product
* 
*/

$('body').on('click', '.btn_increment_oitem', function ()
	{
     	// $( ".btn_increment" ).css( "backgroundColor", "blue" );
     	var $div_unit	= $(this).closest("div").find('.item_unit');
     	var $vx			= parseInt($div_unit.text());
     	var $vx_new		= $vx+1;
     	$div_unit.text($vx_new);
		di_is_btn_bill_order('order');
	}
	
);

/*
* Mustache template Ordered Item
* Used in Diner/Order: #cart
* use this in #bizorder also.
*/
var $mst_order_item = 
				"<div class='row cart_row' > "
					+ "<div class='item_pc' hidden id='carti_pc{{product_code}}'" 
									+ " >{{product_code}}</div>"
					+ "<div class='item_name col l6 m12 s12 black-text' " 
									+ " >  {{product_name}}</div>"
					+"<div class='col l6 m12 s12'>"
					 + "<ul class='button-group row'>"

    	  				+ "<li class='btn center white border col s3 "
    	  							+"red-text waves-effect waves-purple btn_decrement_oitem'>"
	    	  						+ "<span class='hide-on-small-and-down '> <b class='material-icons icon_decrement'></b> </span>"
	    	  						+ "<span class='hide-on-med-and-up'> <b class='material-icons  icon_decrement flow-text'></b> </span>"    	  							
    	 	 					+"</li>"
    	  			   + "<li class='btn white black-text mui-btn--flat item_unit col s4' "
    	  						+ " id=carti_unit{{product_code}}"   
    	  						+ ">"
    	  						+ "{{units}}"
    	  				+ "</li>"    	  				 
    	  				+ "<li class='btn center white border col s3 green-text"
   	 	  						+"waves-effect waves-green btn_increment_oitem'>"
	    	  						+ "<span class='hide-on-small-and-down'><b class='material-icons icon_increment'></b>  </span>"
	    	  						+ "<span class='hide-on-med-and-up'> 	 <b class='flow-text icon_increment'></b> </span>"
    	  					+"</li>"
    	  					
    	  			 +"<ul>"
					+ "</div>"

				+ "</div>";

var $mst_order_item_ro = 
				"<div class='row cart_row' > "
					+ "<div class='item_pc' hidden id='carti_pc{{product_code}}'" 
									+ " >{{product_code}}</div>"
					+ "<div class='item_name col l6 m6 s12 black-text' " 
									+ " >  {{product_name}}</div>"
					+"<div class='col l4 m6 s12'><ul class='button-group'>"
    	  			   + "<li class='btn white red-text mui-btn--flat item_unit' "
    	  					+ " id=carti_unit{{product_code}}"   
    	  					+ ">"
    	  					+ "x {{units}}"
    	  					+ "</li>"    	  				 
	  					+"<ul>"
					+ "</div>"
				+ "</div>";


/*
*
* FX: dl_display_bizorder: Display An Order
* Make it better.
* @params: data
*
*/
function dl_display_bizorder(datafc)
{
	 	var $fitems = [];
		var $aitems = [];
  		$('#bizorder').empty(); 
  		$('#bizorder_extra').empty();
		//$( "#bizorder" ).append("Debugging Latency: 1. Empty.")

		var $customerid		= datafc.customerid;
		var $order_discount	= datafc.order_discount;
		var $disc_userid	   = datafc.discount_userid;
		var $userid	   		= datafc.userid;		
		var $billid				= datafc.billid || 0;
		var $is_cancelled		= datafc.cancelled;
		var $bizorderid		= datafc.orderid;
		var $bizorderdate		= datafc.bizorderdate;		

		var $bill_msg;		
		if($billid > 0)
		{
			var $bill_msg = "Bill has already been created.";
			Materialize.toast($bill_msg, 2000);
		}
		else if($is_cancelled)
		{
			var $str_msg = "Order has been cancelled.";
			Materialize.toast($str_msg, 2000);
			bx_display_error(0,$str_msg);
		}
		else
		{
			$( "#bizorder" ).append($progress_clock);
		}

		if($bizorderid)
		{
			var $msg_oid = "<p>OID: " + $bizorderid 
					+ " (served by: " +  $userid +")" + "</p>" 
					+ "<p> Time : " +  $bizorderdate  + "</p>";
			$( "#bizorder_extra" ).append($msg_oid);
		}		
		if($order_discount)
		{
			var $msg_disc = "<p>Discount: " + $order_discount + " (by: " +  $disc_userid +")" +"</p>";
			$( "#bizorder_extra" ).append($msg_disc);
		}		
		if($customerid)
		{
			var $msg_client = "<p>Customerid: " + $customerid +"</p>";
			$( "#bizorder_extra" ).append($msg_client);
		}		
		
		var $count_items = 0;

		var $mst_cu = "<div class='row' > "
						+ "<div class='col s6 bizorder_item_code black-text'>  {{product_name}} </div>"
						//+ "<div class='col s3 bizorder_item_price'> @ {{ price }}</div>"
						+ "<div class='col s3 bizorder_item_unit'>  x {{units}}</div>"
						+ "</div>";
  		//Mustache.parse($mst_cu);
  		var $mst_oitem;
  		if($is_cancelled || $billid)
  		{
  			$mst_oitem = $mst_order_item_ro;
  			$("#bizorder_menu").remove();
  		}
  		else
  		{
  			$mst_oitem = $mst_order_item;
  		}
		Mustache.parse($mst_oitem);
		//$( "#bizorder" ).append("3. Template Parsed. ")

		$aitems = datafc.items;	
	   $.each( $aitems, function( index, val ) 
	   {
			//$( "#bizorder" ).append("/3A.");
			var $xe;
			if(val)
			{
					//var $xe = Mustache.to_html($mst_cu, val);
					var $xe = Mustache.to_html($mst_oitem, val);
					$fitems.push($xe);
					$count_items = $count_items + 1;
			}	
  		});
 		//$( "#bizorder" ).append("4. Items drawn.");

  $('#bizorder').empty();   

  
  $( "<div/>", 
  {
    "class": "",
    html: $fitems.join( "" )
  }).appendTo( "#bizorder" );

  //$( "#bizorder" ).append("5. Added to Div. ");

  //Make_Bill is active, Finalize Inactive
	
  var $bizorder_items = $('.item_pc').length;

  if($count_items > 0)
  {
	   di_is_btn_bill_order('bill');
  }		
  else
  {
  		di_is_btn_bill_order();
		var $span_noitem = "<span id='bizorder_no_item'>No Items in the order.</span>" 
		$("#bizorder").append($span_noitem);
  }

	//Add Go To Bill Button  
	if($billid > 0)
	{
		$( "#x_bill" ).remove();//Contains the Make Bill button
		var $fn_click = "onClick=dx_move_bill('"+$billid +"');";
		var $bill_btn = "<a class='btn large red white-text' id='abill' "
								+ $fn_click +" >" 
								+ "Go to Bill " +  "</a>";
		$( "#bizorder" ).append($bill_btn);												
	}  
	else if($is_cancelled)
	{
		$( "#x_bill" ).remove();//Contains the Make Bill button
		var $x_btn = "<span class='white red-text'  ><b>"
								+ "Order is cancelled: " +  $is_cancelled +"</b></span>";
		$( "#bizorder" ).append($x_btn);												
	}
  	

  //$( '.order_count' ).html("");
  //$( '.order_count' ).append( '['+$bizorder_items+']');
  //$( "#bizorder" ).append("6. End ");
}

/*
* FX: dl_order_push_in_cart: Push Product in the Cart
* Writes: DOM
* @params: $msg (Product_code)
*/
function dl_order_push_in_cart(msg)
{
    	  var $pc = msg ;//arguments[0];
    	  var $pname;
    	  $pname 				= $( "#item_"		+ $pc +"" ).text();
    	  var $cart_pc 		= $( "#carti_pc"	+ $pc +"" ).text();
		  var $cart_unit 		= $( "#carti_unit"+ $pc +"" ).text() || 1;
		  var $div_iunit 		= "carti_unit"		+ $pc;
		  var $toast_time 	= "1000";

		  //var $menu_cart_units_id   = "#sym_pcu_" + $pc;


		  var $h_item = { 'product_code':	$pc,
								'units':				$cart_unit,
								'name':				$pname,
								'product_name':	$pname,
							 };

		  if($cart_pc == $pc)
		  {
		  		//Exists in The Cart Already, then Increment
		  	   $xunits = g_incrementInput($div_iunit);
				var $xmsg = "Already Added to Cart.";
				if($xunits >= 0 )
				{
					var $xmsg = "Adding more units: " + $xunits + " ";
					//var $str_xunits = "+"+$xunits;
					//$($menu_cart_units_id).text($str_xunits);
				}

		  	   Materialize.toast($xmsg, $toast_time); 
		  }    	  
    	  else
    	  {
    	  		var $here_count = "#here_count_" + $pc;
	
	   		var $moi_x = Mustache.to_html($mst_order_item, $h_item);
				$( '#bizorder' ).append( $moi_x );

				/*
    	  		var $div_pc =	"<div class='item_pc' hidden"
    	  										+ " id=carti_pc"+ $pc 
    	  											+ ">"+ $cart_pc + $pc 
									+ "</div>";

    	  		var $div_pn = "<div class='col m5 item_name'>" + $pname + "</div>";

    	  		var $div_units = 
  	  			"<div class='col m6'> <ul class='button-group'>"
    	  				+ "<li class='btn center orange waves-effect waves-light btn_decrement'>"
    	 	 				+ "<b>-</b></li>"
    	  			   + "<li class='btn white black-text mui-btn--flat item_unit' "
    	  					+ " id=carti_unit" + $pc  
    	  					+ ">"
    	  				+ 1
    	  					+ "</li>"    	  				 
    	  				+ "<li class='btn center green waves-effect waves-light btn_increment'>"
    	  					+ "<b>+</b></li><ul>"
					+ "</div>";

    	  		var $div_all = "<div class='row cart_row' >" + $div_pc+$div_pn+$div_units + "</div>";

    	  		$( '#cart' ).append( $div_all ); */

  				var $cart_items = $('.cart_row').length;
  				$( '.cart_count' ).html("");
  				$( '.cart_count' ).append( '['+$cart_items+']');

				//Only Send Toast(popups)
    	  		Materialize.toast('Added to Cart', $toast_time);
				//var $str_xunits = "+1";
				//$($menu_cart_units_id).text($str_xunits);

    	  }

			di_is_btn_bill_order('order');
			$("#bizorder_no_item").remove();
 	
};
// Push in Cart.
