/*
	 2016-12-09
	 Reports.js, JS Utilities for BAEL's Reporting

    This file is part of BAEL
    Copyright (C) 2016,  Tirveni Yadav
    @author: Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/

/*
* @dependency: bael.js, Moustache, Jquery, MaterializeCSS.
*
*/


/*-----------------------------Constant Variables----*/
//API Urls
var $rv_api_daystotal		= "/corp/report/daystotal";
var $rv_api_purchases		= "/corp/report/purchases";
var $rv_api_daystaxestotal = "/corp/report/daystaxestotal";
var $rv_api_total_dnotes		= "/corp/report/total_dnotes";
var $rv_api_total_cnotes		= "/corp/report/total_cnotes";


/* -----------------------------Functions------------- */

/*
*
* FX: r_daystotal: Get the JSON/xml data for a the Report: DaysTotal
* @Params: branchid,date
*
* From Div: BranchID(in_branchid),Date(in_date,optional,default:today)
* Fills in the data in the div
* Appends the data to #div_report
*/

function r_daystotal()
{
	var $in_branchid = bgx_get_branchid();
	var $slash = "/";
		
	//#tabMenu
	var $div_tm = "#div_report";
   $($div_tm).empty();
   
	var $div_id_msg = "Please wait while the data is fetched....";
	//$( "#xyz" ).append($in_groupid);
	try 
	{
		progress_append($div_tm,$div_id_msg);
	}
	catch(err)
	{
		$($div_tm).append(err);
	}

	//Url of the Report
	var $url = $rv_api_daystotal+ "/"+ $in_branchid;

	// get the date
	var $in_date = $('#in_date').val() || $('#in_date').data('value');
	if($in_date)
	{
		$url = $url + "/" + $in_date;
		$($div_tm).append($in_date);
	}
	
	if($in_branchid)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
 						error: function (xhr,status,error) 
 						{
 							var $me = "<span class='red-text'>No Data for "
 								+ $in_groupname + "</span>";
	 						$($div_tm).empty();
 							$($div_tm).append($me);
            		},
						success: function( data )
						{
							var $sum = data.sum;
							var $name = data.name;
							var $xdate = data.date;
							g_choosen_date($xdate);
							$($div_tm).empty();
		  					$($div_tm).append( "<b>"+$name + " </b> (" + $xdate + "): " + $sum + ".");
						},
					}	
		);//Ajax
		
	}//IF 

	
};


/*
*
* FX: r_purchases: Get the JSON/xml data for a the Report: DaysTotal
* @Params: branchid,date
*
* From Div: BranchID(in_branchid),Date(in_date,optional,default:today)
* Fills in the data in the div
* Appends the data to #div_report
*/

function r_purchases()
{
	var $in_branchid = bgx_get_branchid();
	var $slash = "/";
		
	//#tabMenu
	var $div_tm = "#div_report";
   $($div_tm).empty();
   
	var $div_id_msg = "Please wait while the data is fetched....";
	//$( "#xyz" ).append($in_groupid);
	try 
	{
		progress_append($div_tm,$div_id_msg);
	}
	catch(err)
	{
		$($div_tm).append(err);
	}

	//Url of the Report
	var $url = $rv_api_purchases + "/"+ $in_branchid;

	// get the date
	var $in_date = $('#in_date').val() || $('#in_date').data('value');
	if($in_date)
	{
		$url = $url + "/" + $in_date;
		$($div_tm).append($in_date);
	}
	
	if($in_branchid)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
 						error: function (xhr,status,error) 
 						{
 							var $me = "<span class='red-text'>No Data for "
 								+ $in_groupname + "</span>";
	 						$($div_tm).empty();
 							$($div_tm).append($me);
            		},
						success: function( data )
						{
							var $sum = data.total;
							var $name = data.name;
							var $xdate = data.date;
							g_choosen_date($xdate);
							$($div_tm).empty();
		  					$($div_tm).append( "<b>"+$name + " </b> (" + $xdate + "): " + $sum + ".");
						},
					}	
		);//Ajax
		
	}//IF 

	
};


/*
* FX: r_daystaxestotal: Gets the Days taxes total
* @params: none
* 
* Appends the data to #div_report
*/

function r_daystaxestotal()
{
	var $in_branchid = bgx_get_branchid();
	var $slash = "/";
		
	//#tabMenu
	var $div_tm = "#div_report";
   $($div_tm).empty();
   
	var $div_id_msg = "Please wait while the data is fetched....";
	//$( "#xyz" ).append($in_groupid);
	try 
	{
		progress_append($div_tm,$div_id_msg);
	}
	catch(err)
	{
		$($div_tm).append(err);
	}

	//Url of the Report
	var $url = $rv_api_daystaxestotal+ "/"+ $in_branchid;

	// get the date
	var $in_date = $('#in_date').val() || $('#in_date').data('value');
	if($in_date)
	{
		$url = $url + "/" + $in_date;
		$($div_tm).append($in_date);
	}
	
	if($in_branchid)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
 						error: function (xhr,status,error) 
 						{
 							var $me = "<span class='red-text'>No Data for "
 								+ $in_groupname + "</span>";
	 						$($div_tm).empty();
 							$($div_tm).append($me);
            		},
						success: function( data )
						{
							var $sum = data.sum;
							var $name = data.name;
							var $xdate = data.date;
							g_choosen_date($xdate);
							$($div_tm).empty();
		  					$($div_tm).append( "<b>"+$name + "</b><div class='row' id='list_taxes'></div> ");
		  					r_list_taxes(data,"#list_taxes");
						},
					}	
		);//Ajax
		
	}//IF 
	
};


/* Moustache Template for List Taxes Total */
var $mst_list_atax =  "<div class='col l6 m6 s6'>{{tax}}</div>"
							+"<div class='col l6 m6 s6'>{{sum}}</div>"
							;

/*
*
* FX: r_list_taxes: Appends to div_parent
* @params: data, div_parent
* @return: none, 
* 
*
*/

function r_list_taxes(data,$div_par)
{
 	
  $.each( data.taxes, function( key,xval )
  {
			var $div_atax = Mustache.to_html($mst_list_atax, xval);
			$( $div_par).append($div_atax);
  }); 	   
	
}

/*
*
* FX: r_total_dnotes: Get the JSON/xml data for a the Report: DaysTotal
* @Params: branchid,date
*
* From Div: BranchID(in_branchid),Date(in_date,optional,default:today)
* Fills in the data in the div
* Appends the data to #div_report
*/

function r_total_dnotes()
{
	var $in_branchid = bgx_get_branchid();
	var $slash = "/";
		
	//#tabMenu
	var $div_tm = "#div_report";
   $($div_tm).empty();
   
	var $div_id_msg = "Please wait while the data is fetched....";
	//$( "#xyz" ).append($in_groupid);
	try 
	{
		progress_append($div_tm,$div_id_msg);
	}
	catch(err)
	{
		$($div_tm).append(err);
	}

	//Url of the Report
	var $url = $rv_api_total_dnotes + "/"+ $in_branchid;

	// get the date
	var $in_date = $('#in_date').val() || $('#in_date').data('value');
	if($in_date)
	{
		$url = $url + "/" + $in_date;
		$($div_tm).append($in_date);
	}
	
	if($in_branchid)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
 						error: function (xhr,status,error) 
 						{
 							var $me = "<span class='red-text'>No Data for "
 								+ $in_groupname + "</span>";
	 						$($div_tm).empty();
 							$($div_tm).append($me);
            		},
						success: function( data )
						{
							var $sum = data.total;
							var $name = data.name;
							var $xdate = data.date;
							g_choosen_date($xdate);
							$($div_tm).empty();
		  					$($div_tm).append( "<b>"+$name + " </b> (" + $xdate + "): " + $sum + ".");
						},
					}	
		);//Ajax
		
	}//IF 

	
};


/*
*
* FX: r_total_cnotes: Get the JSON/xml data for a the Report: DaysTotal
* @Params: branchid,date
*
* From Div: BranchID(in_branchid),Date(in_date,optional,default:today)
* Fills in the data in the div
* Appends the data to #div_report
*/

function r_total_cnotes()
{
	var $in_branchid = bgx_get_branchid();
	var $slash = "/";
		
	//#tabMenu
	var $div_tm = "#div_report";
   $($div_tm).empty();
   
	var $div_id_msg = "Please wait while the data is fetched....";
	//$( "#xyz" ).append($in_groupid);
	try 
	{
		progress_append($div_tm,$div_id_msg);
	}
	catch(err)
	{
		$($div_tm).append(err);
	}

	//Url of the Report
	var $url = $rv_api_total_cnotes + "/"+ $in_branchid;

	// get the date
	var $in_date = $('#in_date').val() || $('#in_date').data('value');
	if($in_date)
	{
		$url = $url + "/" + $in_date;
		$($div_tm).append($in_date);
	}
	
	if($in_branchid)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
 						error: function (xhr,status,error) 
 						{
 							var $me = "<span class='red-text'>No Data for "
 								+ $in_groupname + "</span>";
	 						$($div_tm).empty();
 							$($div_tm).append($me);
            		},
						success: function( data )
						{
							var $sum  = data.total;
							var $name  = data.name;
							var $xdate = data.date;
							g_choosen_date($xdate);
							$($div_tm).empty();
		  					$($div_tm).append( "<b>"+$name + " </b> (" + $xdate + "): " + $sum + ".");
						},
					}	
		);//Ajax
		
	}//IF 

	
};
