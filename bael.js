/*
	 2016-12-25
	 Bael.js, JS Utilities for BAEL

    This file is part of BAEL
    Copyright (C) 2016,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Only Orders List, Bill Payment Functions (which are common for all apps) are here.

*/

/*
Requirement: Jquery,Moustache, MaterializeCSS.

*/



/*
* Trim Date to 10 digits: yyyy-mm-dd
* Div function
*/
$('.trim_date').each(function()
   	{
   		var $div_value = $(this).text();
   		$div_value = $div_value.trim().substr(0, 10);
   		$(this).text($div_value);
   	}
);


/***********Ajax Search**********/
/*
* City Ajax Search Autocomplete   
* $("#icity_autocomplete").easyAutocomplete(icity_autocomplete_options);
*      <input id="icity_autocomplete" type="text" class="validate" name="city">
*/
var icity_autocomplete_options = 
{

	 dataType: 			'json',
	 listLocation: 	'cities',

    url: function(phrase)
    {

		  var $in_country = 	$("#icountry").val();
		  var $xcountry;
		  var $s_url = "/asearch/cities?q=";	
		  if($in_country && $in_country != 'undefined')
		  {
		  		$xcountry = $in_country;
		  }
         
        if (phrase !== "" && phrase.length > 2)
        {
            return $s_url + phrase + "&format=json&" + "icountry=" + $xcountry ;    
        } 
        else 
        {
            return "/asearch/cities";
        }
    },
	
	getValue: function(element)
	{
        return element.name;
   },
    
    template: {
        type: "custom",
        method: function(value, item) 
        {
        	   return "<div class='green-text' data-item-value_citycode='" 
        	   		+ item.code + "' >" 
        	   		+ value + "</div>";
            
        }
    },
    list: {
        onSelectItemEvent: function() 
        {

            var selectedItemValue = 
            $(".easy-autocomplete").
            	find("ul li.selected div[data-item-value_citycode]").attr("data-item-value_citycode");
            //$("#xr").css( "backgroundColor", "red" );	
            //$("#sin_citycode").val("ABC");	
            $("#sin_citycode").val(selectedItemValue).trigger("change");
        	
        	
           /* var selectedItemValue = 
            $(".easy-autocomplete").
            	find("ul li.selected div[data-item-value]").attr("data-item-value");
            $("#input_two").val(selectedItemValue).trigger("change"); */
        }
    }
};

//Currency AJax Search
var icurrency_autocomplete_options = {
    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
            return "/asearch/currencies/" + 
            	phrase ;    
        } 
    },
	
	getValue: function(element)
	{
		  var $xname = element.currencyname; 
        return $xname;
   },
    
    template: {
        type: "custom",
        method: function(value, item) 
        {
        	   return "<div class='green-text' data-item-value_currcode='" 
        	   	+ item.currencycode + "' >" 
        	   	+ value + "</div>";
            
        }
    },
    list: {
        onSelectItemEvent: function() 
        {

            var selectedItemValue = 
            $(".easy-autocomplete").
            	find("ul li.selected div[data-item-value_currcode]").attr("data-item-value_currcode");
            $("#sin_currencycode").val(selectedItemValue).trigger("change");

        }
    }
};

/* TimeZone AjaxSearch
 $("#izone_autocomplete").easyAutocomplete(izone_autocomplete_options);
		<input id="izone_autocomplete" type="text" class="validate" name="city"
      					value="" style="width: 220px;" />
 	   <input id="sin_zoneid" type="text" name="sin_zoneid" />
 
 */
var izone_autocomplete_options = {
    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
            return "/asearch/timezones?q=" + 
            	phrase ;    
        } 
    },
	
	getValue: function(element)
	{
		  var $xname = element.zonename; 
        return $xname;
   },
    
    template: {
        type: "custom",
        method: function(value, item) 
        {
        	   return "<div class='green-text' data-item-value_zoneid='" 
        	   	+ item.zoneid + "' >" 
        	   	+ value + "</div>";
            
        }
    },
    list: {
        onSelectItemEvent: function() 
        {

            var selectedItemValue = 
            $(".easy-autocomplete").
            	find("ul li.selected div[data-item-value_zoneid]").attr("data-item-value_zoneid");
            $("#sin_zoneid").val(selectedItemValue).trigger("change");

        }
    }
};
/***********Ajax Search END **********/

//************* 2016-03-14 RestPagination

/*
* Rest Pagination Increase Button Increment
* @returns: Integer
* @params: integer
* FX: rpage_decrement
*/
function rpage_decrement(inx)
{
	 var $in_div = inx || $rpage_current;
    var $y = $($rpage_current).val();
    $ya = $y - 1;
    var $str = $ya;
    if($ya < 1)
    {
    		$str = 1;
    }
	 $($in_div).html("");		
	 $($in_div).val($str);

    var $fnx_count = $('#fnx_rest_item_count').val() || 10;	 
    var $fnx = $('#fnx_rest').val();
    var $xrd = eval( $fnx + "(" + $fnx_count + "," + $str +")" );    
    //("#api_page_current").val($str);
	 //var $xrd = window[$fnx]($fnx_count,$str);
	 //$("#cxt").append($xrd);	 	
	 return $str;	 	

}


/*
* FX: rpage_increment
* Rest Pagination Decrease
* @returns: Integer
* @params: integer
* Calls the Function in #fnx_rest, increments the #api_page_current
*/
function rpage_increment(inx)
{
	 var $in_div = inx || $rpage_current;
    var $y = $($in_div).val();
    $ya = $y++;
    var $str = $y;
    if($ya < 1)
    {
    		$str = 1;
    }

    //Set Current Page
	 $($in_div).html("");		
	 $($in_div).val($str);

	 
    var $fnx_count = $('#fnx_rest_item_count').val() || 10;	 
    var $fnx		 = $('#fnx_rest').val();
    
    //Call the Abstract function
	 var $xri		 = eval( $fnx + "(" + $fnx_count + "," + $str +")" );
	 return $str;	 
	 //$("#cxt").append($xri);	 	
}

/*
* Rest Page reset
* FX: rpage_reset
*/
function rpage_reset(inval)
{
	 if(inval == 0)
	 {
	 	$str = 0;
	 }
	 else
	 {
	 	$str = 1;
	 } 	 
	 $($rpage_current).html("");
	 $($rpage_current).val($str);		

}

/*
* Rest Page Data Test Display
* FX: rpage_display(datax)
*/
function rpage_display(datax)
{
		var $rest_pg_txt = "#rest_pagination_text";
      var $idx_first = datax.idx_first;
      var $idx_last = datax.idx_last;
      var $idx_str = $idx_first + "-" + $idx_last;
      $($rest_pg_txt).empty();
      $($rest_pg_txt).append($idx_str);
	
}

/*
* Hide a Div Class
* FX: bx_div_class_hide(div_class)
*/
function bx_div_class_hide($in_class)
{
	 if ($in_class)
	 {
        $($in_class).hide();
    } 
	
}

/*
* Display a Class, which is hidden
* FX: bx_div_class_show(div_class)
*/
function bx_div_class_show($in_class)
{
	 if ($in_class)
	 {
        $($in_class).show();
    } 
	
}



/*
* div function button decrement.
*
* Cart: Item Increment,Decrement 
* Used inDiner/Order Operation 
*/
$('body').on('click', '.btn_decrement', function ()
 	{
     	//$(this).closest("div").css( "background-color", "red" );
     	//$(this).closest("div").find('.item_unit').css( "background-color", "white" );
     	var $div_unit = $(this).closest("div").find('.item_unit');
     	var $vx = parseInt($div_unit.text());
     	if($vx > 0 )
     	{
		     	var $vx_new = $vx-1;
		     	$div_unit.text($vx_new);
     	}
	}
);

/*
* div function button increment
*
* Cart: Item Increment,Decrement 
* Used inDiner/Order Operation 
*/
$('body').on('click', '.btn_increment', function ()
	{
     	// $( ".btn_increment" ).css( "backgroundColor", "blue" );
     	var $div_unit = $(this).closest("div").find('.item_unit');
     	var $vx = parseInt($div_unit.text());
     	var $vx_new = $vx+1;
     	$div_unit.text($vx_new);
	}
);


/*
* FX: g_match_passwords(): For Adding new user or Passord change
*
* #register_passwordy,#register_passwordx
* 
*/
function g_match_passwords()
{

	var $pass_a = $("#register_passwordx").val();
	var $pass_b = $("#register_passwordy").val();	
	bx_toast_empty();

	if(!$pass_a || !$pass_b)
	{
		Materialize.toast("Both passwords are required.",5000);
	}
	else if($pass_a != $pass_b)
	{
		Materialize.toast("Both passwords should be same.",5000);
	}
			
}



/*
* Clear all intervals.
* FX: bx_clear_all_intervals.
*
*/
function bx_clear_all_intervals()
{
	    for (var i = 1; i < 99999; i++)
        window.clearInterval(i);
        window.clearTimeout(i);
}

/*
*
* FX: g_timezones_For_country:
* 
* Takes country_code:state_code:city_code, extracts country_code: sin_citycode
* Uses Country_code for getting the timezones of a country.
*
* Creates DropDown Box for Timezones.
*
*	<select id='dropdown_tzone' class='select_tzone '  name='sin_zoneid'>
*			<option >Select </option>
*		</select><span id='tz_progress'></span>
*
* Ajax API: /asearch/timezones
*
*/
function g_timezones_for_country()
{
	var $ci_st_co = $( "#sin_citycode" ).val();

	var $div_dropdown = "#dropdown_tzone";	
	var $div_progress = "#tz_progress";
	
   $($div_dropdown).material_select('destroy');
   
   //Materialize Select Destroy
   
   var $progress = $progress_clock;
	$($div_progress).append($progress);
	
	var $url = "/asearch/timezones";
	var $search_url = $url+ "?tz_city=" + $ci_st_co;   
   
	$.getJSON( $search_url, function( data ) 
	{
	   $($div_dropdown).html("");
	   var $xselect = "Select";
	  	$cn= "<option>" + $xselect + "</option>";
		  
	   var $count = 0;

	   $.each( data, function( key,xval ) 	   
	   {
	   	$count++;

	  	   var $zoneid     = xval.zoneid ;
	  	   var $zname      = xval.zonename ;
	  	   
	  	   $f_b = "<option value='" + $zoneid + "'>";
	  	   $f_e = "</option>"; 
	  	   var $xe = $f_b+ $zname +$f_e;
	   	$($div_dropdown).append($xe);

  		});//ForEach   
  		
    $($div_dropdown).material_select();//Materialize Select
	  	     
	});//Function
	
	$($div_dropdown).html("");//Progress Indicator
	$($div_progress).empty();
	
};//Function


/*** FX:: Standard Utilities: Begin ****/

/*
* FX: bgx_get_appid: 
* Get AppID
*
*/
function bgx_get_appid()
{
	var 		$appid		= $('#in_appid').val();
	return 	$appid;	

}

/*
* FX: bgx_get_bizrole:
* Get Bizrole 
*
*/
function bgx_get_bizrole()
{
	var 		$bizrole 	= $('#in_bizrole').val();
	return 	$bizrole;	

}

/*
* FX: bgx_get_branchid
* Get BranchID
*
*/
function bgx_get_branchid()
{
	var 		$branchid =   $( "#in_branchid" ).val();
	return 	$branchid;
}

/*
* FX: bgx_get_currencycode
* Get BranchID
*
*/
function bgx_get_currencycode()
{
	var 		$xval =   $('#currencycode').val() || $('#currency_code').val(); 
	return 	$xval;
}


/*
* FX: bgx_get_billid:
* Get BillID
*
*/
function bgx_get_billid()
{
	var 	 $billid = $( "#billid" ).text() || $( "#billrid" ).val() || $( "#billid" ).val();
	return $billid;
}

/*
* FX: bgx_get_bill_txnid: GET bills' settled Txn  ID:
* Get TxnID
*
*/
function bgx_get_bill_txnid()
{
	var 	 $xid = $( "#bill_settled_txnid" ).text() || $( "#bill_settled_txnid" ).val();
	return $xid;
}



/*
* FX: bgx_get_bizorderid:
* Get OrderID
*
*/

function bgx_get_bizorderid()
{
	var 	 $bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();
	return $bizorderid;
}

/*
* FX: bgx_access_allowed:
*
* Input: example bill_cancel
* Checks for higher authority or role.
*
*/
function bgx_access_allowed(in_resource)
{
	var $is_allowed	= 0;
	var $bizrole		= bgx_get_bizrole();
	
	if( $bizrole == 'MANAGER' || $bizrole == 'BOSS' )
	{
			$is_allowed = 1;
	}
	
	return $is_allowed;
	
}

/*
* FX: bgx_get_userid: From DOM
*
*
*/
function bgx_get_userid()
{
	var 		$userid =   $( "#in_userid" ).val();
	return 	$userid;
}


/*
*
* FX: bx_fetch_userid():  Gets the UserID through AJAX.
* To Store it somewhere. As this is Async.
* 
*/
function bx_fetch_userid()
{
	var $base_url = '/g/user';
	
	if($base_url)
	{
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"GET",
						dataType: 	"json",
						success: function( xdata )
						{
							return xdata.userid;
						},
				}	
		);
	}
}

/*** FX:: Bill Utilites BEGINS ****/

/*
*
* MST: Bill Search
*
*/
var $mst_bills_search = "<div class='row border'>"
		 + "<ul class='button-group '>"
				+ "<li class='col l3 m5 s12'>"
					+ "<input type='date' id='in_date' class='datepicker xindate' name='search_date' " 
					+	"	value='' placeholder='YYYY-MM-DD'> "
      			+ "</li>"
				+ "<li class='col l3 m3 s6'>"
				+ "	<input type='text' name='search_orderid' id='i_orderid' " 
				+		 "placeholder='Order'>"
      		+ "</li>"
      	   + "<li class='col l3 m3 s6'>"
     					+ "<button name='submit' onClick='{{method_bill}}' "	
     				 	+ " class='btn orange' >Search</button>"
      	   + "</li>"
      + " </ul>"	   
+ "</div>";

/*
* Returns div for bill search
* FX: g_bills_search: Search Bill for Diner/Retail only.
* @params: appid
* Fetch vals: #i_orderid, and use g_choosen_date
*
*/
function g_bills_search($appid)
{
		var $v_method = "dl_display_diner_allbills(10,1);";
		
		if($appid == $c_appid_retail)
		{
			$v_method = "rl_display_allbills(10,1)";
		}
		else if($appid == $c_appid_acquire)
		{
			$v_method = "mb_po_display_allbills(10,1)";
		}
		
		var $h_sx;
		$h_sx = {method_bill:$v_method};
		var $div_search = Mustache.to_html($mst_bills_search, $h_sx);		
		
		return $div_search;
}


/*
Bill Page
*/

/*
* FX: bx_bill_details
* @returns a Div id:#idprint, which itself has id#list_of_bill_items.
* @bug: #81, is it related to here.
*
*/
function bx_bill_details()
{
	var $abc;
	$abc = "<div id='idprint'>	<div id='list_of_bill_items'>" 
				+ $progress_clock 	
			 	+"</div></div>";
			 	
	return $abc;		 		
}

//Bill Header
var $mst_bill_header = 
			"<ul class='row collection z-depth-1 yellow lighten-5 ' "
					+	">"
						+"<div class='collection-item row'>"
	  		 					+ 		"<span class='black-text col l12 m12 s12 left'>"
	  		 					+ 		"{{branch_name}}" 
	  		 					+		" [{{business_name}}]" 
 	  		 					+ 		"</span>"
								+"</div><div class='collection-item row'>"
	  		 					+ 		"<span class='black-text col l12 m12 s12 left'>"
	  		 					+		"{{address}}"  
 	  		 					+ 		"</span>"
								+"</div><div class='collection-item row'>"
							
								+"<div class='collection-item row'>"

	  		 					+ 		"<span class='black-text col l6 m6 s6 left'>"
	  		 					+ 		"B-{{billnumber}}"  
	  		 					+ 		"</span>"
	  		 					+ 		"{{{str_txnid}}}"
	  		 					+ 		"<span class='black-text col l6 m6 s6'>"
	  		 					+ 		"Bill Date: {{billdate}}"  
	  		 					+ 		"</span>"
								+"</div>"
								
								+"<div class='collection-item row'>"
	  		 						+ 		"<span class='black-text col l6 m6 s6'>"
	  		 						+ 		"O-{{days_order}}"  
	  		 						+ 		"</span>"
									+ 		"<span class='col l6 m6 s6'>"
									+ 			"{{{order_btn}}} "
									+		"</span>"
								+ "</div>"	

								+ "{{#order_discount}}"
									+"<div class='collection-item row'>"
	  		 						+ 		"<span class='black-text col l6 m6 s6'>"
	  		 						+ 		"Discount: {{order_discount}} % "  
	  		 						+ 		"</span>"
	  		 						+ "</div>"
	 							+ "{{/order_discount}}"

								+ "{{#purchase_orderid}}"
									+"<div class='collection-item row'>"
		  		 					+ 		"<span class='black-text col l6 m6 s6 left'>"
		  		 					+ 			"{{str_otype}}"  
		  		 					+ 		"</span>"
		  		 					+ 		"<span class='black-text col l6 m6 s6'>"
		  		 					+ 			"{{#purchase_btn}} {{{purchase_btn}}} {{/purchase_btn}}"
		  		 					+ 			"{{#dn_btn}} 		{{{dn_btn}}} 		  {{/dn_btn}}"
		  		 					+ 		"</span>"
									+"</div><div class='collection-item row'>"
								+ "{{/purchase_orderid}}"
									
								+ "{{#po_orderid}}"
									+"<div class='collection-item row'>"
		  		 					+ 		"<span class='black-text col l6 m6 s6 left'>"
		  		 					+ 			"{{str_otype}}"  
		  		 					+ 		"</span>"
		  		 					+ 		"<span class='black-text col l6 m6 s6'>"
		  		 					+ 			"{{#po_btn}} {{{po_btn}}} {{/po_btn}}"  
		  		 					+ 			"{{#dn_btn}} {{{dn_btn}}} {{/dn_btn}}"
		  		 					+ 		"</span>"
									+"</div><div class='collection-item row'>"
								+ "{{/po_orderid}}"	
								

								+ "{{#table_number}}"	
									+ "</div><div class='collection-item row'>"
	  		 						+ 		"<span class='black-text col l6 m6 s6'>"
	  		 						+ 		"T-{{table_number}}"  
	  		 						+ 		"</span>"
	 							+ "{{/table_number}}"
	  		 						
								+ "{{#cancelled}}"
									+ "</div><div class='collection-item row'>"
	  		 						+ 		"<span class='black-text col l6 m6 s6'>"
	  		 						+ 		"Cancelled On: {{cancelled}}" 
	  		 						+ 		"</span>"
	  		 					+ "{{/cancelled}}"
	  		 						  		 					
	  		 					+ "{{#is_delivery}}"
									+ "</div><div class='collection-item row'>"
	  		 						+ 		"<span class='black-text col l12 m12 s12'>"
	  		 						+ 		"Delivery: {{customer_address}}" 
	  		 						+ 		"</span>"
									+ "</div><div class='collection-item row'>"
	  		 						+ 		"<span class='black-text col l12 m12 s12'>"
	  		 						+ 			"Customer Phone: {{customer_phone}}" 
	  		 						+ 		"</span>"
	  		 					+ "{{/is_delivery}}"
	  		 						
	  		 					+ "</div></ul>";

/*
* FX: bx_bill_branch_header: displays bill header, handles CN/BO/DN/PO differences
* Display Branch,Order Info for the Bill
* @returns: DOM(div) String
* uses: $mst_bill_header
*
*/
function bx_bill_branch_header(bi)
{
	var $div_str;

	var $purchase_orderid	= bi.purchase_orderid;
	var $purchase_billid		= bi.purchase_billid;
	var $orderid 				= bi.orderid;
	var $settled_txnid		= bi.settled_txnid;
	var $order_type 			= bi.order_type;
	var $order_discount				= bi.order_discount;	
	var $order_stageid		= bi.order_stageid;
	//Materialize.toast("Stage:"+$order_stageid,3000);
	
	var $appid 					= bgx_get_appid();

	var $fn_click;
	var $order_btn; 	var $purchase_btn;
	var $po_btn;		var $dn_btn;
	var $po_orderid;var $dn_orderid;
	

	var $str_otype;	
	var $str_txnid = '';
	//Materialize.toast("Adding Return Option"+$appid+$settled_txnid,3000);

	//-- Btn/Links to Order OR PO-IDs
	//Btn Default for an BizOrder
	if($orderid)
	{
			$fn_click = "onClick=g_display_existing_order('"+ $orderid +"');"	
			$order_btn = 	"OID-" + $orderid	 
						+ "&nbsp; <a class='btn-floating red white-text center' "+ $fn_click +">"
	  		 			+ 		"&rarr; </a>" ;
	}
	
	//OT and Button			
	if($order_type == $c_order_cn )
	{
		$str_otype = 'Credit Note';
		
		$order_btn = 	"OID-" + $orderid ; 
		var $pr_click = "onClick=rl_abill('"+$purchase_billid +"');";
		$purchase_btn = 	"Purchase OID: " + $purchase_orderid	 
						+ "&nbsp; <a class='btn-floating green white-text center' "+ $pr_click +">"
	  		+ 		"&rarr; </a>" ;

		//bx_more_link_del("#entity_bill_more_cnote");				
		//Materialize.toast("Removing More Return",5000);		
		 	
	}
	else if($order_type == $c_order_po )
	{
		$str_otype  = 'Purchase Order';
		//Materialize.toast("Return for PO",5000);		
		

		$order_btn  = 	"OID-" + $orderid ; 
		$po_orderid = $orderid;
		var $in_div = "#txr";
		var $pr_click = "onClick=mb_apo_txns('"+$po_orderid +"','"+ $in_div +"');";
		$po_btn = 	"PO OID: " + $orderid	 
						+ "&nbsp; <a class='btn-floating green white-text center' "+ $pr_click +">"
	  		+ 		"&rarr; </a>" ;

		bx_more_link_del("#entity_bill_more_cnote");						
		 	
	}
	else if($order_type == $c_order_dn )
	{
		$str_otype = 'Debit Note';
		$order_btn = 	"OID-" + $orderid ;

		$dn_orderid = $orderid;

		var $in_div = "#txr";
		var $pr_click = "onClick=mb_apo_txns('"+$purchase_orderid +"','"+ $in_div +"');";
		$dn_btn = 	"PO OID: " + $purchase_orderid	 
						+ "&nbsp; <a class='btn-floating green white-text center' "+ $pr_click +">"
	  		+ 		"&rarr; </a>" ;

		//Materialize.toast("More Return PO",5000);		
	}

	//Add Input String for Settled
	if($settled_txnid)
	{	
			$str_txnid = "<input id='bill_settled_txnid' type='hidden' value='"
					+ $settled_txnid +"'  /> ";
	}
		
	//More Btn
	var $h_more = {
						order_type:			$order_type,
						appid:				$appid,
						settled_txnid:		$settled_txnid,
						order_stageid:		$order_stageid,
						};
	bx_bill_more_drop($h_more);
	
	
	//Create the Hash for MST
	var $h_bh = {
						branch_name:	bi.branch_name,
						business_name:	bi.business_name,
						address:			bi.address,
						billnumber:		bi.billnumber,
						billdate:		bi.billdate,
						days_order:		bi.days_order,
						table_number:	bi.table_number,
						cancelled:		bi.cancelled,
						order_discount:		$order_discount,

						str_txnid:		$str_txnid,
						str_otype:		$str_otype,
						
						order_btn:		$order_btn,
						
						purchase_orderid 	: $purchase_orderid,
						purchase_btn	  	: $purchase_btn,
						
						dn_orderid			: $dn_orderid,
						dn_btn				: $dn_btn,

						po_orderid			:  $po_orderid,						 	
						po_btn				:	$po_btn,
						
						is_delivery			: bi.is_delivery,
						customer_phone		: bi.customer_phone,
						customer_address	: bi.customer_address,
						customerid			: bi.customerid,						
																			
					};
	var $div_str = Mustache.to_html($mst_bill_header, $h_bh);
	return $div_str;					

}

/*
*
* FX: bx_bill_more: More Button Dropdown options: Email,Print,Partpay,Cancel,Return(CN/PO)
* 
* @params: order_type,appid,settled_txnid
* Used by: bx_bill_branch_header
* 
*/

function bx_bill_more_drop($in_h)
{
	var $order_type 			= $in_h.order_type;
	var $appid		 			= $in_h.appid;
	var $settled_txnid		= $in_h.settled_txnid;
	var $order_stageid		= $in_h.order_stageid;	

	if($appid == $c_appid_foodtakeout && ($order_stageid == 'NEWORDER') )
	{
				bx_more_link_add($mst_fto_delivery_confirm);//Add Confirm Option
				Materialize.toast("Add Confirm Delivery",3000);		
	}
	
	if($appid == $c_appid_retail && $settled_txnid )
	{
			var $allow_cn = 1;
			//Materialize.toast("Adding Return Option For Order",3000);
			
			if($order_type == $c_order_bo)
			{
				bx_more_link_add($mst_link_cnote);
			}
			
			bx_more_link_del("#entity_bill_more_partpay");
			bx_more_link_del("#entity_bill_more_cancel");								

	}
	else if($settled_txnid && ($order_type == $c_order_po))
	{
			bx_more_link_del("#entity_bill_more_partpay");
			bx_more_link_del("#entity_bill_more_cancel");								
			bx_more_link_add($mst_link_po_return);		
			
	}
	else if($settled_txnid )
	{
			//Materialize.toast("Only Settled/",3000);
			bx_more_link_del("#entity_bill_more_partpay");
			bx_more_link_del("#entity_bill_more_cancel");
	}
	else if($order_type == $c_order_cn)
	{
			//Materialize.toast("OT:CN",3000);
			bx_more_link_del("#entity_bill_more_partpay");
			bx_more_link_del("#entity_bill_more_cnote");					
	}
	else if($order_type == $c_order_po)
	{
			bx_more_link_del("#entity_bill_more_partpay");
			bx_more_link_add($mst_link_part_pay);
	}
	else if($order_type == $c_order_dn)
	{
			bx_more_link_del("#entity_bill_more_partpay");
	}
	else
	{
			bx_more_link_del("#entity_bill_more_partpay");
			bx_more_link_add($mst_link_part_pay);
	} 			
	
	return ;
	
}

var $btn_mk_payment = "";

/*
* FX: bx_diner_bill_pay: get the input for Payment. And fwd it relevant payment
* 
* Writes: DOM
* Bill: Accept Payment
* Ajax: PUT
*
*/
function bx_diner_bill_pay()
{
	var $bill_id   	= $( "#billid" ).text() || $( "#billid" ).val();	
   var $amount  		= $( "#inbill_amount" ).val();
	var $api_pay_link = "/restaurant/bill/pay/";
	var $appid		 	= $('#in_appid').val();
	var $branchid		= bgx_get_branchid();
	var $bizorderid	= bgx_get_bizorderid();
	
	var $order_type 	= $("#bill_order_type").val();
	var $pay_type		= $("#payment_type").val() 	 || 'CASH';
	var $message		= $("#full_pay_comment").val() || $("#message").val(); ;
	//Materialize.toast("OT:"+$order_type,2000);
	//return ;
	
	var $cn_orderid;var $po_orderid;var $dn_orderid;
	
	var $h_move;
	$h_move = {
						'order_type':		$order_type,
						'billid'		:		$bill_id,
						'branchid'	: 		$branchid,
						'bizorderid'	:	$bizorderid,
						'amount'		: 		$amount,
						'pay_type'	: 		$pay_type,		
						'message'	: 		$message,
				 };
				 
	var $cn_pay_link = "/plan/credit/settle/";
	var $po_pay_link = "/purchase/bill/pay/";
	var $dn_pay_link = "/purchase/return/settle";
	
	if($order_type == $c_order_cn)
	{
		$cn_orderid = $bizorderid;	
	}
	else if($order_type == $c_order_po)
	{
		$po_orderid = $bizorderid;	
	}	
	else if($order_type == $c_order_dn)
	{
		$dn_orderid = $bizorderid;
	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$api_pay_link = "/pickup/bill/pay/";  	
  	}	
  	else if($appid == $c_appid_retail)
  	{
  		$api_pay_link = "/retail/bill/pay/";  	
  	}	

	var $msg = "BR:"+$branchid+",OID:" + $cn_orderid+".";
	//Materialize.toast($msg,2000);
		
	if($cn_orderid && $order_type && $amount)
	{
		var $msg = "BR:"+$branchid+",OID:" + $cn_orderid+".";
		//Materialize.toast($msg,2000);
		bpv_cn_pay($cn_pay_link,$h_move);			
		
	}
	else if($po_orderid && $order_type && $amount)
	{
		var $msg = "Purchase Order PAY BR:"+$branchid+",OID:" + $po_orderid+".";
		//Materialize.toast($msg,5000);
		bpv_po_pay($po_pay_link,$h_move);			
	}
	else if($dn_orderid && $order_type && $amount)
	{
		var $msg = "Debit Note PAY "+$branchid+",OID:" + $po_orderid+".";
		//Materialize.toast($msg,5000);
		bpv_dn_pay($dn_pay_link,$h_move);			
	}	
	else if($bill_id && $amount )
	{  
		$msg = "Bill Pay for BillID:"+"$bill_id"+" for Amount:"+ $amount;
		//Materialize.toast($msg,2000);
		bpv_bill_pay($api_pay_link,$h_move);	
	} 
	else
	{
		$( "#bill_result" ).append("Please fill the Bill Amount..");
		$( "#bill_result" ).toggleClass( "red accent-3" );	
	}  
	
}

/*** FX:: Bill/CN/PO PAY BEGIN***/

/*
* FX: bpv_bill_pay: Pay the Bill of Bizorder and get the Fresh Txns.
* Bill Pay: bpv_bill_pay.
*
*/

function bpv_bill_pay($api_pay_link,$h_move)
{

		var $bill_id	 = $h_move['billid'];
		var $amount		 = $h_move['amount'];
		var $pay_type 	 = $h_move['pay_type'];
		var $order_type = $h_move['order_type'];
		var $branchid   = $h_move['branchid'];
		var $bizorderid = $h_move['bizorderid'];		
		var $message	 = $h_move['message'] || '';	

	   $('#bill_result').append($progress_clock);
	   var $pay_method = $api_pay_link + $bill_id+"/"+$amount+"/"+$message;
	   
		$.ajax(
		{
        url: 			$pay_method,
        type: 			'PUT',
        headers: 
        { 
        					"Content-Type": "application/json",
        					"payment_type": $pay_type,
        },
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				
 				var $me = "<span class='red-text'>" + $error_msg + "</span>";
 				$('#bill_result').append($me);
 				
 				
        },
		  success: function( data )
		  {
				$('#bill_result').append("Payment Received.");
				//Bill result
				bx_diner_bill_txns(); 
				$('#panel_diner_bill_change').empty();
				$('#panel_diner_bill_change').
					append(
					 "<p class='green'> &nbsp; Payment is complete</p>")				
				
				//$( "#bill_result" ).css( "backgroundColor", "orange" );
				
		   },                
       }); 
	
}

/*
* FX: bpv_cn_pay: Credit-Note Pay, then Redirecto Bill SPA
*
*
*/
function bpv_cn_pay($cn_pay_link,$h_move)
{

		var $bill_id	 = $h_move['billid'];
		var $amount		 = $h_move['amount'];
		var $pay_type 	 = $h_move['pay_type'];
		var $order_type = $h_move['order_type'];
		var $branchid   = $h_move['branchid'];
		var $cn_orderid = $h_move['bizorderid'];		
	
	
	   $('#bill_result').append($progress_clock);

  		$pay_method = $cn_pay_link + $branchid + "/" + $cn_orderid;
  		var $h_fill = {'return_amount':$amount,};
	   
		$.ajax(
		{
        url: 			$pay_method,
        type: 			'PUT',
        data: $h_fill,
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				
 				var $me = "<span class='red-text'>" + $error_msg + "</span>";
 				$('#bill_result').append($me);
 				
 				
        },
		  success: function( data )
		  {
				$('#bill_result').append("Payment Received.");
				//Bill result
				bx_diner_bill_txns($bill_id,$order_type);
				$('#panel_diner_bill_change').empty();
				$('#panel_diner_bill_change').
					append(
					 "<p class='green'> &nbsp; Payment is complete</p>")				
				
				//$( "#bill_result" ).css( "backgroundColor", "orange" );
				
		   },                
       }); 
	
	
}


/*
* FX: bpv_dn_pay: DebitNote Pay, then Redirecto Bill SPA
*
*/
function bpv_dn_pay($dn_pay_link,$h_move)
{
		var $bill_id	 = $h_move['billid'];
		var $amount		 = $h_move['amount'];
		var $pay_type 	 = $h_move['pay_type'];
		var $order_type = $h_move['order_type'];
		var $branchid   = $h_move['branchid'];
		var $cn_orderid = $h_move['bizorderid'];		
	
	   $('#bill_result').append($progress_clock);

  		$pay_method = $dn_pay_link + "/" + $cn_orderid;
  		var $h_fill = {'amount':$amount,};
	   
		$.ajax(
		{
        url: 			$pay_method,
        type: 			'PUT',
        data: $h_fill,
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				
 				var $me = "<span class='red-text'>" + $error_msg + "</span>";
 				$('#bill_result').append($me);
 				
 				
        },
		  success: function( data )
		  {
				$('#bill_result').append("Payment Received.");
				//Bill result
				bx_diner_bill_txns($bill_id,$order_type);
				$('#panel_diner_bill_change').empty();
				$('#panel_diner_bill_change').
					append(
					 "<p class='green'> &nbsp; Payment is complete</p>")				
		   },                
       }); 
	
	
}


/*
* FX: bpv_po_pay
* PO Pay Link then Redirecto Bill SPA
*
*/
function bpv_po_pay($po_pay_link,$h_move)
{

		var $bill_id	 = $h_move['billid'];
		var $amount		 = $h_move['amount'];
		var $pay_type 	 = $h_move['pay_type'];
		var $order_type = $h_move['order_type'];
		var $branchid   = $h_move['branchid'];
		var $cn_orderid = $h_move['bizorderid'];	
			
		//Materialize.toast("PO Payment"+$bill_id, 10000);	
	
	   $('#bill_result').append($progress_clock);

	   var $pay_method = $po_pay_link + $bill_id+"/"+$amount;
  		//var $h_fill = {'return_amount':$amount,};
	   
		$.ajax(
		{
        url: 			$pay_method,
        type: 			'PUT',
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				
 				var $me = "<span class='red-text'>" + $error_msg + "</span>";
 				$('#bill_result').append($me);
 				
 				
        },
		  success: function( data )
		  {
				$('#bill_result').append("Payment Received.");
				//Bill result
				mb_po_bill_txns($bill_id,$order_type);
				//bx_diner_bill_txns($bill_id,$order_type);
				$('#panel_diner_bill_change').empty();
				$('#panel_diner_bill_change').
					append(
					 "<p class='green'> &nbsp; Payment is complete</p>")				
				
				//$( "#bill_result" ).css( "backgroundColor", "orange" );
				
		   },                
       }); 
	
	
}


/*
* FX: bx_bill_part_pay
* Bill Part Payment PART_PAY
* Ajax: PUT Bill
*
*/
function bx_bill_part_pay()
{
	
	var $bill_id   	= $("#billid" ).text() || $( "#billid" ).val();
   var $in_amount  	= $("#bill_part_pay_amount" ).val() ;	
	var $in_reason  	= $("#bill_part_pay_reason" ).val() ;
	var $pay_type 		= $("#bill_part_pay_type").val() || $("#payment_type").val() || 'CASH';	
	var $appid = $('#in_appid').val();
		//Default API	

	var $div_di   = "#diner_index";
	var $api_pay_link = "/restaurant/bill/pay";	

	if($appid == $c_appid_acquire)
	{
			$api_pay_link = "/purchase/bill/pay";
	}	
  	else if($appid == $c_appid_foodtakeout)
  	{
	  		$api_pay_link = "/pickup/bill/pay";  	
  	}
  	else if($appid == $c_appid_retail)
  	{
	  		$api_pay_link = "/retail/bill/pay";  	
  	}	

	if($bill_id && $in_amount && $in_reason )
	{
		
   	$("#x_extra_more_below").empty();
   	$("#x_extra_more_below").append($progress_clock);
   	
		var $pay_method = $api_pay_link + "/" + $bill_id + "/" + $in_amount + "/" + $in_reason;
		
		$.ajax(
		{
        url: $pay_method,
        type: 'PUT',
        headers: 
        { 
        					"Content-Type": "application/json",
        					"payment_type": $pay_type,
        },
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
  	  		   $('#rest_messages').empty();
  	  		   
 				var $fmsg = "<span class='red-text'>" + $g_sign_fail 
 					+ $error_msg + "</span>";
 				$('#rest_messages').append($fmsg);
        },
		  success: function( data )
		  {
				bx_bill_partpay_success(data,$bill_id,$appid);
		  },                
      }); 
       
    }
	else
	{
		var $fmsg = "<span class='red-text'><b>" + $bill_id +" "+ $in_amount 
 					+ "</b>Please fill in the Amount.</span>";
		$( $rest_msg_out).empty();
		$( $rest_msg_out).append($fmsg);
	}
	   

}

/*
* FX: bx_bill_partpay_success(data,billid,$appid)
*
* run in bx_bill_part_pay after success of part payment
* 
*/
function bx_bill_partpay_success(data,$billid,$appid)
{
		var $in_appid = $appid || 'NONE';

		if($appid == $c_appid_acquire)
		{
			mb_po_abill($billid);
			$(".preloader-wrapper").remove();			
		}		
		else if($appid == $c_appid_retail)
		{
			rl_abill($billid);//This is bad idea,
			//Removing WaitCircle by force.
			$(".preloader-wrapper").remove();
			//Materialize.toast($in_appid);
		}
		else
		{
		  	   $('#rest_messages').empty();				
		  	   var $smsg = "<span class='green-text'><b>"+ $g_sign_pass +"</b>Payment Received.</span>";
				$('#rest_messages').append($smsg);		
				$("#more_btn_below").empty();		
		
				$("#idprint").remove();//Bug #81
			
				//Refresh the Bill Page.
				$("#bill_result").empty();
				$($div_di).empty();
				
				//This is problematic as handles the bill more part only
				//var $ul_bill_top	= bx_bill_top();
				//$($div_di).append($ul_bill_top);
				
				//Add List of Bill Items Div.
												
				var $dv_bxd 		= bx_bill_details();
				$("#di_top").append($dv_bxd);
				
				bx_diner_bill_txns();
				//Removing WaitCircle by force.
				$(".preloader-wrapper").remove();
		}
		
}

/*** FX:: BILL DISPLAY ***/

/*
* FX: bx_bill_header
* Bill Header
* Returns: Dom: div (string)
*
*/
function bx_bill_header()
{
		var $str_div;
	  	$str_div =		 "<div class='collection-item row '>"
	  			 		+ "<div class='col l2 m3 s3 hide-on-small-only  "+ $btx1_color +" '><b>"		  
	  		 				+ "S.No." 		+ "</b></div>"
	  			 		+ "<div class='col l10 m9 s9   "+ $btx1_color +" '><b>"		  
	  		 				+ "Name" 		+ "</b></div>"
	  		 				
	  		 			+"<div class='col  l4 m4 s12   "+ $btx3_color +" left-align '>" 	  		 			 	  		 			 	  		 			 
	  		 				+ "&nbsp; D/C, Type"  		+ "</div>"
	  		 				
	  		 			+"<div class='col  l4 m4 s6   "+ $btx3_color +" left-align '>" 	  		 			 	  		 			 	  		 			 
	  		 				+ "&nbsp; Details "  		+ "</div>"
	  		 				
	  		 			+"<div class='col  l4 m4 s6   "+ $btx4_color +" right right-align '><b>"
	  		 				+ "Amount" + "</b></div>"
	  		 			+ "</div>";
	  		 			
	  return $str_div;		 			
	  		 			
}	  		 			

/*
* Mustache template: Bill summary(Bizorder)
*
*/
var $mst_bill_summary =  	 
										"<div class='row collection z-depth-1'>" 
            		  			+  
            		  				"{{#total_discount}}"
            		  	 			+ "<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Discount Given" 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{total_discount}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  				+ "{{/total_discount}}"
									+	"<div class='row collection-item "+ $btx4_color +"'>"
				            		  		+ "<div class='col s8 left-align '>"
				            		   		+ "Subtotal"
				            		  			+ " </div>"
				            		  		+ "<div class='col s4 right-align'>"
				            		   		+ "{{sub_total}}"
				            		  			+ " </div>"
            		  	        	+ "</div>"
            		  			+ 	 "<div class='row collection-item  "+ $btx4_color +"'>"
            		  						+ "<div class='col s8 left-align'>"
            		  							+ "Taxes" 
            		  							+ "</div>"
            		  						+ "<div class='col s4 right-align'>"
            		  							+ "{{taxes}}" 
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+ 
            		  	 			"<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Charges" 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{charges}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Bill Total ({{{currency}}})" 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{total_debit}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Credit" 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{total_credit}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item  "+ $btx4_color +"'>"
             		  							+ "<div class='col s8 left-align'>"
            		  									+ "Payable ({{{currency}}})"
            		  									+ "</div>"
             		  							+ "<div class='col s4 right-align red-text'> "
            		  									+ "{{total}}"
            		  							+ "</div>"

										+ "</div>"            		  	
            		  			+ "</div>";

/*** MST: Credit Note summary ***/
var $mst_credit_note_summary =  	 
										"<div class='row collection z-depth-1'>"  
									+	"<div class='row collection-item "+ $btx4_color +"'>"
				            		  		+ "<div class='col s8 left-align '>"
				            		   		+ "Subtotal"
				            		  			+ " </div>"
				            		  		+ "<div class='col s4 right-align'>"
				            		   		+ "{{sub_total}}"
				            		  			+ " </div>"
            		  	        	+ "</div>"
            		  			+ 	 "<div class='row collection-item  "+ $btx4_color +"'>"
            		  						+ "<div class='col s8 left-align'>"
            		  							+ "Taxes Returned" 
            		  							+ "</div>"
            		  						+ "<div class='col s4 right-align'>"
            		  							+ "{{taxes}}" 
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Bill Total ({{{currency}}})" 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{total_debit}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Credit" 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{total_credit}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item  "+ $btx4_color +"'>"
             		  							+ "<div class='col s8 left-align'>"
            		  									+ "Return Amount ({{{currency}}})"
            		  									+ "</div>"
             		  							+ "<div class='col s4 right-align red-text'> "
            		  									+ "{{total}}"
            		  							+ "</div>"

										+ "</div>"            		  	
            		  			+ "</div>";


/*
* FX: bx_diner_bill_txns
* Changes DOM
* Display Bill txns
*
*/
function	bx_diner_bill_txns($in_billid,$order_type) 
{

		var $bill_id = $in_billid || $( "#billid" ).text() || $("#billid").val();

		//console.log($bill_id);
		
		var $appid = $('#in_appid').val();

		//Default API	
		var $api_bill_items = "/restaurant/bill/"+$bill_id;	
		if($order_type == $c_order_dn)
		{
			$api_bill_items = "/purchase/bill/"+$bill_id;
		}
  		else if($appid == $c_appid_foodtakeout)
  		{
	  		$api_bill_items = "/pickup/bill/"+$bill_id;  	
  		}
  		else if($appid == $c_appid_retail)
  		{
	  		$api_bill_items = "/retail/bill/"+$bill_id;  	
  		}		

		$.ajax( 
					{ 
						url: 			$api_bill_items ,
						type: 		"GET",
						dataType: 	"json",
						error: function (xhr,status,error)
 						{
 							var $me = "<span class='red-text'>No Info for Bill:"
 								+ $bill_id + "</span>";
            		},
						success: function( data )
						{
							bx_diner_bill_info(data,$order_type);
						},
				}	
		);
	
}

/*
* MST:  mst_bill_amt_msg: Final Bill Amount Message 
*/
var $mst_bill_amt_msg   = 
	         		  	 "<div class='card row red'>"
	         		  + "{{#return_amount}}"	 
	         		  		+ "<div class='col s6 '>Return Amount " 
	         		  			+ " </div>"
	         		  + "{{/return_amount}}"
						  + "{{^return_amount}}"
	         		  		+ "<div class='col s6 '>Total Amount " 
	         		  			+ " </div>"
						  + "{{/return_amount}}"						  	         		  			
	         		  + "<div class='col s3 right-align'>"
	         		  		+ "{{{currency}}}" + "</div>"
	          		  	+ "<div class='col s3 right-align'><b>"
	         		  		+ "{{amount}}" + "</b>"
	         		  		+ "<input id='bill_order_type' value='"+ "{{order_type}}" +"' "
	         		  			+ " type='hidden' >"
	         		  	+"</div>"; 

/*** MST: mst_bill_payform: Final Bill Settlement Form: RETURN/PAYMENT ***/	         		  	
var $mst_bill_payform   = 
	         		"<div class='row white '>"
	         		
	         		  	+ "<div class='col l3 m1 s1 '>&nbsp; </div>"
	          		  	+ "<div class='col l6 m10 s10 '>" 

	         		  	+ "{{#return_amount}}Amount Payout"	 
	          		  		+ "<input size='10' id='inbill_amount' name='amount' type='number' " 
	          		  		+ "value='{{amount}}'" 
	          		  		+ " /> "
	         		  + "{{/return_amount}}"
	         		  
						  + "{{^return_amount}}Amount Receivable"
	          		  		+ "<input size='10' id='inbill_amount' name='amount' type='number' " 
	          		  		+ "value='{{amount}}'" 
	          		  		+ " /> "
	         		  + "{{/return_amount}}"

	         		  + "</div>"
          		  	+ "<div class='col l3 m1 s1 '></div>"
	         		  	
	         		 + "</div>"
	         		 
		         	 +"<div class='row'>"
		         		  		+ "<div class='col l3 m3 s1' >&nbsp;</div>"
		         		  		+ "<div class='col l6 m6 s10'>"
	      		   		  			+ "{{{pay_types}}}" 
			            		  		+" </div>"
		         				+ "<div class='col l3 m3 s1' >&nbsp;</div>"
	         		 +"</div>";	         		  	



/*** MST Transactions of a Bill ***/

/*
*
* MST: TXN EDIT: for Bizorder: Returns and PO: returns
*
*/

var $mst_cn_txn_edit = "<div class='collection-item " + "{{div_edit}}" + " order_return_txn '  >"
			  		 			+ "<div class='col l3 m3 s12 '>"
			  		 				+ "Return Units"	
			  		 				+ "</div>"
			  		 			+ "<div class='col l2 m3 s6 ' >"
				  		 				+ "<input class='in_cn_txnid' value='{{txnid}}' "
				  		 						+ " type='hidden' "
				  		 						+ "/> "  
  		 								+ "<input class='in_cn_units' value='0' "
  		 										+ " type='number' min=0 max={{units}} "
  		 										+ "/> "				  		 				
			  		 				+ "</div>"
			  		 			+ "<div class='col l3 m4 s12 "+ $btx2_color +" left-align '>"
	  				 				+ "of {{ item }}"  + "</div>"
			  		 				
				+ "</div>";


/*
* MST: template for a Txn in a bill
*
* Used in bx_bill_txns
*
*/
var $mst_abill_txn =

	 "<div class='collection-item panel row black-text {{div_class}}' > "

	  			 		+ "<div class='col l2 m2   s2  '><b>"		  
	  		 				+ "{{ count }}" 		+ "</b></div>"
	  		 			+ "<div class='col l10 m10 s10 truncate' >"
	  		 				+ "&nbsp; {{ item }}"  + "</div>"

	 + "</div>"
	 +"<div class='collection-item panel row black-text {{div_class}} '>"

	  		 			+ "<div class='col l3 m4 s8  '>" 	  		 			 	  		 			 	  		 			 
	  		 				+ "&nbsp; {{ dc }},{{xtype}}"  		+ "</div>"
	  		 				
	  		 			+ "<div class='col l2 m4 s6   '>"
	  		 				+ "&nbsp; {{{ more_type }}}" + "</div>"
	  		 				
	  		 			+ "<div class='col l2 m4 s6   right right-align'><b>"
		  		 			+ "{{{ currency}}} {{ amount }}" + "</b></div>"
 				
	  		 				
	+ 		"</div>"
	
			+ "{{ #is_return_allowed }}"
				+ $mst_cn_txn_edit
			+ "{{ /is_return_allowed }}"
	;


/*
*
* Runs in bx_diner_bill_txns after success.
* Makes changes in the dom.
* FX: bx_diner_bill_info: Displays the Bill(for payment,cancel,email,print,partpay)
* @params: billitems,order_type
*
*/
function bx_diner_bill_info(billitems,$order_type)
{

		//Get BizORderID and Bill.	
		var $orderid = billitems.orderid;
		var $billid  = billitems.billid;
		var $str_order_type;
		if(!$order_type)
		{
			$order_type = billitems.order_type;			
		}
		
		if(($order_type) && ($order_type != $c_order_bo))
		{
			$str_order_type = $order_type;
		}
		
		$("#bizorderid").val($orderid);
		$("#billid").val($billid);					
	
		//--Txns of Bills	
		var $dv_bill = '#list_of_bill_items';
		
		var items = [];

		//-- Options to Cancel: if not Settled and not-Cancelled			
		var $div_cnxl_pay;	  		 					
  		if(!billitems.cancelled && !billitems.settled_txnid)
  		{
  		 			$div_cnxl_pay = bx_bill_cancel_pay();
  		 			$("#diner_index").append($div_cnxl_pay);//a1
  		 			//Payment and Cancel Options
  		}

		//-- Branch Address, and Bill info header.
		var $bx_info = bx_bill_branch_header(billitems,$order_type);		
  		items.push($bx_info);			   //a2
  				
  		var $currency ;
  		var $t_count = 0;

		var $t_head_str = bx_bill_header();

		//-- Final Hash 			
		var $ft = billitems.finalh;	
		$order_type = billitems.order_type;//Fill It Again.	
		
		//IF Txns 			
		if(billitems.transactions)
		{
			//items.push($t_head_str);//a3

			//-- Txns: List		
			var $h_txns = bx_bill_txns(billitems.transactions,$order_type);		
			var $ul_all_txns = "<ul class='collection'>"+  $t_head_str + $h_txns.txns +"</ul>";
			items.push($ul_all_txns) ;//a4

			//PayTypes
			var $ptypes  = billitems.payment_types;
			var $str_px =	bx_paytypes($ptypes);
									
			$currency 				= $h_txns.currency;
			var $amt_payable		= billitems.amount_payable;
			var $h_payform = {
										'currency'	: $h_txns.currency,
										'amount'		: $amt_payable,
										'order_type' : $str_order_type,
										'pay_types'  : $str_px,
								  };

			
			//Payable Form.
			if($amt_payable > 0)
			{	
					var $div_msg;var $div_bill_payform;
					$div_msg		= Mustache.to_html($mst_bill_amt_msg,$h_payform);
	 	  		 	//items.push($div_msg);
	      		  		
					$div_payform = Mustache.to_html($mst_bill_payform, $h_payform);

					$( "#bill_result" ).append($div_payform);
 		 			$( "#bill_result" ).append($div_msg);

 	  		 			             		 	  		 			 
 	  		}//Amount Payable
 	  		else if($amt_payable < 0)
 	  		{
 	  			   $h_payform['return_amount'] = $amt_payable;

					var $div_msg;var $div_bill_payform;
					$div_msg		= Mustache.to_html($mst_bill_amt_msg,$h_payform);
	 	  		 	//items.push($div_msg);
	
					$div_payform = Mustache.to_html($mst_bill_payform, $h_payform);

					$( "#bill_result" ).append($div_payform);
 		 			$( "#bill_result" ).append($div_msg);
 	  			
 	  		}

			$("#bill_order_type").val($order_type);		
	   	var $h_summary;
	   	var $div_summary;
	   	if($order_type == $c_order_cn)
	   	{
				$div_summary = b_credit_note_summary(billitems,$currency);	   		
	   	}
	   	else if($order_type == $c_order_dn)
	   	{
				$div_summary = b_bill_summary(billitems,$currency);	   		
	   	}
	   	else
	   	{
	   		$div_summary = b_bill_summary(billitems,$currency);
	   	}	
	   	
	   	if($div_summary)
	   	{
	   		items.push($div_summary);//a5
	   	}	
 	  		 	
			//Empty entity_bill_more
	   	$("#more_btn_below").empty();       		
 	  		 	
  		}
  		//IF billitems.transactions 	  		 	
  			
		bx_dropdown_init();

		//Now add to the Div.  		 	
		$($dv_bill).empty().
			append($( "<div/>", 
 			{
    				"class": "",
    				"html": items.join( "" )
 				} 
 			)
 		);
    				
	
}


/*
* FX: bx_bill_txns : Used to display bill_txns
* @params: Bill.txns[,order_type]
* @returns: Hash {txns(string),currency(string)}
* Not Using MoustacheJS here
*
*/
function bx_bill_txns(txns,$order_type)
{
		var $div_txns_str;
		var $currency;
		
		var $t_count=0;		
	  	$.each( txns, function( key, val ) 
	  			{
	  					var $atxn;
	  					$t_count++;
	  					
	  					var $t_icount		= val.serial || '>';
	  		 			var $t_dc  			= val.dc;
	  		 			var $t_price  		= val.price;
	  		 			var $t_type	   	= val.type;

						var $t_val2			= val.tval2;
						
	  		 			var $t_units	  	= '&nbsp;';
	  		 			var $t_calc;
	  		 			if(val.units)
	  		 			{
	  		 				$t_calc = " @" + $t_price + " x "+ val.units;
	  		 			}	  		 			
	  		 			
	  		 			var $t_more_type	= val.more_type || $t_calc || $t_val2 || "&nbsp;";
	  		 			var $t_amount  	= val.amount;
	  		 			var $t_amt_tax		= val.amt_tax  || '&nbsp;';
	  		 			var $t_amt_prod_serv_chgs = val.amt_prod_serv_chgs || '&nbsp;'; 	
	  		 			var $t_date 		= val.txndate;
	  		 			var $t_item_product_code = val.item_product_code ;
	  		 			var $t_item_service_code = val.item_service_code ;
	  		 			$currency	=  val.currency_symbol || val.currency || 'n/a';
	  		 			var $a_comments   = val.comments;
	  		 			var $t_comments	= $a_comments || " &nbsp; ";	  

	  		 			if(($t_item_product_code || $t_item_service_code) && ($a_comments) )
	  		 			{
	  		 				var $t_x = $t_item_product_code || $t_item_service_code;
	  		 				$t_item = "(" + $t_x + ")" + " " + $t_comments;
	  		 			}
	  		 			else if($a_comments)
	  		 			{
	  		 				$t_item = $t_comments;	
	  		 			}
						else
						{
	  		 				var $t_x = $t_item_product_code || $t_item_service_code;
	  		 				$t_item = $t_x; 
						}
						
						var $class_type = 'row ';
						var $class_ps_edit = 'row white red-text';
						var $str_payment_type = '';

						if(val.payment_type)
						{
								$str_payment_type = ", " + val.payment_type;
						}						

						if($t_type == 'PAYMENT' || $t_type == 'RETURN')
						{
								if($t_dc 		== 'C')
								{
									$class_type = 'row green-text';
									$t_amt_prod_serv_chgs = $t_amount;
									$t_item = "to:" + val.userid + $str_payment_type;	
								}	  		
							 	else if($t_dc 	== 'D')
								{
									$class_type = 'row red-text';
									$t_amt_prod_serv_chgs = $t_amount;
									$str_payment_type = val.payment_type || '';
									$t_item = "by:" + val.userid + $str_payment_type;	
								}	
						}							
					 	else if($t_type == 'TAX')
						{
							$class_type = 'row ';
						}	
						

						//Only If Order.
						var $is_return_allowed;
						if($order_type == $c_order_po && $t_type == 'PROD')
						{
							$is_return_allowed = 1;
							//bx_display_error(1,"Return Ready in PO.");
						}
					 	else if($t_type == 'PROD' && $t_dc == 'D')
						{
							$is_return_allowed = 1;
						}	
						
						if($t_icount > 0 )
						{
							$t_icount = "["+$t_icount+"]";
							
						}	

					 $class_type = 'white'
					 if( ($t_count % 2) == 0)
					 {
					 		$class_type = 'amber lighten-5';
					 }			    


						//card_class_type,count,type,more_type,item,dc,currency,amount
						var $h_txn;
						$h_txn = 
						{
							div_class 	: $class_type,
							div_edit		: $class_ps_edit,
							count 		: $t_icount,
							xtype			: $t_type,
							more_type	: $t_more_type,
							item			: $t_item,
							dc				: $t_dc,
							currency		: $currency,
							amount		: $t_amount,

							units			: val.units,							
							txnid			: val.transactionid,
							is_return_allowed : $is_return_allowed,
						};
	  		 			$atxn = Mustache.to_html($mst_abill_txn, $h_txn);
	  		 			
  						if($div_txns_str)
  						{
  								$div_txns_str += $atxn;
  						}
  						else
  						{
  								$div_txns_str  = $atxn;
  						}
  						
  				});
				//Each
 		
		return { txns: $div_txns_str,currency: $currency};	

}
	

/*
* FX: b_Bill_summary: Final Hash
*
* @params: billitems,currency
*/
function b_bill_summary(billitems,$currency)
{
	var $ft = billitems.finalh;		 
	var $amt_payable		= billitems.amount_payable;	
	var $order_type		= billitems.order_type;
	var $m_summary;
	var $h_summary;
	
	if($ft.total_tax || $ft.total_chgs || $ft.total_credit)
	{	

			      var $f_total_tax 					= $ft.total_tax;		
					var $f_total_prod_serv_chgs 	= $ft.total_prod_serv_chgs || 'psc';				
			      var $f_total_chgs 				= $ft.total_chgs;
			      var $f_total_credit				= $ft.total_credit;
			      var $f_total_debit			   = $ft.total_debit;
			      var $f_total_discount		   = $ft.total_discount ;
			      
			      var $f_total_amt 					= $amt_payable;
			      
			      $h_summary = { 	
			      							'taxes'			:	$f_total_tax,
			      							'sub_total'		:	$f_total_prod_serv_chgs,
			      							'charges'		:	$f_total_chgs,
			      							'total_credit' : 	$f_total_credit,				      							
			      							'total_debit'  : 	$f_total_debit,	
			      							'total_discount' : $f_total_discount,

			      							'total'			:	$f_total_amt,
			      							'currency'		:	$currency,
			      					  };
					var $m_summary = Mustache.to_html($mst_bill_summary, $h_summary);
 	  		}
	
	return $m_summary;
	
}
	

/*
* FX: b_cnote_summary: Final Hash
* @params: billitems,currency
*
*/
function b_credit_note_summary(billitems,$currency)
{
	var $ft					= billitems.finalh;		 
	var $amt_payable		= billitems.amount_payable;	
	var $m_summary;
	var $h_summary;
	
	if($ft.total_tax || $ft.total_chgs || $ft.total_credit)
	{	

			      var $f_total_tax 					= $ft.total_tax_return;		
					var $f_total_prod_serv_chgs 	= $ft.total_prod_serv_chgs_return || 'psc';				
			      var $f_total_chgs 				= $ft.total_chgs_return;
			      var $f_total_credit				= $ft.total_credit;
			      var $f_total_debit			   = $ft.total_debit;
			      var $f_total_discount		   = $ft.total_discount_return ;
			      
			      var $f_total_amt 					= $amt_payable;
			      
			      $h_summary = { 	
			      							'taxes'			:	$f_total_tax,
			      							'sub_total'		:	$f_total_prod_serv_chgs,
			      							'charges'		:	$f_total_chgs,
			      							'total_credit' : 	$f_total_credit,				      							
			      							'total_debit'  : 	$f_total_debit,	
			      							'total_discount' : $f_total_discount,

			      							'total'			:	$f_total_amt,
			      							'currency'		:	$currency,
			      					  };
					var $m_summary = Mustache.to_html($mst_credit_note_summary, $h_summary);
 	  		}
	
	return $m_summary;
	
}


/*
* FX: bx_paytypes: Bill Payment Types
*
* @params: array types
*
*/
function bx_paytypes($ptypes)
{
	  var $str_btn;
	  //return $str_btn;

		if($ptypes)
		{	
			$str_btn = "<Select class='browser-default' name='payment_type' id='payment_type'>";
	  		$.each( $ptypes, function( index, xval ) 
	  		{
	  			var $x = "<option value='"+ xval.paytype +"' " + xval.selected 
	  					+ " >" 
	  					+ xval.name 
	  				+ "</option>";
	  			$str_btn += $x;
	  			//$str_btn = "a";				
	  		});
	  		$str_btn += "</select>";
	  	}	
	
	return $str_btn;
}



/*** FX:: End Bill Display ***/

/*
* FX: bx_bill_cancel_pay: Payment Button  and input for amount is here.
* @returns: Div to accept Payment
*
* uses: $mst_bill_payform
*
*/

function bx_bill_cancel_pay()
{	
 

 	var $abc = " <div id='panel_diner_bill_change' > "
			+ "<div class='card-panel '>"
			
			+ "<div class='row' id='btn_make_payment'> "
			+ "				<div class='col s12 m12 l12 center'> "
									+ "<label class='btn green waves-effect center' id='make_payment' href=''> "
									+ "Make Payment</label> "
									+ "</div>"
			+"</div>	"
			
			+ "<div class='row '>"
												+ "<div class='col s1'></div>"
												+ "<div class='input-field col s10 l10 m10 left ' >" 
													+ "<input type='text'  id='full_pay_comment' /> "
													+ "<label for='first_name'>Comment</label>"	 		
												+ "</div>"
												+ "<div class='col s1'></div>"
			+"</div>"
			
			+ "<div class='row '>"
												+"<div class='input-field col s12 l12 m12 left "
												+ " ' id='bill_result'> " 		 		
												+ "</div>"
			+"</div>"
			+ "	</div> "
			+ "</div>"; 
	
	return $abc ;	
}

/*
* FX: bx_entity_more_empty:
* Dom changes: x_more div is emptied

*/

function bx_entity_more_empty($in_div,$in_div_below)
{
	var $div_moretop = $in_div || $div_entity_more;
	$($div_moretop).empty();
	var $div_below = $in_div_below || $div_entity_more_below;
	$($div_below).empty();
}

/*
* FX: bx_bill_top: More buttons
* Returns dom: UL as string
* Bill top
*
*/
function bx_bill_top()
{

	//Print and More btns
	var $btn_more  = bx_bill_btn_more();
		
	return $btn_more;							
	
}



/*** FX:: Bill: EMAIL/CANCEL ***/

/*
* FX: bx_diner_bill_cancel
* Writes: DOM
* Bill: Cancel
* Ajax: PUT Bill
*
*/
function bx_diner_bill_cancel()
{
	var $bill_id   = $( "#billid" ).text() || $( "#billid" ).val();
   var $message  = $( "#inbill_cancel_message" ).val();
	var $order_type 	= $("#bill_order_type").val();   
	var $api_bill_cancel = "/restaurant/bill/cancel";
	
	var $appid = $('#in_appid').val();

	if($appid == $c_appid_acquire)
	{
		$api_bill_cancel = "/purchase/bill/cancel";
	}	
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$api_bill_cancel = "/pickup/bill/cancel";  	
  	}	
  	else if($appid == $c_appid_retail)
  	{
  		$api_bill_cancel = "/retail/bill/cancel";  	
  	}	
	   
	if($bill_id && $message )
	{  
	   $('#bill_result').append($progress_clock);
	   
	   var $cancel_method = $api_bill_cancel + "/" + $bill_id + "/" +$message;
		$( "#cx" ).css( "backgroundColor", "red" );	
	   //$( "#bill_result" ).append($input);
		$.ajax(
		{
        url: $cancel_method,
        type: 'PUT',
        headers: { "Content-Type": "application/json", },
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);

 				var $fmsg = "<span class='red-text'>" + $g_sign_fail 
 								+ $error_msg + "</span>";
  	  		   $('#rest_messages').empty();
 				$('#rest_messages').append($fmsg);
 				
        },
		  success: function( data )
		  {
		  		$("#more_btn_below").empty();
		  		bx_entity_more_empty();
		  		
		  		var $msg = "<p class='red'>Bill has been cancelled.</p>";
		  		
		  		$( "#bill_result" ).css( "backgroundColor", "red" );
		  		
				$('#panel_diner_bill_change').empty();
				$('#panel_diner_bill_change').append($msg);
					 				
		   },                
       }); 
		
	} 
	else
	{
		var $msg = "<span class='red-text'><b>"+  $g_sign_fail  
				+"</b>Please fill in the reason of the Cancellation.</span>";
		$( $rest_msg_out).empty();
		$( $rest_msg_out).append($msg);
	}  
	
}

/*
* FX: bx_diner_bill_email: Email a bill
*
* AJAX: PUT Bill
*
*/
function bx_diner_bill_email()
{
	var $bill_id   = $( "#billid" ).text() || $( "#billid" ).val();
   var $in_email  = $( "#bill_email" ).val() ;	

	var $appid = $('#in_appid').val();
		//Default API	
	var $api_bill_email = "/restaurant/bill/email";	

	if($appid == $c_appid_acquire)
	{
	  		$api_bill_email = "/purchase/bill/email";  	
	}	
  	else if($appid == $c_appid_foodtakeout)
  	{
	  		$api_bill_email = "/pickup/bill/email";  	
  	}
  	else if($appid == $c_appid_retail)
  	{
	  		$api_bill_email = "/retail/bill/email";  	
  	}	
	
	   
	if($bill_id && $in_email )
	{  
	   $('#rest_messages').append($progress_clock);
	   
	   var $email_method = $api_bill_email + "/" + $bill_id + "/" +$in_email;
	   
		$.ajax(
		{
        url: 		$email_method,
        type: 		'PUT',
        headers: 	{ "Content-Type": "application/json", },
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				
  	  		   $('#rest_messages').empty();
 				var $fmsg = "<span class='red-text'>" + $g_sign_fail 
 					+ "Oops, Something went wrong!</span>";
 				$('#rest_messages').append($fmsg);
        },
		  success: 	function( data )
		  {
		  	   $('#rest_messages').empty();				
		  	   var $smsg = "<span class='green-text'><b>"+ $g_sign_pass +"</b>Mail Sent.</span>";
				$('#rest_messages').append($smsg);		
				$("#more_btn_below").empty();		
		   },                
      }); 
		
	} 
	else
	{
		var $fmsg = "<span class='red-text'><b>" + $bill_id +" "+ $in_email + $g_sign_fail 
 					+ "</b>Please fill in the Email.</span>";
		$( $rest_msg_out).empty();
		$( $rest_msg_out).append($fmsg);
	}
	
}

/*
* FX: bx_fto_order_delivery_accept
*
*/

function bx_fto_order_delivery_accept()
{
	var $bill_id   = $( "#billid" ).text() || $( "#billid" ).val();
   var $in_reason  = $( "#reason" ).val() ;	
   var $in_status  = $( "#accept_order" ).val() ;
   
   var $h_in = {};
   $h_in.reason 		 = $in_reason;
   $h_in.accept_order = $in_status;

	Materialize.toast("ACCEPT Order",3000);
   		   
   var $is_ok;
   if($in_status == 0 && !$in_reason)
   {
   		Materialize.toast("Reason is required for Rejection",3000);
   		$is_ok=0;
   }
   else if($in_status == 0 && $in_reason )
   {
   		$is_ok=1;
   }
   else if($in_status > 0)
   {
   		$is_ok=1;
   }
    
	var $appid = $('#in_appid').val();
	var $api_bill_put = "/pickup/bill";	
	   
	if($bill_id && $is_ok )
	{  
	   $('#rest_messages').append($progress_clock);
	   
	   var $url = $api_bill_put + "/" + $bill_id ;
	   
		$.ajax(
		{
        url: 				$url,
        type: 				'PUT',
        dataType:       'json',
        data:				JSON.stringify($h_in),
        headers: 			{ "Content-Type": "application/json", },
		  error: function (xhr, status, error) 
 		  {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				
  	  		   $('#rest_messages').empty();
 				var $fmsg = "<span class='red-text'>" + $g_sign_fail 
 					+ "Oops, Something went wrong!</span>";
 				$('#rest_messages').append($fmsg);
        },
		  success: 	function( data )
		  {
		  	   
		  	   $('#rest_messages').empty();				
		  	   var $smsg = "<span class='green-text'><b>"+ $g_sign_pass +"</b>Updated Order.</span>";
				$('#rest_messages').append($smsg);		
				$("#more_btn_below").empty();		
				
				move_pager('bill');//diner/index 
		   },                
      }); 
		
	} 
	else
	{
		var $fmsg = "<span class='red-text'><b>" + $bill_id  
 					+ "</b>Please fill in the the details for the bill</span>";
		$( $rest_msg_out).empty();
		$( $rest_msg_out).append($fmsg);
	}
	
}


/*** FX:: Bill More ***/


var $mst_link_part_pay = 	"<span  class='btn-flat'  id='entity_bill_more_partpay' "
										+"onClick='bx_bill_open_part_pay();' >"
										+ "Partpay "
								+ "</span><li class='divider'></li>";									

var $mst_link_cnote = 	"<span  class='btn-flat' id='entity_bill_more_cnote'  "
										+ "onClick='bx_bill_open_cnote();' >"
										+ "Return"
								+ "</span><li class='divider'></li>";									

var $mst_link_po_return = 	"<span  class='btn-flat' id='entity_bill_more_cnote'  "
										+ "onClick='bx_bill_open_po_return();' >"
										+ "Return"
									+ "</span><li class='divider'></li>";									

var $mst_link_email = 	"<span  class='btn-flat' id='entity_bill_email'  " 
										+ "onClick='bx_bill_open_email();' >"
										+ "Email Add"
								+ "</span><li class='divider'></li>";	

var $mst_fto_delivery_confirm = 	"<span  class='btn-flat left-align' id='entity_bill_more_delivery_accept'  "
										+ "onClick='bx_bill_fto_accept_order();' >"
										+ "Accept"
								+ "</span><li class='divider'></li>";									


/*
* FX: bx_bill_btn_more: Adds More Button
* @returns: String Div for more btn in bill
* It also add print button(new page) link.
*
*/
function bx_bill_btn_more($in_div)
{

	var $btn_bill_more;
   var $btn_id = $in_div || 'dropdown_order_more';

	var $is_su = bgx_access_allowed('');
	
	var $link_cancel   	= "";
	var $appid 				= bgx_get_appid();

	var $billid = $("#billid").val();
	var $txnid_settled  = bgx_get_bill_txnid();
	var $is_creditnote_allowed = 0;
	
	var $print_url;

	if($appid == $c_appid_restaurant)
	{
		$print_url = "diner/bill_pure/"+$billid;
	}
	else if($appid == $c_appid_retail)
	{
		$print_url = "shop/bill_pure/"+$billid;
		if($txnid_settled)
		{
				$is_creditnote_allowed = 1;
				//CNote and PART Payment				
				//Handled in bx_bill_branchheader.
		}
	}
	else if($appid == $c_appid_foodtakeout)
	{
		$print_url = "foodtakeout/bill_pure/"+$billid;
	}
	else if($appid == $c_appid_acquire)
	{
			$print_url = "acquire/bill_pure/"+$billid;
	}

	if($is_su)
	{
		$link_cancel = 	"<span class='btn-flat white red-text waves-effect waves-light ' "
									+"onClick='bx_bill_open_cancel();' id='entity_bill_more_cancel' >"
								+"Cancel Bill</span><li class='divider'></li>";	
	}	  
	
	


	var $print_btn = "onclick='g_print_div()'";
	
	var $btn_print =
         "<a class='btn-flat white blue-text waves-effect waves-light' "
                                + " type='submit' "
                                +  "href=' " + $print_url + "' target='_blank' " 
                                + " name='action'> Print  " 
                        + "</a>";


	   
	$btn_bill_more = 	"<div id='more_btn' class='x_dropdown'>"

								+"<div class='btn " 
										+ " border grey lighten-3 black-text' href='#' "
												+ "> " 
												+ "<span class='icon_menu_more'>"
												+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; More "
												+ "</span>"									
								+ "</div>"
							
								+ "<div id='" + $btn_id + "' class='x_dropdown-content  '>"
    									+ $mst_link_email
    									+ $link_cancel
										+ $btn_print    									    	    						    	    						
  									+ "</div>"
  						+ "</div>";

							
	
	return $btn_bill_more;
}

/*
* FX: bx_more_remove: More_below: is emptied out
* @params: in_div
*
*/
function bx_more_remove($in_div)
{
	var $parent_div = $in_div || $div_entity_more_below;
	$($parent_div).empty();

	//return div shuld hide
	var $div_class_returns = '.order_return_txn';		
	bx_div_class_hide($div_class_returns);
	
	return $parent_div;
	
}

/*
* FX: bx_more_link_add
* Add CN/Return More Button in More Menu
* For PartPay,CreditNote
* 
*/
function bx_more_link_add($add_link,$in_div)
{
	var $div_more_id = $in_div ||	'#dropdown_order_more';
	$($div_more_id).append($add_link);
}

/*
* FX: bx_more_link_del
* Add CN/Return More Button in More Menu
* For PartPay,CreditNote
* 
*/
function bx_more_link_del($in_div)
{
	$($in_div).remove();
}

/*
* Returns Close BTn string for More Bill
* FX: bx_more_btn_close
*
*/
function bx_more_btn_close()
{
	var $str_btn = "<div class='col l1 m1 s2 right'>"
									+"<span class='btn center red white-text' "
									+" onClick='bx_more_remove();' >X</span>"
							+ "</div>";
	return $str_btn;						
}

/*** FX:: Bill MORE open Actions ***/

/*
* FX: bx_bill_open_cnote
* Add Cancel Div to parent_div for bill
* @params: div_parent
*
*/
function bx_bill_open_cnote($in_div)
{
	var $parent_div = bx_more_remove();
	var $product_txn_count = $('.order_return_txn').size();
	
	var $str_close = bx_more_btn_close();
	
	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l6 m6 s12'>"
									+ "<input placeholder='reason for Credit Note' id='inbill_cnote_message'  "
										+"type='text' name='message' required />"
								+"</div>"
						+ "<div class='col l5 m5 s9' >"
								+"<a class='btn border white red-text' "
									+ " onClick='bx_bill_create_credit_note();' >"
									+"Create Credit Note</a>"
								+ "<input type='hidden' id='is_more_return_clicked' value='0' > "	
							+"</div>"
						+ $str_close			
						+ "<div class='col l12 m12 s12 right green-text'>"
							+ "<p> Please select "+ $product_txn_count +" items in the bill for return. </p>";
						+ "</div>"
							
						+ "</div>";

	//Txns Edit						
	bx_bill_begin_credit_note('.order_return_txn');						

	$($parent_div).append($str_div);
	
}


/*
* FX: bx_bill_open_po_return
* Add Cancel Div to parent_div for bill
* @params: div_parent
*
*/
function bx_bill_open_po_return($in_div)
{
	var $parent_div = bx_more_remove();
	var $product_txn_count = $('.order_return_txn').size();
	
	var $str_close = bx_more_btn_close();
	
	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l6 m6 s12'>"
									+ "<input placeholder='reason for Debit Note' id='inbill_dnote_message'  "
										+"type='text' name='message' required />"
								+"</div>"
						+ "<div class='col l5 m5 s9' >"
							+"<a class='btn border white red-text'  "
									+ "onClick='bx_bill_create_debit_note();' >"
									+ "Create Debit Note</a>"
									+ "<input type='hidden' id='is_more_return_clicked' value='0' > "
							+"</div>"		
						+ $str_close			
						+ "<div class='col l12 m12 s12 right green-text'>"
							+ "<p> Please select "+ $product_txn_count +" items in the bill for return. </p>";
						+ "</div>"
							
						+ "</div>";

	//Txns Edit						
	bx_bill_begin_credit_note('.order_return_txn');						

	$($parent_div).append($str_div);
	
}


/*
* FX: bx_bill_open_cancel
* Add Cancel Div to parent_div for bill
* @params: div_parent
*
*/
function bx_bill_open_cancel($in_div)
{
	var $parent_div = bx_more_remove();
	var $str_close = bx_more_btn_close();
	
	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l6 m6 s12'>"
									+ "<input placeholder='reason for cancellation' id='inbill_cancel_message'  "
										+"type='text' name='message' required />"
								+"</div>"
						+ "<div class='col l5 m5 s9'>"
							+"<a class='btn border white red-text'  onClick='bx_diner_bill_cancel();' >"
							+"Cancel Bill</a></div>"
						+ $str_close									
						+ "</div>";

	$($parent_div).append($str_div);
	
}


/*
* FX: bx_bill_open_email
* Add Email Send Div to parent_div for Bill
* @params: div_parent
*
*/
function bx_bill_open_email($in_div)
{
	var $parent_div = bx_more_remove();
	var $str_close = bx_more_btn_close();


	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l6 m6 s12'>"
									+ "<input id='bill_email'  type='email' name='bill_email' required />"
								+"</div>"
						+ "<div class='col l5 m5 s9 disabled'>"
									+"<a class='btn border white green-text' onClick='bx_diner_bill_email();' >"
									+ "Send Email</a></div>"
						+ $str_close									
						+ "</div>";

	$($parent_div).append($str_div);
	
}


/*
* FX: bx_bill_open_email
* Add Email Send Div to parent_div for Bill
* @params: div_parent
*
*/
function bx_bill_fto_accept_order($in_div)
{
	var $parent_div 	= bx_more_remove();
	var $str_close 	= bx_more_btn_close();

	var $str_div = "<div class='row card-panel border'>"
						
						+ "<div class='col l6 m6 s12'>"
									+ "<label for='accept_order'>Delivery Order</label>"									
									+ "<select class='browser-default' name='accept_order' id='accept_order' >"
											+ "<option value=''>Accept or Reject Delivery Order</option>"	
      									+ "<option value='1'>Accept</option>"
	      								+ "<option value='0'>Reject </option>"								
									+ "</select>"
						+"</div>"
								
						+ "<div class='col l12 m12 s12 input-field'>"
									+ "<input id='reason'  type='text' name='reason' required />"
									+ "<label for='reason'>Reason for Rejection</label>"
								+"</div>"
								
						+ "<div class='col l5 m5 s9 disabled'>"
									+ "<a class='btn border white green-text' "
									+ " onClick='bx_fto_order_delivery_accept();' >"
									+ "Update Delivery Order</a></div>"
						+ $str_close									
						+ "</div>";

	$($parent_div).append($str_div);
	
}


/*
* FX: bx_bill_open_part_pay
* Add Part Payment Div to parent_div for Bill
* @params: div_parent
*
*/
function bx_bill_open_part_pay($in_div)
{
	var $parent_div = bx_more_remove();
	var $str_close = bx_more_btn_close();

	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l4 m4 s12'>"
									+ "<input id='bill_part_pay_amount'  type='number' "
										+ "name='bill_part_pay' placeholder='Amount' required />"
								+"</div>"
						+ "<div class='col l8 m8 s12'>"
									+ "<input id='bill_part_pay_reason' type='text' "  
											+ "name='bill_part_pay_reason' placeholder='Reason' required />"
								+"</div>"
						+ "<div class='col l5 m5 s9 disabled'>"
									+"<a class='btn border white green-text' onClick='bx_bill_part_pay();' >"
								+ "Part Payment</a></div>"
						+ $str_close									
						+ "</div>";

	$($parent_div).append($str_div);
	
}
/*** Bill More END ***/

/*** FX:: BIZORDER_MORE_BUTTON ***/


/*
* FX: bx_bizorder_btn_more
* Add More Div to parent_div for Order
* @params: div_parent
*
*/
function bx_bizorder_btn_more($in_div)
{

	var $btn;
   var $btn_id = $in_div || 'dropdown_order_more';

	var $is_su = bgx_access_allowed('');
	
	var $link_email    ="";
	var $link_discount ="";
	var $link_cancel ="";
	
	if($is_su)
	{
		$link_discount = 	"<span class='btn-flat' "
										+" onClick='bx_bizorder_open_discount();' >"
								+"Discount</span><li class='divider'></li>";
	}	  
	$link_email 		= 	"<span  class='btn-flat'  "
										+"onClick='bx_bizorder_open_email();' >"
								+"Email</span><li class='divider'></li>";	
									
	$link_cancel 		= 	"<span class='btn-flat flat white red-text waves-effect waves-light ' "
									+"onClick='bx_bizorder_open_cancel();' >"
								+"Cancel </span><li class='divider'></li>";	


	$btn = 	"<div id='more_btn' class='x_dropdown '>"
	
								+"<div class='btn " 
										+ " border grey lighten-3 black-text' href='#' "
											+ "> " 
											+ "<span class='icon_menu_more'></span>"									
								+ "</div>"
							
								+ "<div id='" + $btn_id + "' class='x_dropdown-content  '>"
    										+ $link_email
    										+ $link_cancel    	    						
    										+ $link_discount
  									+ "</div>"
  						+ "</div>";
	
	return $btn;
}

/*
* FX: bx_dropdown_init
*
* Init Dropdown Btn
* @params: div_parent
*
*/
function bx_dropdown_init()
{

  $(document).ready(function()
  {

			 $('.dropdown-button').dropdown({
			      inDuration: 300,
			      outDuration: 225,
			      constrain_width: false, // Does not change width of dropdown to that of the activator
			      hover: true, // Activate on hover
			      gutter: 0, // Spacing from edge
			      belowOrigin: false, // Displays dropdown below the button
			      alignment: 'left' // Displays dropdown with edge aligned to the left of button
			    }
			  );
			       
    
  });
	
}

/*
* Add add_discount div to parent_div for Order
* @params: div_parent
* FX: bx_bizorder_open_discount
*
*/
function bx_bizorder_open_discount($in_div)
{
	var $parent_div = bx_more_remove();
	
	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l6 m6 s12'><input id='discount'  type='number' name='discount' /></div>"
						+ "<div class='col l5 m5 s9'><a class='btn border white green-text' onClick='b_bizorder_update();' >"
							+"Add Discount %</a></div>"
						+ "<div class='col l1 m1 s2 right'>"
							+"<span class='btn-floating center red white-text'  onClick='bx_more_remove();' >X</span></div>"
						+ "</div>";

	$($parent_div).append($str_div);
	
}

/*
* FX: bx_bizorder_open_cancel
* Add cancel_div to parent_div for Order
* @params: div_parent
*
*/
function bx_bizorder_open_cancel($in_div)
{
	var $parent_div = bx_more_remove();
	
	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l6 m6 s12'><input id='cancel_msg' placeholder='reason'  type='text' name='message' /></div>"
						+ "<div class='col l5 m5 s9 disabled'>"
							+"<a class='btn border white red-text' onClick='b_bizorder_cancel();' >Cancel Order</a></div>"
						+ "<div class='col l1 m1 s2 right'>"
							+"<span class='btn-floating center red white-text'  onClick='bx_more_remove();' >X</span></div>"
						+ "</div>";

	$($parent_div).append($str_div);
	
}

/*
* FX: bx_bizorder_open_email
* Add email div to parent_div for Order
* @params: div_parent
*
*/
function bx_bizorder_open_email($in_div)
{
	var $parent_div = bx_more_remove();

	var $str_div = "<div class='row card-panel border'>"
						+ "<div class='col l6 m6 s12'><input id='email'  type='text' name='email' /></div>"
						+ "<div class='col l5 m5 s9 disabled'>"
							+"<a class='btn white green-text' onClick='b_bizorder_update();' >Add Email</a></div>"
						+ "<div class='col l1 m1 s2 right'>"
							+"<span class='btn-floating center red white-text'  onClick='bx_more_remove();' >X</span></div>"
						+ "</div>";

	$($parent_div).append($str_div);
	
}
/****************MORE END***********************/

/*** FX:: BizOrder BEGIN ***/

/*
* FX: gx_div_orderlist: Div for OrdeR List 
* Orders List 
* @returns: div string
*
*/
function gx_div_orderlist()
{
	var $gdo;
	
	$gdo = " "
		+ "<input id='branch_tablenumber' value='' name='tablenumber' type='hidden' /> "

		+ "<div class='row'  ><div class='col s12' id='fto_neworder'></div></div>"	

		+ "<div align='center' id='list_of_orders' >" 
			+ $progress_clock
		+ "</div>"

		+ "<div  hidden id='refresh_token' > </div>"
		+ "<div  hidden id='current_time' ></div>";

		return $gdo;	
}

/*
* FX: b_bizorder_update: Updates the BizOrder for FTO/RESTAURANT
* BizOrder
*
*/
function b_bizorder_update()
{
   var $progress = $progress_clock;
   $("#bizorder").append($progress);

   $c_bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();

	var $base_url;
	var $appid = $('#in_appid').val();
  	if($appid == $c_appid_restaurant)
  	{
  		$base_url = "/restaurant/order/"+$c_bizorderid;
  	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$base_url = "/pickup/order/"+$c_bizorderid;  	
  	}	


	var $ran = 0;
 			 
   var $x_discount 		   = $("#discount").val();
	var $x_email				= $("#email").val();
   $("#abc").append($x_discount);

	var $h_xbo ={};
	if($x_discount || $x_email )
	{
			$h_xbo = { 'discount':$x_discount,'email':$x_email};
	}
	
	var $update_msg;
	if($x_discount)
	{
			$update_msg = "Discount percentage added.";
	}
	else if($x_email)
	{
			$update_msg = "Email added.";
	}

	var $url = $base_url;

	$.ajax(
	{
         url:  		$url,
         type: 				'PUT',
			data: 				$h_xbo,     					
    		traditional:		true,
			dataType: 			"json",
	 		success: function(xdata)
       	{
       		Materialize.toast($update_msg,2000);
				$("#more_btn_below").empty();       		
       	  	var orderid = xdata.orderid;
       	  	$("#bizorder").empty();
	  		         	  
	  		  $ran = dl_display_anorder($c_bizorderid);
         },
         error: function(xhr, status, err)
         {
				$("#bizorder").empty();
				$("#bizorder").append("Please try again.");
         },
      }
    );
		//Ajax End


	 //$( "#result" ).css( "backgroundColor", "blue" );
	  if($ran < 1)
	  {
				dl_display_anorder($c_bizorderid);
	  } 	  
	  $("#cart").html("");
	  $( '.cart_count' ).html("");
	  //$( "#bizorder" ).css( "backgroundColor", "yellow" );	
	  //$( '#shuffle' ).append( '<p>red</p>' );
	  return false;

	
}

/*
* FX: b_bizorder_cancel: Handles Bizorder Cancellation for FTO/Rest.
* BizOrder
* https://jsonformatter.curiousconcept.com/
* AJAX: Update
*
*/
function b_bizorder_cancel()
{
   var $progress = $progress_clock;
   $("#bizorder").append($progress);

   $c_bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();

	var $base_url;
	var $appid = $('#in_appid').val();
  	if($appid == $c_appid_restaurant)
  	{
  		$base_url = "/restaurant/order/"+$c_bizorderid;
  	}
  	else if($appid == $c_appid_foodtakeout)
  	{
  		$base_url = "/pickup/order/"+$c_bizorderid;  	
  	}	


	var $ran = 0;
 			 
   var $x_cancel_msg 		   = $("#cancel_msg").val();
   $("#abc").append($x_cancel_msg);

	var $h_xbo ={};
	if($x_cancel_msg )
	{
			$h_xbo = { 'message':$x_cancel_msg};
	}

	var $url = $base_url;

	$.ajax(
	{
         url:  		$url,
         type: 				'DELETE',
			data: 				$h_xbo,     					
    		traditional:		true,
			dataType: 			"json",
	 		success: function(xdata)
       	{
				$("#more_btn_below").empty();       		
       	  	var orderid = xdata.orderid;
       	  	$("#bizorder").empty();
	  		         	  
	  		  $ran = dl_display_anorder($c_bizorderid);
         },
         error: function(xhr, status, err)
         {
				$("#bizorder").empty();
				$("#bizorder").append("Please try again.");
         },
      }
    );
		//Ajax End


	 //$( "#result" ).css( "backgroundColor", "blue" );
	  if($ran < 1)
	  {
				dl_display_anorder($c_bizorderid);
	  } 	  
	  $("#cart").html("");
	  $( '.cart_count' ).html("");
	  //$( "#bizorder" ).css( "backgroundColor", "yellow" );	
	  //$( '#shuffle' ).append( '<p>red</p>' );
	  return false;

	
}


/* 
* FX: g_display_orders
* This function is for List of orders
* @params: incount,pages
* AJAX: GET
* 
*/
function	g_display_orders($incount,$inpage) 
{
	var $page =  $inpage || 1 ;
	var $rows =  30;		

	var $tablenumber;
	try 	{
				var $in_tablenumber = $('#branch_tablenumber').val();
				if($in_tablenumber)
				{
					$tablenumber = $in_tablenumber;
				}
			}
	catch(err)
	{
		$('#xyz').append(err);
	}	

  var $fnx;
  var $appid = $('#in_appid').val();
  if($appid)
  {
 		var $url;
  		if($appid == $c_appid_restaurant)
  		{
  			$url = "/restaurant/order/list";
  		}
  		else if($appid == $c_appid_foodtakeout)
  		{
  			$url = "/pickup/order/list";  	
  		}
  		else if($appid == $c_appid_retail)
  		{
  			$url = "/retail/order/list";  	
  		}
  
  		var $slash = "/"
  		$fnx = $url + $slash + 100 + $slash + $page;
  }
	
  var $div_add = '#list_of_orders';
	
	

	$.ajax( 
				{ 
					url: $fnx ,
					type: "GET",
					dataType: "json",
					headers: { 'tablenumber':$tablenumber },
 					error: function (xhr,status,error) 
 					{
 						var $me = "<span class='red-text'>No Orders."
 								+ "</span>";
 						$($div_add).empty();
 						$($div_add).append($me);
 						
            	},
					success: function( orderdata )
					{
						g_order_list(orderdata,$appid);
					},
				}	
	);		
		
}

/*
* FX: g_orders_tokenrefresh: This function is to Token Refresh for Order Page.
*
* Refresh_token: #refresh_token
*
*/
function	g_orders_tokenrefresh() 
{

  var $fnx;
  var $appid = $('#in_appid').val();
  if($appid)
  {
 		 var $url;
  		if($appid == $c_appid_restaurant)
  		{
  			$url = "/restaurant/order/refresh";
  		}
  		else if($appid == $c_appid_foodtakeout)
  		{
  			$url = "/pickup/order/refresh";  	
  		}
  		else if($appid == $c_appid_retail)
  		{
  			$url = "/retail/order/refresh";  	
  		}
  
  		$fnx = $url;
  }
	
	
		return	$.getJSON( $fnx, 
		function( token ) 
				{
					$('#refresh_token').html( token );
  					
  					})
   		
}

/*
* FX: g_ordeR_list: Used by Diner/order, 
* Div Order: of List Order
* @params: orderdate,appid
* DIV: #list_of_orders
*
*/
function g_order_list(orderdata,appid)
{     
		var items = [];
		var $fto_link = "/foodtakeout/order/"; 
		var $rt_link = "/diner/orderdiner/";
		var $url;

		rpage_display(orderdata);
		//
  				
		$.each( orderdata.items, function( key, val ) 
		{
	  		 		var $ot_on       = val.ordernumber;
	  		 		var $ot_odate    = val.bizorderdate;
	  		 		var $ot_bizorderid = val.orderid;
	  		 	   var $ot_appid	= val.appid;
	  		 	   var $ot_cancelled	= val.cancelled_at;	  		 	   
	  		 	   var $ot_link = '&nbsp;';

				   var $sx_type = $('#sx_type').val();
				   var $fn_click;
				   var $btnx;
					if($sx_type)
					{
						$fn_click = "onClick=g_display_existing_order('"+ $ot_bizorderid +"');"	

  	  		 			$btnx =  "<a class='btn-floating red white-text' "
	  	  		 					+ $fn_click
  	  			 					+ "> &rarr;</a>";
						
					} 	
	  		 	   else if($ot_appid == appid)
	  		 	   {

					   if( appid == $c_appid_restaurant)
					   {		  		 	   	
	  		 	   	
	  		 	   		$ot_link = $rt_link;
	  		 	   	
	  		 	   	}
	  		 	   	else if($ot_appid == appid && appid == $c_appid_foodtakeout)
	  		 	   	{
	  		 	   		$ot_link = $fto_link;
  	  			 		}
  	  		 			$btnx =  "<a class='btn-floating red white-text' href='"
	  	  		 					+ $ot_link
  		  		 					+ $ot_bizorderid
  	  			 					+ "'> &rarr; </a>";
  	  			 					
	  		 	   }//appid	
  	  		 	   
	  		 		var $str_order ="<span><b>OID-" + $ot_bizorderid + "</b> (" + $ot_appid +") </span>";
	  		 		var $ot_userid   = val.userid;
	  		 		var $str_user = "<p> &nbsp </p>";
	  		 		
					if($ot_userid)
					{
						$str_user = "<p>"+$ot_userid+"</p>";
					}	  		 		

					var $str_cancelled = "";	  		 		
					if($ot_cancelled)
					{
						$str_cancelled = "<b class='red-text'>(X)</b>";
					}
	  		 		
	  		 		var $ot_tnumber   = val.tablenumber;
	  		 		var $str_table ="<p>"+ $str_cancelled + "</p>";
					if($ot_tnumber )
					{
						$str_table ="<p>T-" + val.tablenumber +  " " + $str_cancelled +"</p>";
					}
					else
					{
						$str_table = "<p>&nbsp;</p>";
					}
	  		 		
	  		 		var $o_a   = "<div class='card  col l4 m6 s12 white border' id="+$ot_on+">"
	  		 					+ "<div class='card-content'>"
	  		 					+ 		"<span class='card-title black-text'>"
	  		 					+ 		"O-" + $ot_on 
	  		 					+ 		"</span>"
	  		 					+ "<p>" + $ot_odate + " </p>"
	  		 					+ $str_order
  	  		 					+ $str_user
  	  		 					+ $str_table
	  		 					+ "</div>"
								+ "<div class='card-action '>"	  		 					  	  		 					
  	  		 					+ $btnx
	  		 				   +	"</div>"  	  		 					
	  		 				   +	"</div>";
						    		
		    					items.push($o_a);
  		});
  					//ForEach
 
		$('#list_of_orders').empty().append($( "<div/>", 
    				{	
       					"class": "row",
       					"html": items.join( "" )
    	} ));
	
}

/*
*
* FX: g_display_existing_order: Display an order
* USed through Orders List
* @params: orderid
* 
*
*/
function g_display_existing_order($orderid)
{
		if($orderid)
		{
				$("#sx_type").val('bizorder');
				$("#sx_value").val($orderid);
				$("#bizorderid").val($orderid);
				move_pager('bizorder');
		}		
}

/*
* Update Order List Page
* Old Token: #refresh_token, Time: #current_time
* FX: g_update_orderlist
*
*/
function g_update_orderlist() 
{
	
	setTimeout
	(
        function()
        {
	         var old_token = $( "#refresh_token" ).text();
				//console.log(old_token);
				var ctime = $( "#current_time" ).text();
				//console.log(ctime);
				if(ctime - old_token <= 10)
				{
						g_display_orders();				
				}
   		}, 1000
	);

}

/*** FX:: User Utils BEGIN ***/

/* 
* FX: gx_user_notices(inpage,url):This function is for List of Notices
* @params: incount,pages
* AJAX: g/user/notices
* 
*/
function	gx_user_notices($inpage,$in_url) 
{
	var $page =  $inpage || 1 ;
	var $rows =  30;		

	var $slash = "/";
	var $url = '/g/user/notices';
	if($in_url)
	{
			$fnx = $in_url;
	}	
	else
	{
		$fnx = $url + $slash + $page;
	}
	
	var $div_add = '#list_notices';

	$.ajax( 
				{ 
					url: $fnx ,
					type: "GET",
					dataType: "json",
 					error: function (xhr,status,error) 
 					{
 						var $me = "<span class='red-text'>No Notices."
 								+ "</span>";
 						$($div_add).empty();
 						$($div_add).append($me);
            	},
					success: function( a_notices )
					{
							g_anotices($div_add,a_notices);
					},
				}	
	);		
		
}

/*
* FX: g_anotices: ForEach Noteces
* @params: div-id, data
* Appends data to div-id
* 
*/
function g_anotices($in_div,a_notices)
{

	  var items 			 = [];

	  $($in_div).empty();
	  var $div_pgx = Mustache.to_html($mst_notices_pgx, a_notices);
	  items.push($div_pgx);	


	  var $count = 0;	  
	  $.each( a_notices.notices, function( key, val ) 
	  {
	  	    $count++;
	   	 var $div_pf = Mustache.to_html($mst_a_notice, val);	  	    
	  		 items.push($div_pf);
  	  }) 

  $( "<ul/>", 
  {
    "class": "collection",
    html: items.join( "" )
  }).appendTo( $in_div); 

}
/*
*
* MST: Notices and Pagination
*
*/
var $mst_a_notice = 				"<li class='row collection-item "+ $btx4_color +"'>"
				            		  		+ "<div class='col l1 m1 s2 left-align '>"
				            		   		+ "{{notifyid}}"
				            		  			+ " </div>"
				            		  		+ "<div class='col s8		 right-align'>"
				            		   		+ "{{message}}"
				            		  			+ " </div>"
				            		  		+ "<div class='col s12   "+ $btx3_color +" '>"
				            		   		+ "{{active_from}} to {{active_till}}"
				            		  			+ " </div>"
            		  	        	+ "</li>";
            		  	        	
var $mst_notices_pgx = 	"<li><div class='card row yellow lighten-3'>"
						+ "<li><div class='row'>"
							+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=gx_user_notices('1','{{previous}}');"							
									+ "> "
									+ "<< </span>"

							+ "<span class='btn col l4 m4 s6 red'>{{page}}</span>"
							+ "<input id='current_page' value='{{page}}' type='hidden' />"
							
							+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=gx_user_notices('1','{{next}}');" 	 
									+ "> "
									+ ">> </span>"
	  						+ "</div></li>"
	  				;
            		  	        	


/************* APP Setup Stuff ***/
/*
* FX: gx_app_setup: This function is for App Setup Status
* @params: divid,branchid,appid
* AJAX: GET
* 
*/
function	gx_app_setup($div_add,$branchid,$appid) 
{
	var $slash = "/";
	
	var $url = '/corp/list/app_setup/'+$branchid+$slash+$appid;

	$.ajax( 
				{ 
					url: $url ,
					type: "GET",
					timeout: 7000, 
					dataType: "json",
 					error: function (xhr,status,error) 
 					{
 						var $me = "<span class='red-text'>No info."
 								+ "</span>";
            	},
					success: function(h_setup_data)
					{
							$($div_add).empty();
							
							g_app_setupinfo($div_add,h_setup_data);
					},
				}	
	);		
		
}


/*
* FX: g_app_setupinfo: Display Readines by changing the DOM
* Given Div is appended.
*
* 
*/

function g_app_setupinfo($div_add,h_setup_data)
{
	var $is_ready = h_setup_data.app_setup_status;
	if($is_ready > 0)
	{
  	   	 var $msg = "<p>Ready</p>";
   	 	 $("#" + $div_add).append($msg);
	}
	else
	{
			var $count = 0;
	  		$.each( h_setup_data.pending, function( index,value ) 
	  		{
	  	   	 $count++;
	  	   	 var $msg = "<p>" + $count + ": " + value +"</p>";
	   	 	 $("#" + $div_add).append($msg);
	   	 	 //Materialize.toast("Pending",2000);	  	    
  	  		}); 
				
	}
	
	return ;
}
/************* APP Setup Stuff  END ***/


/************* User LoggedIn Stuff. ***/
/* 
* This function is to check if user is logged
* @params: divid
* AJAX: GET
* 
*/
function	gx_is_user_logged($div_add) 
{
	var $url = '/g/user';

	$.ajax( 
				{ 
					url: 			$url ,
					type: 		"GET",
					timeout: 	5000, 
					dataType: 	"json",
 					error: function (xhr,status,error) 
 					{
 						var $me = "<span class='red-text'>Information cannot be received about the user."
 								+ "</span>";
            	},
					success: function(xdata)
					{
						var $userid = g_user_login(xdata);
						$($div_add).append($userid);
					},
				}	
	);		
	
	return ;
		
}

/*
* FX: g_user_login: gets the current login
* Msg based on logged in or not.IF User is not Logged then Toast: User is not logged in.Else Nothing
* 
* @returns: UserID.
*
*/
function	g_user_login(xdata)
{
	if(xdata.userid == 'UNKN' )
	{
		var $msg = "User is not logged in";
		Materialize.toast($msg, 5000);							
	}
	else
	{
		//Materialize.toast("Logged In", 5000);
		return xdata.userid; 
	}
}

/*
* MST: Login Form
*
*/

var $mst_form_login ="<div class='card-panel'>"
	+ "<div class='row '>"
			 + "<div class='col s12 red-text'>"
			 		+ "<h5>Bael</h5>"
			 	+ "</div>"
			 + "<div class='col s12'>"
			 		+ "Sign in to continue"
			 	+ "</div>"
		+ "</div>"
	+ "<div class='row '>"
       + "<div class='input-field col s12'>"
		       + "     <input id='userid' type='email' name='userid' class='validate' />"
			    + "     <label for='userid' data-error='Wrong Email'>"
		   	 + "	      	Email or phone</label>"
       + "</div>"
	+ "</div>"
        
   + "<div class='row '>&nbsp;</div>"
   + "<div class='row '>"
        + "  <div class='input-field col s12'>"
        + "    <input id='password' type='password' name='password' class='validate' required>"
        + "    	<label for='password' >Password"
        + "    		</label>"
        + "  </div>"
   + "</div>"
        
   + "<div class='row'>"          
        + "  <div class='input-field col s12 m12 l12  login-text'>"
        + "      <input type='checkbox' id='remember-me' />"
        + "      <label for='remember-me'>Remember me</label>"
        + "  </div>"
   + "</div>"
        
   + "<div class='row'>"
   	  + "  <span id='progress_sign_in'></span>"
        + "  <button class='col s10 m10 l10 offset-s1 offset-l1 offset-m1 " 
        + "  					btn waves-effect waves-light green' type='button' "
        + " onClick='g_sign_in();' > "
        + "  		&#x279f; &#x25a1;   Sign In" 
  		  + "	 </button>"
   + "</div>" 	

   + "<div class='row'>"            		
        + "  <a 		class='col s10 m10 l10 offset-s1 offset-l1 offset-m1 " 
        + " 		btn waves-effect waves-dark red-text white border' " 
        + "  					href='/login/forgot_password' > "
        + "  						<b>Forgot password ? </b> "
        + "    	</a>"
   + "</div>" 	

   + "	<div class='row'> "            		
		  + "		<a class='col s10 m10 l10 offset-s1 offset-l1 offset-m1 " 
		  + "				btn white border blue-text waves-effect waves-dark ' href='/login/google' > "
		  + "			Sign In with G+</a> "
	+ "	</div>"

   + "<div class='row'> "
        + "    <a class='col s10 m10 l10 offset-s1 offset-l1 offset-m1 "
        + "   		btn white border blue-text  waves-effect waves-dark' href='/registration'> "
        + "    		Register Now! "
        + "    		<b>&#9998;</b>"
        + "    	</a>"
	+ "	</div>"
				

   + "<div class='row'>"            		
        + "    <a class='col s10 m10 l10 offset-s1 offset-l1 offset-m1 "
        + "			btn white border blue-text  waves-effect waves-dark' href='/registration/validate'> "
        + "    			Verify Email Address</b></a>"
        + " </div>"
   + "</div>"  
        ;


/*
*
* FX: gx_form_sign: Create a Form for Sign In
* 
*/
function gx_form_sign($in_div)
{
	var $str_div;	
	var $str_div = Mustache.to_html($mst_form_login);

	if($in_div)
	{
		$($in_div).append($str_div);
	}

	return $str_div;	
}

/*
* FX: g_sign_in
* Sign In Form: Checks the Data
* Checks through AJAX API: /g/user/login
*
*/
function g_sign_in()
{
	var $userid 	= $("#userid").val();
	var $password	= $("#password").val();
	
	$h_user = {
					userid: $userid,
					password: $password,
			    };
		
	var $url = "/g/user/login";	
	var $progress_msg = $progress_clock + "Please wait.....";
	var $div_wait = "#progress_sign_in";
	
	if(!$userid || !$password)
	{
			bx_toast_empty();
			Materialize.toast("User ID or password is missing", 5000);
			$($div_wait).empty();	
	}
	else if($userid && $password && $url )
	{
				bx_toast_empty();
				Materialize.toast($progress_msg, 5000);
				$($div_wait).append($progress_msg);
								
				$.ajax( 
							{ 
								url: 			$url ,
								type: 		"POST",
								timeout: 	5000,
								data:			$h_user, 
								dataType: 	"json",
		 						error: function (xhr, status, error) 
 								{
									$($div_wait).empty();
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);			 								
			            	},
								success: function(xdata)
								{
									$($div_wait).empty();
									bx_toast_empty();
									Materialize.toast("You have signed in.", 5000);
									window.location.href = '/home';
								},
							}	
				);// ajax		
		
	}	
	
	
}

/************* User LoggedIn Stuff. END	 ***/



/*** FX:: Credit Note BEGIN ***/

/*
* FX: bx_bill_begin_credit_note(div_class): Creates input boxes for txns (prod/serv)
*   
* 
*/
function bx_bill_begin_credit_note($div_class)
{
	//1. Display Prod/Serv Txns
	bx_div_class_show('.order_return_txn');
	
}

/*
*  
* FX: bx_bill_create_Credit_note: Create Credit Note
* Collects the Data and Send ti to fn for Creation
*
*/
function bx_bill_create_credit_note()
{

	var $in_msg_id = "#inbill_cnote_message";
	var $in_msg = $($in_msg_id).val();	
		
	var $div_class_collect = ".order_return_txn";
	//1. Display Prod/Serv Txns
	bx_div_class_show($div_class_collect);
	
	//1. GET BizOrderID,BranchID
	var $branchid;
	$branchid   = $("#in_branchid").val() ;
	//Materialize.toast($branchid, 2000);

	var $bizorderid;var $billid;var $appid;
	$bizorderid = bgx_get_bizorderid();
	$appid		= bgx_get_appid();
	
	
	//2. Get TxnID and Units, ForEach
	var $h_rx = bx_cn_txns_units($div_class_collect);
	var $a_product_returns 	= $h_rx['returns'];
	var $count					= $h_rx['count'];
	
	if(!$branchid || !$appid)
	{
		var $e_msg = "Branch/App is not available.";	
		Materialize.toast($e_msg, 2000);
	}	
	else if(!$bizorderid)
	{
		var $e_msg = "Order ID is not available.";	
		Materialize.toast($e_msg, 2000);
	}
	else if(!$in_msg)
	{
		var $e_msg = "Reason is not available.";	
		Materialize.toast($e_msg, 2000);
	}
	else if($count < 1)
	{
		var $e_msg = "Select product for return.";	
		Materialize.toast($e_msg, 2000);
	}
	else if($branchid && $bizorderid && $appid && $in_msg && $a_product_returns)
	{	
		//3. Ajax POST to Create CreditNote.
			var $h_input = {
						 'product_returns':JSON.stringify($a_product_returns),
						 'ref_bizorderid'	:$bizorderid,
						 'customerid'		:'',
						 'appid'				: $appid,
						 'message'			: $in_msg,
					};	
			Materialize.toast("Creating Credit Note", 2000);
			bx_more_remove();						
			bx_create_cn($branchid,$h_input);
	}
	else
	{
		var $e_msg = "Error ";	
		Materialize.toast($e_msg,2000);
	}	
	
}

/*
* FX: bx_create_cn: Data is already created. Now Create the CN
* AJAX API: bx_create_cn
* @params: branchid,input_hash{tnxid,units}
*
*/

function bx_create_cn($branchid,$h_input)
{

	//1. Create the Note
	var $base_url = "plan/credit/"+$branchid;	
	
		$.ajax( 
				{ 
						url: 			$base_url ,
						type: 		"POST",
						timeout: 	5000,
						data: 		$h_input,						
						dataType: 	"json",
		 				error: function (xhr, status, error) 
 						{
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);			 								
			         },
						success: function(xdata)
						{
									bx_toast_empty();
									Materialize.toast("Credit Note is created", 3000);
									var $billid = xdata.billid;
									rl_credit_note($billid);
						},
				}	
				);// ajax		

	
	//2. Redirect to CreditNote Bill Or display a Bill

	
}


/*
* Collect All the Items for Credit Note Returns
* .order_return_txn -> .in_cn_txnid,.in_cn_units
* FX: bx_cn_txns_units: collects data tnxid,units for PROD/SERV
* @returns: hash{txnid,units},count
*
*/
function bx_cn_txns_units($in_div)
{
        var $fitems = [];
        var $count = 0;
        $( $in_div ).each(function()
         {
                         
               var $txnid               = $( this ).find(".in_cn_txnid").val();
               var $units               = $( this ).find(".in_cn_units").val();
               $units = parseInt($units);
               $txnid = parseInt($txnid);
                                 
               if($txnid && ($units > 0) )
               {
                   var $xah = { 'txnid':$txnid,'units':$units};
                   $fitems.push($xah);
                   $count++;
                   //var $msg = "Return " + $txnid + " Units:" + $units;
                   //Materialize.toast($msg, 2000);
               }
        }
        );
        //ForEach End

		  return {'returns':$fitems,'count':$count};
		  //Use in AJAX
        //data: {'items':JSON.stringify($fitems)},
                        
} 

/*** FX:: DEBIT Note BEGIN ***/


/*
*  
* Shows Return divs under each Prod/Serv Txns
* FX: bx_bill_begin_debit_note(div-id)
* Opens up the Bills Txns input for Returns (PROD/SERV)
*
*/
function bx_bill_begin_debit_note($div_class)
{
	//1. Display Prod/Serv Txns
	bx_div_class_show('.order_return_txn');
	
}

/*
*  
* FX: bx_bill_create_debit_note: collects data and sends it to bx_create_dn
* @params:
* 
*/
function bx_bill_create_debit_note()
{

	var $in_msg_id = "#inbill_dnote_message";
	var $in_msg = $($in_msg_id).val();	
		
	var $div_class_collect = ".order_return_txn";
	//1. Display Prod/Serv Txns
	bx_div_class_show($div_class_collect);
	
	//1. GET BizOrderID,BranchID
	var $branchid;
	$branchid   = $("#in_branchid").val() ;
	//Materialize.toast($branchid, 2000);

	var $bizorderid;var $billid;var $appid;
	$bizorderid = bgx_get_bizorderid();
	$appid		= bgx_get_appid();

	//To Handle Multiple clicks
	var $mrc_in							= 	"#is_more_return_clicked";
	var $is_more_already_clicked	= $($mrc_in).val();
	
	//2. Get TxnID and Units, ForEach
	var $h_rx = bx_cn_txns_units($div_class_collect);
	var $a_product_returns 	= $h_rx['returns'];
	var $count					= $h_rx['count'];

	if(	$is_more_already_clicked == 0
			&& $branchid && $bizorderid && $appid 
			&& $in_msg && $a_product_returns && $count > 0
		)
	{	
			$($mrc_in).val("1");
			var $h_input = {
						 'product_returns':JSON.stringify($a_product_returns),
						 'ref_bizorderid'	:$bizorderid,
						 'customerid'		:'',
						 'appid'				: $appid,
						 'message'			: $in_msg,
					};	
			
			Materialize.toast("Creating Debit Note" + $in_msg, 2000);
			bx_more_remove(); 							//removes more entry						
			bx_create_dn($branchid,$h_input);    //Create DN
	}
	else
	{
			var $e_msg = "Error";

			if($is_more_already_clicked > 0)
			{
					$e_msg = "Button is already clicked";
			}			
			else if(!$branchid || !$appid)
			{
				$e_msg = "Branch/App is not available.";	
			}	
			else if(!$bizorderid)
			{
				$e_msg = "Order ID is not available.";	
			}
			else if(!$in_msg)
			{
				$e_msg = "Reason is not available.";	
			}
			else if($count < 1)
			{
				$e_msg = "Select product for return.";	
			}

			Materialize.toast($e_msg,2000);
			$($mrc_in).val("0");	
	}
	
}

/*
* FX: Create DN through API Here.
* @params: branchid,Input_form: returns{txnid,units}
* AJAX: purchase_return
*
*/
function bx_create_dn($branchid,$h_input)
{

	//1. Create the Note
	var $base_url = "purchase/return";	
	
		$.ajax( 
				{ 
						url: 			$base_url ,
						type: 		"POST",
						timeout: 	5000,
						data: 		$h_input,						
						dataType: 	"json",
		 				error: function (xhr, status, error) 
 						{
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);			 								
			         },
						success: function(xdata)
						{
									bx_toast_empty();
									Materialize.toast("Debit Note is created", 3000);
									var $billid = xdata.billid;
									mb_po_abill($billid);
						},
				}	
				);// ajax		

	
	//2. Redirect to CreditNote Bill Or display a Bill

	
}

/*
*
* FX: bx_dn_txns_units: Collect All the Items(Products and units) for Debit Note Returns 
* .order_return_txn -> .in_cn_txnid,.in_cn_units
* @aparams: div-id
* @returns: {returns{txnid,units},count};
* 
*/
function bx_dn_txns_units($in_div)
{
        var $fitems = [];
        var $count = 0;
        $( $in_div ).each(function()
         {
                         
               var $txnid               = $( this ).find(".in_cn_txnid").val();
               var $units               = $( this ).find(".in_cn_units").val();
               $units = parseInt($units);
               $txnid = parseInt($txnid);
                                 
               if($txnid && ($units > 0) )
               {
                   var $xah = { 'txnid':$txnid,'units':$units};
                   $fitems.push($xah);
                   $count++;
                   //var $msg = "Return " + $txnid + " Units:" + $units;
                   //Materialize.toast($msg, 2000);
               }
        }
        );
        //ForEach End

		  return {'returns':$fitems,'count':$count};
		  //Use in AJAX
        //data: {'items':JSON.stringify($fitems)},
                        
} 


/*
* FX: fbx_form_in_items(in_div)
* Get each rows: .item_pc , .item_unit from the in_div
* .item_pc and .item_unit are text not inputs.
* Returns: Array of Hash {product_code,units}
* Used by diner.js/ dl_bizorder_finalize
* 
*/
function fbx_form_in_items($in_class)
{

	var $div_class= $in_class || '.cart_row';
	var $fitems = [];		
	
	$( $div_class).each(function()
		  {
			    $item_pc 		   = $( this ).find(".item_pc").text();
		   	 $item_unit_str	= $( this ).find(".item_unit").text();

			    $item_unit= parseInt($item_unit_str);
				 
				 if($item_pc && ($item_unit >= 0) )
				 {
						var $xah = { 'product_code':$item_pc,'units':$item_unit};
					   $fitems.push($xah);
				 }
     	}
	);

	return $fitems;

}
