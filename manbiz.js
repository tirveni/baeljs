/*
	 2017-07-04
	 bizman.js, Admin Utilities for Bael

    This file is part of BAEL
    Copyright (C) 2017,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/


/*
* 
Naming Conventions:
Constant Variables Prefix:  mbv_
	Functions Prefix :  			 mbf_

xhr: 			XMLHttpRequest

*
*/


var $mbv_mst_pp_form = 
	"<div class='card-panel "

			+" {{#formid}} orange lighten-4  {{/formid}} "
			+" {{^formid}} white {{/formid}}  ' "

			+" {{#formid}} id='{{formid}}' {{/formid}} "
			+" {{^formid}} id='form_new' {{/formid}}  >"

			+ "{{#exist_from_date}}"
				+ "<input name='exist_from_date' value='{{exist_from_date}}' type='hidden'  />"
				+ "<input name='exist_till_date' value='{{exist_till_date}}' type='hidden' />"
			+"{{/exist_from_date}}"
			
      	+ "<div class='row border'> "
	      	+ "	<div class='col l6 m6 s12'> "
	      	+ "			Date Begin<input type='date' id='i_begin' class='datepicker' name='begin_date' "
	      	+ "			{{#from_date}}	data-value='{{from_date}}' {{/from_date}} >"
	      	+ "</div>"
	      	+ "	<div class='col l6 m6 s12'>		"
	      	+ "			Date End <input type='date' id='i_end' 	 class='datepicker' name='end_date' "
	      	+ "			{{#till_date}}	data-value='{{till_date}}' {{/till_date}} >"
	      	+ "</div>"
      	+ "</div>"   
	
          + " <div class='row border'>"
			          	+ "  <div class='s6  col input-field '>Price"
			          	+ " {{#currencycode}} ({{currencycode}}) {{/currencycode}} "
					    	+ "       <input id='biz_branch_productprice_price' type='number' min='0.00' " 
			          	+ "    			placeholder='Price' name='biz_branch_productprice_price' "
			          	+ "    			value='{{price}}' required />"
	          	+ "  </div>"
	          	+ "{{#from_date}}"
						 	+ "	<div class='col s6 input-field '>"
						 	
						 	+ "{{{active_select}}}"

							+ "    </div>"
					+ "{{/from_date}}"
			+ "</div>"
      
         + " <div class='row border'> "
		            

					+ "   <div class='col l4 m4 s4  col '>Input Price</div> "		          
		         + "   <div class='col l8 m8 s8  col input-field '> "
				   + "        <input id='biz_branch_productprice_input_price' type='number' min='0.00' " 
		         + "     			placeholder='Input Price' name='biz_branch_productprice_input_price' "
		         + "     			value='{{input_price}}'  /> "
		         + "   </div> "

					+ "	<div class='col l4 m4 s4'>VAT</div><div class='col l8 m8 s8 left-align'> "
									+ "{{{vat_select}}}"
					+ "    </div>"
		         + "   <div class='col l12 m12 s12 black-text'> "
		         + "   	If vat is enabled Item Taxes are calculated on difference between Price and Input Price." 
		         + "   </div> "
					
					
			+ "	</div>"
					+ "{{#till_date}}"

	               		+ "<div class='row'>"
            					+ "<div class='col s5 m5 s11'>"
               					+ "   <button type='submit' class='btn green waves-effect waves-light '  name='submit' "
                        		+ "		value='edit_price' onClick=mbf_edit_pprice('{{str_formid}}'); >"
                        		+					"Update Price </button>"
            					+ "</div>"
            				+ "</div>"	

            				+ "{{#happy_pricing}}"
            				+ "<div class='row'>"
	            				+ "<div class='col l6 m6 s11'>"
	                        	+	"<a class='btn orange' href='/biz/branch/pprice_happy/"
	                        		+"{{branchid}}/{{product_code}}/{{appid}}/{{from_date}}/{{till_date}}"
	                        	+"'>Happy Hour Pricing</a>"
	            				+ "</div>"
            				+ "</div>"
            				+ "{{/happy_pricing}}"

               + "{{/till_date}}"

               + "{{^till_date}}"
          				+ "<div class='row collapse'>"
            				+ "<div class='s12  col'>"
               			+ " <button type='submit' class='btn green waves-effect waves-light ' name='submit' " 
               			+ "         value='add_price' onClick='mbf_add_pprice();' >Save Price</button> "
            				+ "</div>"
          				+ "</div>"
               + "{{/till_date}}"

					+ "<div id='pp_existing_api'>"
					+ "</div>"
		
			
	+ " </div>";

var $mst_timex_weekday = "<div class='col l12 m12 s12 center input-field'>"
									    +	"			<select  name='day_of_week' class='browser-default' >"
									    + "					<option value='' >Select Day</option>"
									    + " 							<option value='1' >Monday</option>"
									    + "							<option value='2' >Tuesday</option>"
									    + "							<option value='3' >Wednesday</option>"
									    + "							<option value='4' >Thursday</option>"
									    + "							<option value='5' >Friday</option>"
									    + "							<option value='6' >Saturday</option> "
									    + "							<option value='7' >Sunday</option>"
									    + "					</select>"
									    + "		</div>";		

/***  Product Happy Hour Price  ***/
var $mbv_mst_pptd_form = 
	"<div class='card-panel "

			+" {{#formid}} orange lighten-4  {{/formid}} "
			+" {{^formid}} white {{/formid}} ' "
	
			+" {{#formid}} id='{{formid}}' {{/formid}} "
			+" {{^formid}} id='form_new' {{/formid}}  >"

			+ "{{#exist_till_time}}"
				+ "<input name='exist_from_time' value='{{exist_from_time}}' type='hidden'  />"
				+ "<input name='exist_till_time' value='{{exist_till_time}}' type='hidden' />"
				+ "<input name='exist_day_of_week' value='{{exist_day_of_week}}' type='hidden' />"				
			+"{{/exist_till_time}}"

			+ "{{#till_time}}"
			+ "{{/till_time}}"
			+ "{{^till_time}}"
		         + " <div class='row'> "
							+ $mst_timex_weekday
					+ "	</div>"
					
		      	+ "<div class='row border'> "
			      	+ "	<div class='col l6 m6 s12'> "
			      	+ "			Time Start <input type='date' id='begin_time' class='timepicker' name='begin_time' "
			      	+ "			{{#from_time}}	data-value='{{from_time}}' {{/from_time}} >"
			      	+ "</div>"
			      	+ "	<div class='col l6 m6 s12'>		"
			      	+ "			Time Start<input type='date' id='end_time' 	 class='timepicker' name='end_time' "
			      	+ "			{{#till_time}}	data-value='{{till_time}}' {{/till_time}} >"
			      	+ "</div>"
		      	+ "</div>"   
      	+ "{{/till_time}}"
	
          + " <div class='row border'>"
			          	+ "  <div class='s6  col input-field '>Price"
			          	+ " {{#currencycode}} ({{currencycode}}) {{/currencycode}} "			          	
					    	+ "       <input id='biz_branch_prodhh_price' type='number' min='0.00' " 
			          	+ "    			placeholder='Price' name='biz_branch_productprice_price' "
			          	+ "    			value='{{price}}' required />"
	          	+ "  </div>"
	          	+ "{{#till_time}}"
						 	+ "	<div class='col s6 input-field '>"
						 	
						 	+ "{{{active_select}}}"

							+ "    </div>"
					+ "{{/till_time}}"
			+ "</div>"
      
					+ "{{#till_time}}"
               		+ "<div class='row collapse'>"
            				+ "<div class='col l8 m8 s12'>"
               			+ "   <button type='submit' class='btn green waves-effect waves-light '  name='submit' "
                        + "		value='edit_price' onClick=mbf_edit_phh_price('{{str_formid}}'); >"
                        +					"Update Happy Hour Price </button>"
            				+ "</div>"
          				+ "</div>"
               + "{{/till_time}}"
               + "{{^till_time}}"
          				+ "<div class='row collapse'>"
            				+ "<div class='col l8 m8 s8 col'>"
               			+ " <button type='submit' class='btn green waves-effect waves-light ' name='submit' " 
               			+ "         value='add_price' onClick='mbf_add_phh_price();' >Save Happy Hour Price</button> "
            				+ "</div>"
          				+ "</div>"
               + "{{/till_time}}"

					+ "<div id='pp_existing_api'>"
					+ "</div>"
		
			
	+ " </div>";



/* USed for Product Price Edit */
var $a_pprice_row = "	<li class=''>" 
	   		+			"{{{form_row}}}"
	    		+ 	"<div class='collapsible-body'>"
				+ 			"{{{form_edit}}}"
				+ 			"</div>"	
				+ 	"</li>";	


/*
*
* FX: mbf_price_addform_close
*
*/
function mbf_price_addform_close()
{
	  $('#new_price_collapsible').removeClass('active');
}

/*
*
* FX: mbf_price_addform_open
*
*/
function mbf_price_addform_open()
{
	$('#new_price_collapsible').addClass('active');
}
	

/*
* FX: mbf_pprice_form: Add a PRice Form.
* @params: in_div,type
* Handles Div. Creates a div for the Price
*   
*/
function mbf_pprice_form($in_div,$in_type)
{
   $($in_div).empty();
	var $div_add;

	var $vat_active = 0;
	var $vat_select			= 
			g_drop_true_false("tax_on_value_added",$vat_active,"Enable/Disable VAT");
   var $current_date = g_current_date();
	var $currencycode = bgx_get_currencycode();
	

	var $h_type = {
							'begin':				1,
							vat_select:			$vat_select,
							'from_date':		$current_date,
							'currencycode':	$currencycode,
			};
	
	if($in_type == 'PRICE')
	{
		$h_type['type_form'] = 'PRICE';	
		$div_add = Mustache.to_html($mbv_mst_pp_form,$h_type);
	}
	else if($in_type == 'PRICETIMEDAY')    
	{
		$h_type['type_form'] = 'PRICETIMEDAY';
		$div_add = Mustache.to_html($mbv_mst_pptd_form,$h_type);
	}

	
	$($in_div).append($div_add);

	//DatePicker	
	g_datepicker_init();
	g_timepicker_init();

	
}


/*
* FX: mbf_pprice_collect_input: Collect Data for Product Price
* @params: none
* Returns: hash
* Works for Price, PriceTimeDay
*
*/
function mbf_pprice_collect_input($in_formid)
{

				var $form_div;
				$form_div = "#form_new";
				if($in_formid)
				{
					$form_div = $in_formid;
				}
				//Materialize.toast("Collect from:"+$in_formid,3000);

    			var $branchid			= $("#i_branchid").val();
   			var $in_productcode  = $("#i_productcode").val();
    			var $in_appid     	= $("#i_appid").val();
				
				var $id_price	= $form_div+" :input[name='biz_branch_productprice_price']";
				var $price		= $($id_price).val();
				var $in_status = $($form_div+" :input[name='price_status_in']").val();
				    			
				var $input_price   				= $($form_div+" :input[name='biz_branch_productprice_input_price']").val();
				var $tax_on_value_added     	= $($form_div+" :input[name='tax_on_value_added']").val();
				
				
				var $st_msg = $form_div + ":" + $in_status;
				//Materialize.toast($st_msg,5000);

				var $in_dt_a				= $($form_div+" :input[name='begin_date']").val();
				var $in_dt_b				= $($form_div+" :input[name='end_date']").val();	
				var $is_dt_increasing 	= g_dates_increasing($in_dt_a,$in_dt_b);
				
				var $exist_dt_a				= $($form_div+" :input[name='exist_from_date']").val();
				var $exist_dt_b				= $($form_div+" :input[name='exist_till_date']").val();	

				var $exist_time_a				= $($form_div+" :input[name='exist_from_time']").val();
				var $exist_time_z				= $($form_div+" :input[name='exist_till_time']").val();	
				var $exist_day_of_week		= $($form_div+" :input[name='exist_day_of_week']").val();								

				var $from_time				= $($form_div+" :input[name='begin_time']").val();
				var $till_time				= $($form_div+" :input[name='end_time']").val();		
				var $day_of_week			= $($form_div+" :input[name='day_of_week']").val();
				
		var $h_new = {
							branchid:			$branchid,
							appid:				$in_appid,
							product_code:		$in_productcode,

							price:				$price,
							input_price:		$input_price,
							tax_on_value_added: $tax_on_value_added,
							active:				$in_status,

							from_date:			$in_dt_a,
							till_date:			$in_dt_b,
							from_time:			$from_time,
							till_time:			$till_time,
							day_of_week:		$day_of_week,
							
							exist_from_date:	$exist_dt_a,
							exist_till_date:	$exist_dt_b,

							exist_from_time:	$exist_time_a,
							exist_till_time:	$exist_time_z,
							exist_day_of_week:$exist_day_of_week,
							
						 };
		//Materialize.toast("Collected Stuff",4000);					 		
		return $h_new;				
				
}

/*
* FX: mbf_existing_pprices: Checks for Existing Product Prices for an app and date-range.
* Used for REST/FTO Existing Prices Search.
*
*/

function mbf_existing_pprices($in_div)
{
	
	var $h_pp 			= mbf_pprice_collect_input();
			
	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	$in_dt_a  			= $h_pp.from_date;
	$in_dt_b  			= $h_pp.till_date;	

	var $is_multiple_pricing = $("#is_multiple_pricing").val();
	
	if ( $branchid && $in_product_code && $is_multiple_pricing != 't' && $in_dt_a && $in_dt_b)
	{
			mbfv_check_prices($h_pp);
   }
   else if(!$in_dt_a || !$in_dt_b)
   {
   		Materialize.toast("Start and End Date are required.",3000);
   }
   
}	

/*
* FX: mbfv_check_prices: Used by above, to check prices of a product in app for a date range
*
*/
function mbfv_check_prices($h_pp)
{
	
				$branchid 			= $h_pp.branchid;
				$in_product_code 	= $h_pp.product_code;
				$in_appid 			= $h_pp.appid;
				$in_dt_a  			= $h_pp.from_date;
				$in_dt_b  			= $h_pp.till_date;	
				
				
 	 			//$(this).css("background-color","#010101");
	 			var $all = ""+$branchid	+	"/"	+	$in_product_code	+	"/"	+$in_appid	+	"/"
					+ $in_dt_a + "/" + $in_dt_b;
	    
	   //$('#all_out').text("ALPHA Search");
	   //Make URL 	 

		var $url = "/biz/search/productprice/"+$all;	   
		var $result;var $is_check_only = 1;
		
		//Search Now.	    
		$.ajax({
			dataType: 			"json",
	 		url: 					$url,
	 		type: 				'GET',
	 		data:					$result,	
	 		contentType: "application/json",
	 		success: function(result)
	 		{
 				var all = mbf_pprices(result,$is_check_only);

				$($in_div).empty();				
	 		
	 			$($in_div).append("<p> Price Exist for this period</p> ");
	 			$($in_div).addClass('red');

				$( "<div/>", 
  				{
    					"class": "row",
    					html: all.join( "" )
  				}).appendTo( $in_div );			
	 			
    		}, 
    		error: function (xhr, status, error)
	 		{
   	      $($in_div).val('Go Ahead');
   	      Materialize.toast("Go ahead",5000);
    			$($in_div).css("background-color","#00f");    			   	      
    		},   		
	 	});
	
}

/*
* FX: mbf_add_pprice: add Product Price
*
*/
function mbf_edit_pprice($form_id)
{
	var $h_pp 			= mbf_pprice_collect_input($form_id);
	//Materialize.toast("Edit Price"+$form_id,1000);

	var $is_edit = 1;
	mbf_add_edit_pprice($h_pp,$is_edit);

	//Materialize.toast("Editing......",1000);
	
}

/*
* FX: mbf_add_pprice: add Product Price
*
*/

function mbf_add_pprice()
{
	var $h_pp 			= mbf_pprice_collect_input();
			
	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	$appid				= $h_pp.appid;
	
	$price 				= $h_pp.price;
	$from_date			= $h_pp.from_date;
	$till_date			= $h_pp.till_date;
	
	if($price && $branchid && $in_product_code && $from_date && $till_date)
	{
			mbf_add_edit_pprice($h_pp);
	}//IF	
	
}

/*
* FX: mbf_add_edit_pprice: This runs the Ajax POST/PUT. To Add / Edit Product Price.
*
*/

function mbf_add_edit_pprice($h_pp,$is_edit)
{
	
	var $exist_from_date			= $h_pp.exist_from_date;
	var $exist_till_date			= $h_pp.exist_till_date;

	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	$appid				= $h_pp.appid;
	
	var $err_add;var $err_edit;var $error;
	var $msg;var $edit_msg; 
	$err_add =   "Price not added.";
	$err_edit =  "Price not edited.";
	$error = $err_add;
	$msg   = "Price Saved";
	if($is_edit)
	{
		$msg = "Price Saved";
		$error = $is_edit;
	}


	var $base_url = "/corp/product/price/";
	var $url  ;	
	var $method_type = 'POST';
	if($exist_from_date && $exist_till_date)
	{
		$url			  = $base_url	+ $branchid 
				+ "/" + $in_product_code + "/" + $appid
				+"/"	+ $exist_from_date +"/"+ $exist_till_date ;
		$method_type = 'PUT';		
		$msg = "Price Edited";
	}
	else
	{
		$url			  = $base_url	+ $branchid +"/"+ $in_product_code +"/"+ $appid;
		$method_type  = 'POST';		
		$msg = "New Price Created";
	}

		
	$.ajax( 
							{ 
								url: 			$url ,
								type: 		$method_type,
								data: 		$h_pp,     					
								dataType: 	"json",
								statusCode: 
								{
		    								404: function() {
											$('#txr').append("You have been logged out.");    									
		    							},
		    					},		
				 				error: function (xhr, status, error) 
 								{
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);			 								
		            		},
								success: function( data )
								{

									Materialize.toast($msg,2000);		 

									mbf_pprice_form("#edit_add_pp","PRICE");//RefreshForm for Add
									mbf_price_addform_close();
									
									mbf_list_pprices("#list_pprices");

									if(!$is_edit)
									{
										mbf_pprice_form("#edit_add_pp","PRICE");
									}
									
								},
						}	
	);
	//Ajax
	
	
}
								
/*
* FX: mbf_list_pprices: List Product Prices from Ajax
* @params: input Div
* 
*/
function mbf_list_pprices($in_div,$in_url)
{

	var $page			= 1;		
	var $h_pp 			= mbf_pprice_collect_input();
			
	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	
	//var $wait_msg = "Waiting for existing prices...." + $progress_clock;
	//Materialize.toast($in_div + $wait_msg,5000);
	progress_append($in_div,$wait_msg);

	 if ( $branchid && $in_product_code)
	 {
			//Materialize.toast("Prod and Branch"+ $branchid + " " + $in_product_code,5000);
			var $h_pp 		= mbf_pprice_collect_input();
				
			$branchid 			= $h_pp.branchid;
			$in_product_code 	= $h_pp.product_code;
			$in_appid 			= $h_pp.appid;

			var $url;
			if($in_url)
			{
					$url = $in_url;
				//Materialize.toast("Found URL "+ $url,5000);
			}
			else
			{
		 		var $all = ""+$branchid	+	"/"	+	$in_product_code	+	"/"	+$in_appid + "/"+  $page;
				$url = "/corp/list/product_prices/"+$all;
				//Materialize.toast("Created URL "+ $url,5000);
			}

			var $wait_msg = "Prices are being fetched....";
			$($in_div).empty();
			progress_append($in_div,$wait_msg);
		
			//Search Now.	    
			$.ajax({
					timeout: 			$c_ajax_timeout_milisec,
					dataType: 			"json",
			 		url: 					$url,
			 		type: 				'GET',
			 		contentType: 		"application/json",
	 				error: function (xhr, status, error) 
					{
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
 	  								$($in_div).append($error_msg);
 	  								Materialize.toast($error_msg,5000);
		    		},   		
			 		success: function(result)
			 		{
		   	      	//Materialize.toast(" Rsult Found "+ $in_url,5000);
							bx_progress_empty($in_div);

							var $self_method		= 'mbf_list_pprices';
							var $h_pgx;
							if(result)
							{
									$h_pgx = {	next:			result.next,
													add_div:		$in_div,
													previous:	result.previous,
													page: 		result.page,
												 };
							
							
                			var $str_pgx =  bgx_pagination($h_pgx,$self_method);
			 					$($in_div).append($str_pgx);

			 					var $all_rows = mbf_pprices(result.items);
			 				
			 				 	$( "<ul/>", 
							  	{
    									"class": "collapsible accordion ",
    									html: $all_rows.join( "" )
  								}).appendTo( $in_div );

								g_collapse_init();
								g_datepicker_init();
  							}			 
					    		 
					   }, 		
	 			});  
	 	//Ajax End


 	  }//If 
	
}

/* A Product Price */
var $mbv_mst_pprice =
	   			"<div class='collapsible-header waves-effect waves-green {{row_color}} '>"
 					+"<div class='row' id='pp_{{from_date}}_{{till_date}}' >"
							+ "<div class='col l1 m1 s1 center tag'> &#10095;</div>"
							+ "<div class='col l5 m5 s11' >" 
								+"<input class='datepicker_display_only black-text' "
									+	" value='{{from_date}}' DISABLED size='9' />"
									+ "<=>"
								+"<input class='datepicker_display_only black-text' "
									+	" value='{{till_date}}' DISABLED size='9' />"
							+"</div> "
							+"<div class='col l3 m3 s6 switch radius center'> " 
									+ "<h5 class='{{#str_active}}  icon_check_ballot {{/str_active}} "
									+ " {{^str_active}}  icon_close {{/str_active}}' </h5>"
							+ "</div>"								
							+"<div class='col l3 m3 s6'>{{price}}"
			          			+ " {{#currencycode}} ({{currencycode}}) {{/currencycode}} "			          								
							+"</div> "
					+ "</div>"
				+ "</div>"				;

/*
* FX: mbf_pprices: Converts Ajax Return Data into Rows.
* Returns: String of All rows.
* @params: data
* 
*/

function mbf_pprices(data,$is_check_only)
{
		var $all_rows = [];
		var $t_count = 0;
		var $is_multiple_pricing = $("#is_multiple_pricing").val();
		var $is_happy_pricing;
		if($is_multiple_pricing == 't')
		{
				$is_happy_pricing;
		}
		else
		{
				$is_happy_pricing = 't'; 
		}
		
		$(data).each(function(index, val) 
		{
				$t_count++;
				var $pp_active 	= val.active;
				var $vat_active	= val.tax_on_value_added;
				var $from_date 	= val.from_date;
				var $till_date 	= val.till_date;
				var $currency_code = val.currencycode;	
				
				var $formid 		= "edit_"+$from_date+"_"+$till_date;
				var $str_formid 	= "#"+$formid;


				var $str_active;
				if($pp_active > 0)
				{
					$str_active = 1;
				}

				 $row_color = 'white'
				 if( ($t_count % 2) == 0)
				 {
					 		$row_color = 'amber lighten-4 black-text';
				 }			    
				

				val.str_active				= $str_active;
				val.active_select			= 
					g_drop_true_false("price_status_in",$pp_active,"Enable/Disable Price");

				val.vat_select			= 
					g_drop_true_false("tax_on_value_added",$vat_active,"Enable/Disable VAT");

				
				val.formid					= $formid;
				val.exist_from_date 		= $from_date;
				val.exist_till_date 		= $till_date;
				val.str_formid 			= $str_formid;
				val.row_color				= $row_color;
				val.happy_pricing			= $is_happy_pricing;
				val.currencycode			= $currency_code;

				var $a_pp 					= Mustache.to_html($mbv_mst_pprice,val);
				if(!$is_check_only)
				{
						var $a_edit 				= Mustache.to_html($mbv_mst_pp_form,val);
						var $h_x 					= {form_row:$a_pp,form_edit:$a_edit,};
						
						var $a_row = Mustache.to_html($a_pprice_row,$h_x);
						$all_rows.push($a_row);
						//Create Form for Editing
				}
				else
				{
						$all_rows.push($a_pp);
						//Check Existing					
				}

		});
	
		return $all_rows;
}


/***** Product Prices ****/

/*** HappyHour Price Product ***/

/*
* FX: mbf_existing_pprices: Checks for Existing Product Prices for an app and date-range.
* Used for REST/FTO Existing Prices Search.
*
*/

function mbf_existing_phh_prices($in_div)
{
	
	var $h_pp 			= mbf_pprice_collect_input();
			
	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	$in_dt_a  			= $h_pp.from_date;
	$in_dt_b  			= $h_pp.till_date;	

	var $is_multiple_pricing = $("#is_multiple_pricing").val();
	
	if ( $branchid && $in_product_code && $is_multiple_pricing != 't' && $in_dt_a && $in_dt_b)
	{
			mbfv_check_hh_prices($h_pp);
   }
   else if(!$in_dt_a || !$in_dt_b)
   {
   		Materialize.toast("Start and End Time are required.",3000);
   }
   
}	

/*
* FX: mbfv_check_prices: Used by above, to check HH prices of a product in app for a date range
*
*/
function mbfv_check_hh_prices($h_pp)
{
	
				$branchid 			= $h_pp.branchid;
				$in_product_code 	= $h_pp.product_code;
				$in_appid 			= $h_pp.appid;
				$in_dt_a  			= $h_pp.from_date;
				$in_dt_b  			= $h_pp.till_date;	
	$from_time			= $h_pp.from_time;
	$till_time			= $h_pp.till_time;
	$day_of_week		= $h_pp.day_of_week;		
				
				
				
	 var $all = ""+$branchid	+	"/"	+	$in_product_code	+	"/"	+$in_appid	+	"/"
					+ $in_dt_a + "/" + $in_dt_b ;

		var $url = "/corp/product/happyhour_prices/"+$all;	   
		var $result;var $is_check_only = 1;
		
		//Search Now.	    
		$.ajax({
			dataType: 			"json",
	 		url: 					$url,
	 		type: 				'GET',
	 		data:					$result,	
	 		contentType: "application/json",
	 		success: function(result)
	 		{
 				var all = mbf_phh_prices(result,$is_check_only);

				$($in_div).empty();				
	 		
	 			$($in_div).append("<p> Price Exist for this period</p> ");
	 			$($in_div).addClass('red');

				$( "<div/>", 
  				{
    					"class": "row",
    					html: all.join( "" )
  				}).appendTo( $in_div );			
	 			
    		}, 
    		error: function (xhr, status, error)
	 		{
   	      $($in_div).val('Go Ahead');
   	      Materialize.toast("Go ahead",5000);
    			$($in_div).css("background-color","#00f");    			   	      
    		},   		
	 	});
	
}

/*
* FX: mbf_add_pprice: add Product Price
*
*/
function mbf_edit_phh_price($form_id)
{
	//Materialize.toast("Edit Price"+$form_id,1000);
	var $h_pp 			= mbf_pprice_collect_input($form_id);
	//Materialize.toast("Edit Price"+$form_id,1000);

	var $is_edit = 1;
	mbf_add_edit_phh_price($h_pp,$is_edit);

	//Materialize.toast("Editing......",1000);
	
}

/*
* FX: mbf_add_pprice: add Product Price
*
*/

function mbf_add_phh_price()
{
	var $h_pp 			= mbf_pprice_collect_input();
			
	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	$appid				= $h_pp.appid;
	
	$price 				= $h_pp.price;
	
	var $exist_from_date			= $h_pp.exist_from_date || $("#i_from_date").val();
	var $exist_till_date			= $h_pp.exist_till_date || $("#i_till_date").val();
	
	
	$from_time			= $h_pp.from_time;
	$till_time			= $h_pp.till_time;
	$day_of_week		= $h_pp.day_of_week;
	
	$xyz			  = $branchid 
				+ "/" + $in_product_code + "/" + $appid
				+"/"	+ $exist_from_date +"/"+ $exist_till_date
				+"/"	+ $day_of_week				
				+"/"	+ $from_time +"/"+ $till_time 

	
	
	if(   $price && $branchid && $in_product_code 
			&& $exist_from_date && $exist_till_date 
			&& $from_time && $till_time && $day_of_week)
	{
			mbf_add_edit_phh_price($h_pp);
			//Materialize.toast($exist_from_date,4000);		
	}//IF	
	else
	{
			var $msg = "Time Period, Day of week or Price is missing";
			
			if(!$price)
			{
				$msg = "Price is missing";
			}
			else if(!$from_time)
			{
				$msg = "Start Time is missing";				
			}	
			else if(!$till_time)
			{
				$msg = "End Time is missing";				
			}	
			else if(!$day_of_week)
			{
				$msg = "Day of Week is missing";				
			}	
			
			Materialize.toast($msg,4000);			
	}
	
}

/*
* FX: mbf_add_edit_pprice: This runs the Ajax POST/PUT. To Add / Edit Product Price.
*
*/

function mbf_add_edit_phh_price($h_pp,$is_edit)
{

	var $exist_from_date			= $h_pp.exist_from_date || $("#i_from_date").val();
	var $exist_till_date			= $h_pp.exist_till_date || $("#i_till_date").val();

	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	$appid				= $h_pp.appid;

	$exist_from_time			= $h_pp.exist_from_time;
	$exist_till_time			= $h_pp.exist_till_time;
	$exist_day_of_week		= $h_pp.exist_day_of_week;	
	
	$from_time			= $h_pp.from_time;
	$till_time			= $h_pp.till_time;
	$day_of_week		= $h_pp.day_of_week;		
	
	
	var $err_add;var $err_edit;var $error;
	var $msg;var $edit_msg; 
	$err_add =   "Price not added.";
	$err_edit =  "Price not edited.";
	$error = $err_add;
	$msg   = "Price Saved";
	if($is_edit)
	{
		$msg = "Price Saved";
		$error = $is_edit;
	}


	var $base_url = "/corp/product/price_happyhour/";
	var $url = "aaa" ;	
	var $method_type = 'POST';
	
	if($exist_from_date && $exist_till_date 
		&& $exist_day_of_week && $exist_from_time && $exist_till_time)
	{
		$url			  = $base_url	+ $branchid 
				+ "/" + $in_product_code + "/" + $appid
				+"/"	+ $exist_from_date +"/"+ $exist_till_date
				+"/"	+ $exist_day_of_week				
				+"/"	+ $exist_from_time +"/"+ $exist_till_time 
				;
		$method_type = 'PUT';		
		$msg = "Price Edited";
	}
	else if($exist_from_date && $exist_till_date)
	{
		$url			  = $base_url	+ $branchid +"/"+ $in_product_code +"/"+ $appid
							+"/"	+ $exist_from_date +"/"+ $exist_till_date; 		
		$method_type  = 'POST';		
		$msg = "New Price Created";
		
	}
	
	//Materialize.toast($url,7000);
		
	$.ajax( 
							{ 
								url: 			$url ,
								type: 		$method_type,
								data: 		$h_pp,     					
								dataType: 	"json",
								statusCode: 
								{
		    								404: function() {
											$('#txr').append("You have been logged out.");    									
		    							},
		    					},		
				 				error: function (xhr, status, error) 
 								{
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);			 								
		            		},
								success: function( data )
								{

									Materialize.toast($msg,2000);		 

									mbf_pprice_form("#edit_add_pp","PRICETIMEDAY");//RefreshForm for Add
									mbf_price_addform_close();
									
									mbf_list_phh_prices("#list_hprices");

									if(!$is_edit)
									{
										mbf_pprice_form("#edit_add_pp","PRICETIMEDAY");
									}
									
								},
						}	
	);
	//Ajax
	
	
}
								
/*
* FX: mbf_list_pprices: List Product Prices from Ajax
* @params: input Div
* 
*/
function mbf_list_phh_prices($in_div,$in_url)
{

	var $page			= 1;		
	var $h_pp 			= mbf_pprice_collect_input();
			
	$branchid 			= $h_pp.branchid;	
	$in_product_code 	= $h_pp.product_code;
	
	//var $wait_msg = "Waiting for existing prices...." + $progress_clock;
	//Materialize.toast($in_div + $wait_msg,5000);
	progress_append($in_div,$wait_msg);

		var $h_pp 			= mbf_pprice_collect_input();
			
		$branchid 			= $h_pp.branchid;
		$in_product_code 	= $h_pp.product_code;
		$in_appid 			= $h_pp.appid;
		$from_date			= $h_pp.from_date || $("#i_from_date").val();
		$till_date			= $h_pp.till_date || $("#i_till_date").val();;
	

	 if ( $branchid && $in_product_code && $from_date && $till_date && $in_appid)
	 {

			var $url;
			if($in_url)
			{
					$url = $in_url;
				//Materialize.toast("Found URL "+ $url,5000);
			}
			else
			{
		 		var $all = ""+$branchid	+	"/"	+	$in_product_code	+	"/"	+$in_appid 
		 				+ "/"+  $from_date + "/"+  $till_date ;
				$url = "/corp/product/happyhour_prices/"+$all;
				//Materialize.toast("HH Prices "+ $url,5000);
			}

			var $wait_msg = "Prices are being fetched....";
			$($in_div).empty();
			progress_append($in_div,$wait_msg);
		
			//Search Now.	    
			$.ajax({
					dataType: 			"json",
			 		url: 					$url,
			 		type: 				'GET',
			 		contentType: "application/json",
	 				error: function (xhr, status, error) 
					{
 									bx_toast_empty();
	 								var $error_msg = bx_extract_err_msg(xhr);
									Materialize.toast($error_msg, 5000);	
						 	  		var $msg = "No active prices exist for this product.";
 	  								$($in_div).append($msg);
 	  								Materialize.toast($msg,5000);
		    		},   		
			 		success: function(result)
			 		{
		   	      	//Materialize.toast(" Rsult Found "+ $in_url,5000);
							bx_progress_empty($in_div);

							var $self_method		= 'mbf_list_phh_prices';
							var $h_pgx = {	next:			result.next,
												add_div:		$in_div,
												previous:	result.previous,
												page: 		result.page,
											 };
							
                		var $str_pgx =  bgx_pagination($h_pgx,$self_method);
			 				$($in_div).append($str_pgx);

			 				var $all_rows = mbf_phh_prices(result.items);
			 				
			 				 $( "<ul/>", 
							  	{
    									"class": "collapsible accordion ",
    									html: $all_rows.join( "" )
  								}).appendTo( $in_div );

							g_collapse_init();
							g_timepicker_init();
					    		 
					   }, 		
	 			});  
	 	//Ajax End


 	  }//If 
 	  {
 	  		$($in_div).empty();
 	  }
	
}

/* A Product Price */
var $mbv_mst_phh_price =
	   			"<div class='collapsible-header waves-effect waves-green {{row_color}} '>"
 					+"<div class='row' id='phh_{{from_time}}_{{till_time}}' >"
							+ "<div class='col l1 m1 s1 center tag'> &#10095;</div>"
							+ "<div class='col l2 m4 s4' >"
							+ "{{#weekday}}"
								+ "{{weekday}}"							
							+ "{{/weekday}}"
							+ "{{^weekday}}"
								+ "{{day_of_week}}"
							+ "{{/weekday}}"
							+ "</div>"
							 
								
							+ "<div class='col l5 m5 s7' >{{from_time}}" 
										+ " <=> "
										+ "{{till_time}}"
							+"</div> "
								
							+"<div class='col l2 m6 s6 switch radius center'> " 
									+ "<h5 class='{{#str_active}}  icon_check_ballot {{/str_active}} "
									+ " {{^str_active}}  icon_close {{/str_active}}' </h5>"
							+ "</div>"								
							+"<div class='col l2 m6 s6'>{{price}}</div> "
								+ "</div>"
				+ "</div>"				;

/*
* FX: mbf_pprices: Converts Ajax Return Data into Rows.
* Returns: String of All rows.
* @params: data
* 
*/

function mbf_phh_prices(data,$is_check_only)
{
		var $all_rows = [];
		var $t_count = 0;
		var $is_multiple_pricing = $("#is_multiple_pricing").val();
		var $is_happy_pricing;
		if($is_multiple_pricing == 't')
		{
				$is_happy_pricing;
		}
		else
		{
				$is_happy_pricing = 't'; 
		}				
		
		$(data).each(function(index, val) 
		{
				$t_count++;
				var $pptd_active 	= val.active;
				var $from_date 	= val.from_date;
				var $till_date 	= val.till_date;

				var $day_of_week	= val.day_of_week;
				var $from_time 	= val.from_time;
				var $till_time 	= val.till_time;

				var $from_time_int   = g_time_remove_colon($from_time);
				var $till_time_int   = g_time_remove_colon($till_time);
				
				var $formid 		= "edit_"+$day_of_week+"_"+$from_time_int+"_"+$till_time_int;
				var $str_formid 	= "#"+$formid;


				var $str_active;
				if($pptd_active > 0)
				{
					$str_active = 1;
				}

				 $row_color = 'white'
				 if( ($t_count % 2) == 0)
				 {
					 		$row_color = 'amber lighten-4 black-text';
				 }			    
				

				val.str_active				= $str_active;
				val.active_select			= 
					g_drop_true_false("price_status_in",$pptd_active,"Enable/Disable Price");
				
				val.formid					= $formid;
				val.exist_from_date 		= $from_date;
				val.exist_till_date 		= $till_date;
				val.exist_day_of_week	= $day_of_week;
				
				val.exist_from_time		= $from_time;
				val.exist_till_time		= $till_time;
				
				val.str_formid 			= $str_formid;
				val.row_color				= $row_color;
				val.happy_pricing			= $is_happy_pricing;

				var $a_pp 					= Mustache.to_html($mbv_mst_phh_price,val);
				if(!$is_check_only)
				{
						var $a_edit 				= Mustache.to_html($mbv_mst_pptd_form,val);
						var $h_x 					= {form_row:$a_pp,form_edit:$a_edit,};
						
						var $a_row = Mustache.to_html($a_pprice_row,$h_x);
						$all_rows.push($a_row);
						//Create Form for Editing
				}
				else
				{
						$all_rows.push($a_pp);
						//Check Existing					
				}

		});
	
		return $all_rows;
}



/*** HappyHour Price Product***/

/****** Purchase Order ****/

/*
* 
* data.orderid,data.billid,data.pending_credit,
* data.transactions,data.settled_txnid
*
*/

var $mst_po_addprod_form = 
				"<div class='card row green-text'>"
						+"<div class='col l3 m6 s6 input-field'>"
								+"<input id='new_pc' type='text' placeholder='Search Name' >"
									+"</input>"
							+"</div>"								
						+"<div class='col l3 m6 s6 input-field'>"		
								+"<input id='new_pcode' type='text' placeholder='barcode' >"
								+"</input>"
								+"<label for='new_pcode'>Barcode</label>"
							+"</div>"
						+"<div class='col l2 m6 s6  input-field'>"
								+"<input id='new_pc_unit' type='number' "
										+" min='0'  value='' ></input>"
										+"<label for='new_pc_unit'>Units</label>"
										+	"</div>"
						+"<div class='col l2 m6 s6  input-field'>"
								+"<input id='new_pc_price' type='number' "
										+" min='0'  value='' ></input>"
										+"<label for='new_pc_price'>Price</label>"
							+	"</div>"
						+ "<button type='button' class='col l3 m6 s6 btn green' {{{add_btn}}} "
							+ " >+ Add Item"
						+ "</button>"
				+ "</div>";

/*
* FX: mb_display_newpo_btn: Creates a button for New Purchase Order in div:#txr 
* @Params: in_div
*
*
*/

function mb_display_newpo_btn($in_div)
{
	var $div_tm = $in_div || "#txr";
   $($div_tm).empty();
   //Materialize.toast("Adding Button"+$div_tm,2000);
	
	var $fn_click  = "onClick=mb_create_new_po('"+ $div_tm +"');"	   
	var $nbtn 		= "<a class='btn green' id='new_po_btn' "
								+ $fn_click 
								+ " ><b>New Purchase Order</b></a>";

	$($div_tm).append($nbtn);
	
}


/*
* FX: mb_create_new_po: Creates Purchase Order and its Bill and calls mov_pager(billid)
* 
* @params: gets sx_type.
* 
*/
function mb_create_new_po($in_div)
{
	var $orderid ;
	var $branchid = bgx_get_branchid(); 
	var $appid    = bgx_get_appid() || $c_appid_retail;
	var $h_npo = {
							'appid': $appid,
					 };
					 
	$($in_div).append($progress_clock);				 
	
	var $sx_type  = $("#sx_type").val();
	var $in_m = "/purchase/order/";
	   
	$.ajax( 
				{ 
				url: 			$in_m ,
				type: 		"POST",
				dataType: 	"json",
				data:			$h_npo,
				timeout: 	5000,
 				error: function (xhr, status, error) 
				{
						bx_toast_empty();
						var $error_msg = bx_extract_err_msg(xhr);
						Materialize.toast($error_msg, 5000);			 								
 						Materialize.toast("Error in Creating PO.");
            },
				success: function( $po_data )
				{
						var $orderid 	= $po_data.orderid;
						bx_toast_empty();
						Materialize.toast("Created PO"+$orderid);
						if($orderid )
						{
								mb_apo_txns($orderid,$in_div,$po_data);							
						}
				}	,//success
				
	}); 
			   						    
}//Fn end

/*
* FX: mb_apo_txns: Handles Purchase Order: IF data is available then display else fetch
* Txns data for the PO.
* @params: bizorderid,$in_div,$po_data
*
*/
function mb_apo_txns($bizorderid,$in_div,$po_data)
{

  var $h_po;var $div_txns;var $currency;
  var $billid;var $orderid;
  if(!$in_div)
  {
  		$in_div = '#txr';//Retail Div
  }
  
  var $t_str = "OID:"+ $bizorderid +"Div:" + $in_div + ",Data:"+ $po_data;
  Materialize.toast($t_str);
	
  if($bizorderid && $in_div && $po_data)
  {
  		Materialize.toast(" 1. APO Txns: ",3000);
  		mb_display_po($bizorderid,$in_div,$po_data);
  }
  else if($bizorderid && $in_div)
  {
  		Materialize.toast(" 2. APO Txns: ",3000);
  		mb_get_po($bizorderid,$in_div);
  }
  else
  {
  	  Materialize.toast(" 3. APO Txns: ",3000);
  }
  
	
}

/*
* GET PO details
* FX: mb_get_po
*/

function mb_get_po($bizorderid,$in_div)
{

	var $branchid = bgx_get_branchid(); 

	
	var $sx_type	= $("#sx_type").val();
	var $in_m		= "/purchase/order/"+$bizorderid;
	var $h_npo;
	   
	$.ajax( 
				{ 
				url: 			$in_m ,
				type: 		"GET",
				dataType: 	"json",
				data:			$h_npo,
				timeout: 	5000,
 				error: function (xhr, status, error) 
				{
						bx_toast_empty();
						var $error_msg = bx_extract_err_msg(xhr);
						Materialize.toast($error_msg, 5000);			 								
            },
				success: function( $po_data )
				{
						var $orderid 	= $po_data.orderid;
						
						if($orderid )
						{
								mb_display_po($orderid,$in_div,$po_data);							
						}
				}	,//success
				
	}); 

	
}

/*
* See: bael.js/mst_credit_note_summary
*/

var $mst_mb_po_summary = 
										"<div class='row collection z-depth-1'>"  
									+	"<div class='row collection-item "+ $btx4_color +"'>"
				            		  		+ "<div class='col s8 left-align '>"
				            		   		+ "Subtotal"
				            		  			+ " </div>"
				            		  		+ "<div class='col s4 right-align'>"
				            		   		+ "{{sub_total}}"
				            		  			+ " </div>"
            		  	        	+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Bill Total " 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{total_debit}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item "+ $btx4_color +"'>"
            		  							+ "<div class='col s8 left-align'>"
            		  								+ "Credit" 
            		  								+ "</div>"
            		  							+ "<div class='col s4 right-align'>"
            		  								+ "{{total_credit}}"
            		  								+ "</div>"
            		  				+ "</div>"
            		  			+
            		  	 			"<div class='row collection-item  "+ $btx4_color +"'>"
             		  							+ "<div class='col s8 left-align'>"
            		  									+ "Amount Payable  "
            		  									+ "</div>"
             		  							+ "<div class='col s4 right-align red-text'> "
            		  									+ "{{total}}"
            		  							+ "</div>"

										+ "</div>"            		  	
            		  			+ "</div>";

var $mst_mb_po_header =  "<div class='collection z-depth-1'>"  
								+ "{{#orderid}}"
									+"<div class='collection-item row'>"
		  		 					+ 		"<span class='black-text col l6 m6 s6 left'>"
		  		 					+ 			"OID: {{orderid}}"  
		  		 					+ 		"</span>"
		  		 					+ 		"<span class='black-text col l6 m6 s6'>"
		  		 					+ 			"{{{bill_btn}}}"  
		  		 					+ 		"</span>"
									+"</div>"
								+ "{{/orderid}}"	
         		  	     + "</div>" 
            		  	        	;
            		  			

/*
*
* FX: mb_po_summary: Final Hash
* @params: billitems,currency
* similar to b_credit_note_summary, using
*
*/
function mb_po_summary(data,$currency)
{
	var $ft					= data.finalh;		 
	var $amt_payable		= data.pending_credit;
	var $x_currency = $currency || '';	
	var $m_summary;
	var $h_summary;
	
	if($ft.total_tax || $ft.total_chgs || $ft.total_credit)
	{	

			      var $f_total_tax 					= $ft.total_tax_return;		
					var $f_total_prod_serv_chgs 	= $ft.total_prod_serv_chgs_return || 'psc';				
			      var $f_total_chgs 				= $ft.total_chgs_return;
			      var $f_total_credit				= $ft.total_credit;
			      var $f_total_debit			   = $ft.total_debit;
			      var $f_total_discount		   = $ft.total_discount_return ;
			      
			      var $f_total_amt 					= $amt_payable;
			      
			      $h_summary = { 	
			      							'taxes'			:	$f_total_tax,
			      							'sub_total'		:	$f_total_prod_serv_chgs,
			      							'charges'		:	$f_total_chgs,
			      							'total_credit' : 	$f_total_credit,				      							
			      							'total_debit'  : 	$f_total_debit,	
			      							'total_discount' : $f_total_discount,

			      							'total'			:	$f_total_amt,
			      					  };
					var $m_summary = Mustache.to_html($mst_mb_po_summary, $h_summary);
 	  		}
	
	return $m_summary;
	
}


/*
* FX: mb_display_po: Display PurchaseOrder
* 
*/
function mb_display_po($bizorderid,$in_div,$po_data)
{
		var $billid 	= $po_data.billid;
		var $orderid 	= $po_data.orderid;
		var $settled_txnid = $po_data.settled_txnid;

		$($in_div).empty();		
		bx_toast_empty();
		var $div_new_form;
		var $items = [];
		//Materialize.toast(" Display PO: "+$bizorderid,);

		//Fill in the Values						
		$("#billid").val($billid);					   
		$("#bizorderid").val($orderid);

		var $bill_btn;
		if($orderid)
		{
				var $fn_click = "onClick=mb_po_abill('"+$billid +"');";
				$bill_btn = "<a class='btn waves-effect waves-teal "
											+ " red white-text center' id='abill' "+ $fn_click 
										+ " > Bill</a>";
		}		
		
		var $h_px = {
								orderid:  $orderid,
								bill_btn: $bill_btn,	
						} ;
		var $div_header = Mustache.to_html($mst_mb_po_header, $h_px);						
		
		//Form if PO is not settled.
	   if($bizorderid && !$settled_txnid)
	   {
				var $fn_add_pc = "onClick=mb_po_add_pc('"+$orderid+"','" + $in_div + "');"
				var $h_new_form = {
											add_btn: $fn_add_pc,
										};
				$div_new_form = Mustache.to_html($mst_po_addprod_form, $h_new_form);
	
		}


		//Txns and Currency
		$h_po		 = bx_bill_txns($po_data.transactions);
		$div_txns = $h_po.txns;
		$currency = $h_po.currency;

		//Summary PO			
		var $div_summary = mb_po_summary($po_data,$currency);

		$($div_entity_more).empty();//Flush This Part.
				
		$items.push($div_new_form);	//1
		$items.push($div_header);		//2							
		$items.push($div_txns);			//3
		$items.push($div_summary);		//4
				
  		$( "<div/>",  { "class": "collapsible-header",html: $items.join("") }).
  			appendTo( $in_div );
		eac_product_search_init();
		
	
}

/*
* FX: mb_po_add_pc: Add Product to PurchaseOrder
*
*/
function mb_po_add_pc($orderid,$in_div)
{
	var $branchid			= bgx_get_branchid(); 
	var $billid				= $("#billid").val();					   
	var $bizorderid		= $("#bizorderid").val();

	var $prname				= $("#new_pc").val();
	var $prcode				= $("#new_pcode").val();
		
	var $new_pc_unit 		= $("#new_pc_unit").val() || 1;
	var $new_pc_price 	= $("#new_pc_price").val();
	var $new_pc_amount 	= $new_pc_unit * $new_pc_price;	

	var $new_pc;
	if($prcode)
	{
		$new_pc = $prcode;
	}
	else
	{
		$new_pc = $prname;	
	}	
	
			
	var $url = "/purchase/order";
	var $in_m = $url + "/" +$bizorderid ;
	var $m_type = 'PUT';
	
	var $h_dx;
	$h_dx = {
							product_code: $new_pc,
							amount:		$new_pc_amount,
							price:		$new_pc_price,
							units:		$new_pc_unit,	
	 };
	 
	 if($new_pc && $new_pc_price && $new_pc_unit)
	 {
	 		Materialize.toast("Ready to Add in PO.",5000);
	 }
	 else
	 {
	 		bx_toast_empty();
	 		var $err_msg = "Code:"+$new_pc + ", Price:"+$new_pc_price+", Units: "+$new_pc_unit;
	 		Materialize.toast("Missing Product/Service, Price, Units"+$err_msg,5000);
	 		return;
	 }
	
	$.ajax( 
				{ 
				url: 			$in_m ,
				type: 		$m_type,
				dataType: 	'json',
				data:			$h_dx,
				timeout: 	5000,
 				error: function (xhr, status, error) 
				{
						bx_toast_empty();
						var $error_msg = bx_extract_err_msg(xhr);
						Materialize.toast($error_msg, 5000);			 								
            },
				success: function( $po_data )
				{
						var $orderid 	= $po_data.orderid;
						
						if($orderid )
						{
								mb_display_po($bizorderid,$in_div,$po_data);							
						}
				}	,//success
				
	}); 
	
		
}
/****** Purchase Order END ****/

/*
* FX: mb_po_display_allbills: Display All bills 
* @params: inpage,indate,
*
*/

function mb_po_display_allbills(incount,inpage,indate)
{

  var $fnx;
  var $appid =  bgx_get_appid();
  var $branchid = bgx_get_branchid();
  $url = "/purchase/bill/list/"+inpage;
  $fnx = $url ;
  
	//Search Input First
	var $x_date = g_choosen_date();
	var $in_date;
	if($x_date)
	{
			$in_date = $x_date;
	}
	
	
	var $search_orderid    = $('#fnx_bill_orderid').val()    || 	$('#i_orderid').val() ;
	var $search_billnumber = $('#fnx_bill_billnumber').val() || 	$('#i_billid').val();;
	//Materialize.toast($in_date,2000);	

	var $div_tm = "#txr";
   $($div_tm).empty();

	//Search Options Div  
	$dv_search = g_bills_search($appid);
	$($div_tm).append($dv_search);
   

	var $v_method  = "mb_po_display_allbills";   	
   var $pgx_ul    = bgx_display_pgx($v_method);
	$($div_tm).append($pgx_ul);	

	$($div_tm).append($rtl_bill_pgx);	
	$($div_tm).append($rtl_bills_div);

	var $in_page  = inpage || 1;
	
	//var $in_m = $in_rest + "/20/1";	
	var $rest_pg_txt = "#rest_pagination_text";

	$.ajax( 
				{ 
				url: $fnx ,
				type: "GET",
				dataType: "json",
				headers: { 
								'indate':      $in_date,
							   'orderid':		$search_orderid,
							   'billnumber':	$search_billnumber,							   
							  },
				error: function (xhr, status, error) 
	 		   {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);

	 						var $me = "<span class='red-text'>No Bills for Page:"
 								+ $in_page + "</span>";
 							$('#div_allbills').empty();
 							$('#div_allbills').append($me);
            },
				success: function( data )
				{
					mb_po_display_bills(data);
					g_choosen_date($in_date);
				},
					//GetJSON Function_data

	});
	//ajax

	

}// Function_dda


/*
* FX: mb_po_display_bills: Display PO Bills in div: #div_allbills
* @params: data
*
*/
function mb_po_display_bills(data)
{
   		       var items = [];
   		       
   		       var $idx_first = data.idx_first;
   		       var $idx_last = data.idx_last;
   		       var $idx_str = $idx_first + "-" + $idx_last;
   		       var $rest_pg_txt = "#rest_pagination_text";	  		
   		       $($rest_pg_txt).empty();
   		       $($rest_pg_txt).append($idx_str);
   		       
         		 $.each( data.items, function( key, val ) 
         				{
         			 		var $m_b = mb_div_po_bill(val);
								items.push($m_b);         			 		
                		}
                );
                //ForEach
		
					$('#div_allbills').empty();

  					$( "<div/>",  { "class": "row",html: items.join("") }).
  						appendTo( "#div_allbills" );
  					//

}

   var $mst_list_po_abill   = 
	                    "<div class='card col l4 m6 s12 border'>" +
	                    		" <div class='card-content black-text'> "
					      + "<span class='card-title'>" 
					      + 	 "B-{{billid}}</span>"
					      + "<p>{{billdate}}"+ "{{str_otype}}" +"</p>" 
					      + 	"<p><b>O-{{days_order}}</b>, OID-{{orderid}}</p>"				      
					      + "{{#customerid}}"
									+ "<p>C: {{customerid}}</p>"					      
					      + "{{/customerid}}"
					      + "{{^customerid}}"
									+ "<p>U: {{userid}}</p>"					      
					      + "{{/customerid}}"

					      + "{{#str_status}}"
					      		+			     "{{{str_status}}}"
					      + "{{/str_status}}"
					       					      
	                  + "<div class='card-action'>"
	                   	+ "{{{link_btn}}}"	 
					      + 		" </div>"
					      
			      + "</div></div>";



/*
* FX: mb_div_po_bill: Display A PO Bill
*
* @params: data.val
* @returns: a String(Mustache).
* REST/FTO have different div for bill
* This fn , and its mst can be common across
*/
function mb_div_po_bill(val)
{
     var $m_url  			= val.url;
     var $m_txnid		   = val.transactionid;
	  var $m_cancel	= val.cancelled_at;
	  var $order_type = val.order_type;
	  var $str_status;
	  var $str_payment; 
	  var $str_cancel = '<span></span>';

	  if($m_cancel)
	  {
	  		$str_cancel = " <span class='red-text'><b>(X)</b></span> ";
	  		$str_status = $str_cancel;
	  }        
	  else if($m_txnid > 0)
		{
			$str_payment = "<span class='green-text bold'>PAYMENT</span>";
			$str_status = $str_payment;
		}
	 	else
		{
			$str_status = "<span class='red-text '><b>PENDING</b></span>";;
		}

	   var $str_order ="<p><b>O-{{days_order}}</b>, OID-{{orderid}}</p>";
      var $m_userid			= val.userid ;
      var $m_customerid   = val.customerid;
      var $str_user = "<p>U: {{userid}}</p>";
	                         
		if(!$m_userid && $m_customerid)
		{
		  	$str_user = "<p>C: {{customerid}}</p>";
		}


		var $v_billid = val.billid;
		var $orderid = val.orderid;
		var $str_otype;
		var $fn_click = "onClick=mb_po_abill('"+$v_billid +"');";
		if($order_type == $c_order_po)
		{
			$str_otype = ", Purchase";
		}		
		else if($order_type == $c_order_dn)
		{
			$str_otype = ", Debit Note";
		}
			   
		var $info_btn = "<a class='btn-floating waves-effect waves-teal "
					+ " red white-text center' id='abill' "+ $fn_click 
					+ " > " + $rtl_arrow_right + "</a>";

		var $h_abill = {
									'link_btn'	: $info_btn,
									'str_otype'	: $str_otype,
									
									'billid'		: val.billid,
									'billdate'	: val.billdate,
									'days_order': val.days_order,
									'orderid'	: val.orderid,
									'customerid': val.customerid,
									'userid'		: val.userid,

									'str_status': $str_status,									
									'transactionid': $m_txnid,	
							};
	                         
	
		var $mx_mst = Mustache.to_html($mst_list_po_abill, $h_abill);
		return $mx_mst;
		
}

/*
* FX: mb_po_abill
* @params: orderid
*
*/

function mb_po_abill($billid)
{
		var $dv_bill = '#list_of_bill_items';
		$("#billid").val($billid);
		
		var $div_txr = "#txr";
		$($div_txr).empty();		
		
		var $ul_bill_top	= bx_bill_top();
		$($div_entity_more).empty();//Flush This Part.
		$($div_entity_more).append($ul_bill_top);
		
		var $div_bill = "<div id='diner_index'></div>"
				+"<div id='idprint' class=''><div id='list_of_bill_items'></div></div>";
		$($div_txr).append($div_bill);
		
		var $dv_bxd 		= bx_bill_details();
		$($div_bill).append($dv_bxd);

		//Materialize.toast($billid,3000);
		mb_po_bill_txns($billid);
			
}

/*
* FX: mb_po_bill_txns: Display Bill txns
* Changes DOM
* 
* @params: orderid
*
*/
function	mb_po_bill_txns($billid) 
{
		var $branchid = bgx_get_branchid();

		//Default API	
		var $api_po_items = "/purchase/bill/" + $billid;

		$.ajax( 
					{ 
						url: 			$api_po_items ,
						type: 		"GET",
						dataType: 	"json",		  
						error: function (xhr, status, error) 
			 		   {
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
 							var $me = "<span class='red-text'>Bill not found:"
 								+ $billid + "</span>";
            		},
						success: function( data )
						{
							//Materialize.toast("mb_po_bill_txns",2000);
							bx_diner_bill_info(data);
						},
				}	
		);
	
}

