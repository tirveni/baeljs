/*
	 2015-12-25
	 Guest.js, JS Utilities for BAEL's 

    This file is part of BAEL
    Copyright (C) 2018,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/


/*
* Fx_guest_menu_flip: groups OR pages
*
*/

function fgx_guest_menu_flip()
{
   var $display_type;

   if ( $('#pgx_display_groups').prop('checked') )
   {
   		$display_type = 'groups';
   		//Materialize.toast($display_type,2000);
   }
   else
   {
   		$display_type = 'pages';
      	//Materialize.toast($display_type,2000);
   }
	$div_x = "#pgx_display_type";
	$($div_x).html("");		
	$($div_x).val($display_type);

	$($div_head).empty();
	$($div_pg).empty();
		
	var $v_method  = "dl_display_menu_order_cache";   
   var $pgx_ul    = bgx_display_pgx($v_method);
	$($div_head).append($pgx_ul);
		   
   var $d_menu    = dl_div_menu();
	$($div_pg).append($d_menu);
	
	dl_display_menu_order_cache(); //Load MenuItems
	
}

/*
* FX: fgx_create_order();
* For Pickup(FTO)
*/
function fgx_create_order()
{
	//Create  an Order
	var $branchid 	= bgx_get_branchid();
	var $url			= '/g/pickup/order/creation/' + $branchid;

	var $x_userid = bgx_get_userid();
	

   
	if($branchid && $x_userid)
	{
		Materialize.toast("OK"+$x_userid,5000);	

		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"POST",
						dataType: 	"json",
 						error: function (xhr,status,error) 
 						{
							bx_toast_empty();
							var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000);
            		},
						success: function( data )
						{
							var $bizorderid = data.orderid;
							window.location.href = '/g/cart/'+$bizorderid;
						},
				}	
			);
		
		}
		else if(!$x_userid)
		{
				bx_toast_empty();
				var $err_msg = "Please login, before creating an order."	;
				Materialize.toast($err_msg,2000);		
		}
		else if(!$branchid)
		{
				bx_toast_empty();
				var $err_msg = "Branch is not available."	;
				Materialize.toast($err_msg,2000);		
		}
   
}


/*
Templates for Buttons of Place Order,Make Bill in diner/order Page.
*/
var $div_btn_gfinalize = "<div class='row di_bizorder_btn'>"
											+ "<div class='col l4 m4 s12 btn-large   green waves-effect waves-light ' "
												+ "  href='' onClick='fgx_order_additems();' >"
												+ " 1. Add Items >> </div>"
											+ "<div class='col l4 m4 s12 btn-large  disabled waves-effect waves-light ' "
												+ "  >"
												+ " 2. Fill Address >> </div>"
											+ "<div class='col l4 m4 s12 btn-large  disabled waves-effect waves-light ' "
												+ "  >"
												+ " 3. Confirm </div>"
								+ "</div>";

var $div_btn_gmake_bill = "<div class='row di_bizorder_btn' >"
										+ "<div class='col l4 m4 s12 btn-large disabled  waves-effect waves-light ' "
												+ "  >"
												+ " 1. Add Items >> </div>"
										+ "<div class='col l4 m4 s12 btn-large green waves-effect waves-light ' "
											+ " onClick='fgx_order_phone_form();' href='#spa_header' >"
											+ " 2. Fill Address >> </div>"
										+ "<div class='col l4 m4 s12 btn-large  disabled waves-effect waves-light ' "
												+ "  >"
												+ " 3. Confirm </div>"
								+ "</div>";

var $div_btn_gbill_confirm = "<div class='row di_bizorder_btn' >"
										+ "<div class='col l4 m4 s10 btn-large disabled  waves-effect waves-light ' "
												+ "  >"
												+ " 1. Add Items >> </div>"
										+ "<div class='col l4 m4 s11 btn-large  disabled waves-effect waves-light ' "
												+ "  >"
												+ " 2. Address >> </div>"
										+ "<div class='col l4 m4 s12 btn-large  green waves-effect waves-light ' "
												+ " onClick='fgx_order_make_bill();' >"
												+ " 3. Confirm </div>"
								+ "</div>";


/*
* FX: fgx_dsp_order_section: display Order Section
*
*/
function fgx_dsp_order_section()
{
	var $div_bizo = "<div class='section' >"

					+ "<div class='order_count right' >"
					+"</div>"

					+ "<div class='card'>"
			
						+ "	<div class='card-content'>"
				  			+ " <div class='card-title activator grey-text text-darken-4 " 
											+"	 icon_menu_more display_bo'>  "
							+ "</div>"
							+ "</div>"
					
						+ "<div class='card-reveal'>"
						  			+ " <div class='card-title grey-text text-darken-4 " 
											+"	 icon_close display_bo'>  "
									+ "</div>"
  							+ "<span id='bizorder_extra'>Order details are not available</span>"
 							+ "</div>"

						+ "<div id='bizorder' class='card-content'>"
							+ $progress_clock
							+ "</div>"


						+ "<div id='bizorder_action' class='card-action' >"
							+ $div_btn_make_bill
						+ "</div>"	
						
					+ "</div>"
		+ "<div>";

	return $div_bizo;	
	
}


/*
* FX: dgx_display_anorder: Fetch An Order(ajax)  
*
* @params: bizorderid
*
*/
function dgx_fetch_anorder($x_bizorderid)
{
	var $url;
	var $appid = $('#in_appid').val();

	var $url = '/g/order/';
	var $in_m = $url + $x_bizorderid;

	if($x_bizorderid)
	{
		$.ajax( 
				{ 
				url: 					$in_m ,
				type: 				"GET",
				dataType: 			"json",
 				error: function (xhr,status,error) 
 				{
						bx_toast_empty();
						var $error_msg = bx_extract_err_msg(xhr);
						Materialize.toast($error_msg, 5000);
						$("#rest_messages").append($error_msg);
            },
				success: function( datafc )
				{
 					fgx_dsp_bizorder(datafc);

					var $bizorderid = datafc.orderid;
					var $str_dor = " OID-" + $bizorderid; 
					$(".display_bo").empty();
					$(".display_bo").append($str_dor);
				},
		//Function Success

	});
	//ajax
	
	}
	else 
	{
			Materialize.toast("OrderID is invalid",5000);
	}

}

var $mst_bizord_extra = "<div class='row'>"

								+ "{{#city_name}}"									
								+ "<div class='col l4 m4 s4'>"
									+ "City"
									+ "</div>"
								+ "<div class='col l8 m8 s8'>"
									+ "{{city_name}}"
									+ "<input id='branch_city' 	name='branch_city' value='{{branch_city}}' type='hidden' />"
									+ "<input id='branch_country' name='branch_country' value='{{branch_country}}' type='hidden' />"
									+ "<input id='branch_state' 	name='branch_state' value='{{branch_state}}' type='hidden' />"
									+ "</div>"
								+ "{{/city_name}}"

								+ "{{#orderid}}"
								+ "<div class='col l4 m4 s4'>"
									+ "OID"
									+ "</div>"
								+ "<div class='col l8 m8 s8'>"
									+ "{{orderid}}"
									+ "</div>"
								+ "{{/orderid}}"									

								+ "{{#bizorderdate}}"
								+ "<div class='col l4 m4 s4'>"
									+ "Time"
									+ "</div>"
								+ "<div class='col l8 m8 s8'>"
									+ "{{bizorderdate}}"
									+ "</div>"
								+ "{{/bizorderdate}}"

								+ "{{#customerid}}"
								+ "<div class='col l4 m4 s4'>"
									+ "Customer"
									+ "</div>"
								+ "<div class='col l8 m8 s8'>"
									+ "{{customerid}}"
									+ "</div>"
								+ "{{/customerid}}"
								
								+ "{{#customer_address}}"	
								+ "<div class='col l4 m4 s4'>"
									+ "Address"
									+ "</div>"
								+ "<div class='col l8 m8 s8' id='customer_address'>"
									+ "{{customer_address}}"
									+ "</div>"
								+ "{{/customer_address}}"

								+ "{{#customer_phone}}"									
								+ "<div class='col l4 m4 s4' >"
									+ "Phone"
									+ "</div>"
								+ "<div class='col l8 m8 s8' id='customer_phone'>"
									+ "{{customer_phone}}"
									+ "</div>"
								+ "{{/customer_phone}}"								
							+ "</div>";
							
var $mst_form_phone = "<form id='bizo_address_phone' class='card-panel'>"

								+ "<div class='row border '>"
								+ "<div class='col l12 m12 s12 center-align green-text '>"
										+"<b>Phone </b></div>"
								
								+ "{{#phone}}"
								+ "<div class='col l4 m4 s12'>"
										+ "Update Phone"
										+ "</div>"
									+ "<div class='col l8 m8 s12'>"	
										+	"<select name='phone_update' class='browser-default' >&nbsp;"
											+ $mst_boolean_dropbox
										+ 	"</select>"
									+ "</div>"
									+ "<div class='col l12 m12 s12'>"
										+ "&nbsp;"
										+ "</div>"
								+ "{{/phone}}"
								
								+ "<div class='col l4 m4 s12'>"
									+ "Phone"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='phone' required value='{{phone}}' />"
									+ "</div>"
									
								+ "</div>"
								+ "<div class='row border '>"

								+ "<div class='col l12 m12 s12 center-align green-text '>"
										+"<b>Address </b></div>"

								+ "{{#address}}"
								+ "<div class='col l12 m12 s12 center-align green-text '>"
										+"{{address}}</div>"
										
									+ "<div class='col l4 m4 s12'>"
										+ "Update Address"
										+ "</div>"
									+ "<div class='col l8 m8 s12'>"	
										+	"<select name='address_update' class='browser-default' >&nbsp;"
											+ $mst_boolean_dropbox
										+ 	"</select>"
									+ "</div>"
									+ "<div class='col l12 m12 s12'>"
										+ "&nbsp;"
										+ "</div>"
								+ "{{/address}}"

								+ "<div class='col l4 m4 s12'>"
									+ "Line 1"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='streetaddress1' required value='{{streetaddress1}}' />"
									+ "</div>"

								+ "<div class='col l4 m4 s12'>"
									+ "Line 2"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='streetaddress2' required value='{{streetaddress2}}' />"
									+ "</div>"

								+ "<div class='col l4 m4 s12'>"
									+ "Line 3"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='streetaddress3' required value='{{streetaddress3}}' />"
									+ "</div>"
									
								+ "<div class='col l4 m4 s12'>"
									+ "Directions"
								+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='directions' required value='{{directions}}' />"
								+ "</div>"

									+ "<div class='col l12 m12 s12 center-align '>"
											+ "<btn class='green btn' onClick='fgx_order_add_phone();' >"
												+"Update Phone & Address</btn>"

											+ "&nbsp;"
												
											+ "<btn class='red btn' onClick='bx_more_link_del(bizo_address_phone);' >"
												+"Close</btn>"
										+ "</div>"
										
								+ "</div>"
								+ "<div id='default_address'></div>"								
								+ "</form>";


var $mst_g_default_address = "<form id='default_address_phone' class='card-panel'>"

								+ "<div class='row border '>"
			
								+ "<div class='col l12 m12 s12 center-align green-text '>"
										+"<b>Default Phone </b></div>"
								+ "<div class='col l4 m4 s12'>"
									+ "Phone"
								+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='phone' required value='{{phone}}' />"
								+ "</div>"
									
								+ "</div>"
								+ "<div class='row border '>"

								+ "<div class='col l12 m12 s12 center-align green-text '>"
										+"<b>Default Address </b>"
										+ "<input name='addressid' value='{{addressid}}' type='hidden' /> "
								+"</div>"

								+ "<div class='col l4 m4 s12'>"
									+ "Line 1"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='streetaddress1' required value='{{streetaddress1}}' />"
									+ "</div>"

								+ "<div class='col l4 m4 s12'>"
									+ "Line 2"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='streetaddress2' required value='{{streetaddress2}}' />"
									+ "</div>"

								+ "<div class='col l4 m4 s12'>"
									+ "Line 3"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='streetaddress3' required  value='{{streetaddress3}}' />"
									+ "</div>"
									
								+ "<div class='col l4 m4 s12'>"
									+ "Directions"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='directions' required value='{{directions}}' />"
									+ "</div>"
									
								+ "<div class='col l4 m4 s12'>"
									+ "Addresss"
									+ "</div>"
								+ "<div class='col l8 m8 s12'>"
									+ "<input name='address' required value='{{addressid}}' />"
									+ "</div>"
									
								+ "</div>"		
															
								+ "</form>";

/*
*
* FX: fgx_dsp_bizorder: Display An Order Object
* Make it better.
* @params: data
*
*/
function fgx_dsp_bizorder(datafc)
{
	 	var $fitems = [];
		var $aitems = [];
  		$('#bizorder').empty(); 
  		$('#bizorder_extra').empty();
  		
		//$( "#bizorder" ).append("Debugging Latency: 1. Empty.")
		

		var $customerid		= datafc.customerid;
		var $order_discount	= datafc.order_discount;
		var $disc_userid	   = datafc.discount_userid;
		var $userid	   		= datafc.userid;		
		var $billid				= datafc.billid || 0;
		var $is_cancelled		= datafc.cancelled;
		var $bizorderid		= datafc.orderid;
		var $bizorderdate		= datafc.bizorderdate;	
		
		var $customer_phone 		= datafc.customer_phone;
		var $customer_address 	= datafc.customer_address;
		var $is_bill_ready;
		
		if($customer_phone && $customer_address)
		{
				$is_bill_ready = 1;
		}
		

		var $bill_msg;		
		if($billid > 0)
		{
			var $bill_msg = "Bill has already been created.";
			Materialize.toast($bill_msg, 2000);
		}
		else if($is_cancelled)
		{
			var $str_msg = "Order has been cancelled.";
			Materialize.toast($str_msg, 2000);
			bx_display_error(0,$str_msg);
		}
		else
		{
			$( "#bizorder" ).append($progress_clock);
		}

		var $h_extra = {};
		var $div_extra = Mustache.to_html($mst_bizord_extra,datafc );
		$( "#bizorder_extra" ).append($div_extra);
		
		var $count_items = 0;

  		var $mst_oitem;
  		if($is_cancelled || $billid)
  		{
  			$mst_oitem = $mst_order_item_ro;
  			$("#bizorder_menu").remove();
  		}
  		else
  		{
  			$mst_oitem = $mst_order_item;
  		}
		Mustache.parse($mst_oitem);

		$aitems = datafc.items;	
	   $.each( $aitems, function( index, val ) 
	   {
			var $xe;
			if(val)
			{
					var $xe = Mustache.to_html($mst_oitem, val);
					$fitems.push($xe);
					$count_items = $count_items + 1;
			}	
  		});

  $('#bizorder').empty();   
  
  $( "<div/>", 
  {
    "class": "",
    html: $fitems.join( "" )
  }).appendTo( "#bizorder" );

  var $bizorder_items = $('.item_pc').length;

  if($count_items > 0)
  {
	   di_is_btn_bill_order('bill');
  }		
  else
  {
  		di_is_btn_bill_order();
		var $span_noitem = "<span id='bizorder_no_item'>No Items in the order.</span>" 
		$("#bizorder").append($span_noitem);
  }

	//Add Go To Bill Button  
	if($billid > 0)
	{
		$( "#x_bill" ).remove();//Contains the Make Bill button
		var $fn_click = "onClick=dx_move_bill('"+$billid +"');";
		var $bill_btn = "<a class='btn large red white-text' id='abill' "
								+ $fn_click +" >" 
								+ "Go to Bill " +  "</a>";
		$( "#bizorder" ).append($bill_btn);												
	}  
	else if($is_cancelled)
	{
		$( "#x_bill" ).remove();//Contains the Make Bill button
		var $x_btn = "<span class='white red-text'  ><b>"
								+ "Order is cancelled: " +  $is_cancelled +"</b></span>";
		$( "#bizorder" ).append($x_btn);												
	}
	else if($is_bill_ready > 0)
	{
		di_is_btn_bill_order('confirm');
	}		
	
			

}




/*
* FX: fgx_order_additems: Add Items to Order through POST
* Send Selected Products to the ORder.
* MethodType: POST Data: can handle multiple products through json.
* 
*/
function fgx_order_additems()
{
   var $progress = $progress_clock;
   $("#bizorder").append($progress);

   $c_bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();

	var $base_url;
	var $appid = $('#in_appid').val();
  	if($appid == $c_appid_foodtakeout)
  	{
  		$base_url = "/g/pickup/order/product/"+$c_bizorderid;  	
  	}	

	var $ran = 0;

	var $fitems = [];
	$fitems = fbx_form_in_items();//GEt the Prod and Units
 	$("#bizorder").append("Order is being updated....");

	var $url = $base_url;

	$.ajax(
	{
         url:  				$url,
         type: 				'POST',
			data:      			{'items':JSON.stringify($fitems)},
    		traditional:		true,
			dataType: 			"json",
         error: function(xhr, status, err)
         {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				$("#rest_messages").append($error_msg);
				$("#bizorder").empty();
				$("#bizorder").append($error_msg);
         },
	 		success: function(xdata)
       	{
       	  var orderid = xdata.orderid;
       	  $("#bizorder").empty();
	  		  $ran = dgx_fetch_anorder($c_bizorderid);
         },
      }
    );
	//Ajax End

	  if($ran < 1)
	  {
				dgx_fetch_anorder($c_bizorderid);
	  } 	  
	  $("#cart").html("");
	  $( '.cart_count' ).html("");
	  return false;

	
}

var $mst_address_card_select =  "<div class='row border'>"
			+ "<div class='col l4 m4 s12'>"
				+  "<input name='addressid' type='radio' id='{{addressid}}' value='{{addressid}}' />"
				+  "<label for='{{addressid}}'>Select</label>"
			+ "</div>"
			+"<div class='col l8 m8 s12'>"
			+	"<p>{{address}}<p>"
			+ "</div>"
							  			+ "</div>";	

/*
* FX: fgx_default_address(): Get Default Address for Guest for City if given
* 
*/
function fgx_default_address($div_in) 
{

	var $url 			= '/g/address';	
	var $div_default_address 	= '#default_address_phone';	
	var $div_defadd 	= '#default_address';
	
	var $x_city 	= $("#branch_city").val();
	var $x_state 	= $("#branch_state").val();
	var $x_country = $("#branch_country").val();
	var $h_cityx = {};

	//Materialize.toast($x_city + ":"+$x_state +":"+$x_country, 4000);
	
	if($x_country && $x_state && $x_state)
	{
		$h_cityx = {country:$x_country,state:$x_state,city:$x_city};
	}	
	
	$.ajax(
	{
         url:  				$url,
         type: 				'GET',
			dataType: 			"json",
			headers:				$h_cityx,
         error: function(xhr, status, err)
         {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				$("#rest_messages").append($error_msg);
         },
	 		success: function(xdata)
       	{
       		var $div_ap = Mustache.to_html($mst_g_default_address, xdata.details);
       		$($div_in).append($div_ap);
       		
       		var $addressid 	= xdata.details.addressid;
				if($addressid)
				{	
       			var $h_defadd = {addressid:$addressid,address:xdata.address};
					var $card_defadd = Mustache.to_html($mst_address_card_select, $h_defadd);
					$($div_defadd).append($card_defadd);
				}	
				
       		gx_flip_hide_show($div_default_address);//Hide It
         },
      }
    );

}


/*
* FX: fgx_order_phone_form(in_div): Display Phone and Address Form And Get Default Address
* Empty and Add phone/Address Phone Form
* 
*/
function fgx_order_phone_form($in_div)
{
	$div_id = $in_div || '#spa_header';
	$($div_id).empty();
	
	var $div_default 	= '#default_address_phone';
	var $h_in 			=	x_getform_input($div_default);
	
	var $existing_address = $('#customer_address').text();
	var $existing_phone 	 = $('#customer_phone').text();
	var $h_ap = {address:$existing_address,phone:$existing_phone};
	var $div_ap = Mustache.to_html($mst_form_phone, $h_ap);
	//var $div_ap = Mustache.to_html($mst_form_phone, $h_in);
	
	$($div_id).append($div_ap);		
	fgx_default_address($div_id);

}

/*
* FX: fgx_update_phone: Update Phone And Address. PUT
*
*/
function fgx_order_add_phone()
{
  var $div_form_phone	= '#bizo_address_phone';
  var $h_form 				= x_getform_input($div_form_phone);
  
  var $bizorderid = bgx_get_bizorderid();
  var $phone = $h_form.phone;
  var $line1 = $h_form.streetaddress1;
  var $addressid = $h_form.addressid;
  
  var $url = '/g/order/' + $bizorderid;
	

  if($bizorderid 
  			&& 
  		($line1 || $phone || $addressid)
  )
  {

	$.ajax(
	{
         url:  				$url,
         type: 				'PUT',
			data:      			$h_form,
			dataType: 			"json",
         error: function(xhr, status, err)
         {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				$("#rest_messages").append($error_msg);
         },
	 		success: function(xdata)
       	{
       	  Materialize.toast("Address and Phone update", 5000);
       	  bx_more_link_del(bizo_address_phone);
	  		  dgx_fetch_anorder($bizorderid);       		
         },
      }
    );
	//Ajax End
	
	}
	else 
	{
			Materialize.toast("Missing Address or Phone", 3000);
	}

}


/*
*
* FX: fgx_order_make_bill: Make a Bill. POST
*
*/
function fgx_order_make_bill()
{
  var $bizorderid = bgx_get_bizorderid();
  var $url_post = '/g/order/bill/' + $bizorderid;

  if($bizorderid)
  {
  		Materialize.toast("Ready for Bill", 3000);
		$.ajax(
		{
         url:  				$url_post,
         type: 				'POST',
			dataType: 			"json",
         error: function(xhr, status, err)
         {
				bx_toast_empty();
				var $error_msg = bx_extract_err_msg(xhr);
				Materialize.toast($error_msg, 5000);
				$("#rest_messages").append($error_msg);
         },
	 		success: function(xdata)
       	{
       	  Materialize.toast("Updated", 5000);
       	  Materialize.toast("Billing complete for "+$bizorderid,3000);
			  var $url_ok = '/g/cart/bill/'+$bizorderid;
			  window.location.href = $url_ok;
         },
      }
    ); 
	//Ajax End
	
	}
	else 
	{
			Materialize.toast("OrderID not found", 3000);
	}

}


/*
* FX: fgx_list_orders: Display Orders(including bills) for Guest
*
* Handles Pagination through Data and Moustache
*
*/
function fgx_list_orders($inpage,$in_url)
{

	var $page 		=  $inpage || 1;

	var $in_date  	= g_choosen_date();
		
	var $div_tm 	= "#list_orders";

	var $div_id_msg = "Please wait for the orders....";
	
	var $base_url;
	if($in_url)
	{
		$base_url = $in_url;
		$in_date = "";
	}	
	else
	{
		var $base_url = '/g/order/list' + "/" + $page; 
	}

	progress_append($div_tm);

	if($base_url)
	{		
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"GET",
						dataType: 	"json",
 						error: function (xhr, status, error) 
 						{
								bx_toast_empty();
								var $error_msg = bx_extract_err_msg(xhr);
								//Materialize.toast($error_msg, 5000);
								$($div_tm).empty();
								$($div_tm).append($error_msg);
            		},
						success: function( xdata )
						{
								$($div_tm).empty();
								fgx_all_orders(xdata,$div_tm);
						},
				}	
		);
	}
	
};



/*
* FX: trg_all_bookings: Foreach Loop through All the bookings.
*
* Has next and previous for pgx.
*
*/
function fgx_all_orders(xdata,$in_div)
{
	   var items 			 = [];
  
  		//Pgx part
  		var $div_pgx = "#list_orders_top";
		var $h_pgx = {
											next:			xdata.next,
											previous:	xdata.previous,
											page:			xdata.page,
						};
										
 	   var $xmethod = 		"fgx_list_orders";
		var $str_pgx = 		bgx_page_nf_pg($h_pgx,$xmethod);
		$($div_pgx).empty();
	   $($div_pgx).append($str_pgx);	
	  
	  var $count = 0;	  
	  $.each( xdata.items, function( key, val ) 
	  {
	  		 val.count = $count;	

			 var 	$row_color = 'white blue-text';
			 if( ($count%2) == 0)
			 {
			 		$row_color = 'yellow lighten-5 black-text';
			 }			    
			 val.row_color = $row_color;		  		 
	  		 
			 var $x_item = Mustache.to_html($mst_guest_anorder, val);
	  		 items.push($x_item);
	  		 $count++;
  	  }) 


  $( "<ul/>", 
  {
    "class": "collapsible accordion",
    html: items.join( "" )
  }).appendTo( $in_div );
   
  g_collapse_init();
  
	
}

var $mst_guest_anorder = "<li> "
  	    			+ " <div class='collapsible-header  btn waves-effect  {{row_color}}'>"
	   			+ " <div class='row'>"
               + "            <div class='col l1 m1 s1 left tag'>&#10095;</div>"
	   			+ "				<div class='col l10 m10 s10'> OID:{{orderid}} @ {{bizorderdate}} </div>"
	   			+ "		</div>"
	   			+ " 	    				</div>"
 	    			+ "<div class='collapsible-body yellow lighten-4' >"
 	    				+ "<div class='panel'> "
 	    					+ " <div class='row'>"
 	    						+ "<div class='col l4 m4 s4'>+{{bizorderdate}}</div>"
 	    						+ "<div class='col l4 m4 s4'>{{branch_name}}</div>"
 	    						+ "<div class='col l4 m4 s4'>"
 	    							+"<a class='btn orange' href='/g/cart/{{orderid}}'> Show Order </a>"
 	    						+" </div>"
 	    					+ "</div>"		
 	    				+ "</div>"
 	    			+ "</div>"
 	    			+ "</li>"
					;
