/*
	 2015-12-25
	 ender.js
	 
	 Use it in the footer. Used for Ajax generally or end of file stuff.
	 
    This file is part of BAEL.
    Copyright (C) 2016,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/*
* Function prefix: eac_
*  
*/


/* 
* Selected Fields from Ajax Drop Down for AutoComplete  
*/
function eac_get_selected_value()
{
	var $a_value =	$(".easy-autocomplete").
            			find("ul li.selected div[data-item-value]").attr("data-item-value");
   return $a_value;         			
}

//NAme
function eac_get_selected_name()
{
	var $a_value =	$(".easy-autocomplete").
            			find("ul li.selected div[data-item-value]").text();
   return $a_value;         			
}


/********* Search Ajax Product Code ******/

/* Branch Product AutoComplete for only Name
*	<input id="new_pc" type="text" class="" name="product" placeholder='Product' /> 
*
*/
var br_product_options = {
	
	 dataType: 			'json',
	 listLocation: 	'items',
    url: function(phrase)
    {
    	  $branchid = bgx_get_branchid() ;
        if ( $branchid && (phrase !== "" && phrase.length > 2) )
        {
        		var $url		= "/g/retail/item/list/" + $branchid + "/" + phrase;
        		//Materialize.toast($url,1000); 
            return $url ;    
        } 
        else
        {
        		//Materialize.toast("No Data",1000);
        		var $abc = 'nothing';
        }
    },
	getValue: function(element)
	{
        return element.product_name;
        //Fills value in Search Box
   },
   template: 
   {
   	  //Display in DropDown	
        type: "custom",
        method: function(value, aitem) 
        {
        		 var    $an_item ="<div data-item-value='" + aitem.product_code 
            							+ "' >" + aitem.product_name + "</div>"; 
             return $an_item; 
        }
   },
   list:
   {
		  onSelectItemEvent: function()        
        {
            var selected_item_code = eac_get_selected_value();
            $("#new_pcode").val(selected_item_code);

            /* var selected_item_name = eac_get_selected_name();
            $("#new_pc").val(selected_item_name);
            Materialize.toast(selected_item,2000);  */
            
        }
    },
    theme: 'green-light',
   
};
$("#new_pc").easyAutocomplete(br_product_options);

/*
* FX:: eac_product_search_init()
* Initiates Ajax Product Search Input Box
*
*/

function eac_product_search_init()
{
	$("#new_pc").easyAutocomplete(br_product_options);
}



/********* Search Ajax City ******/

var home_options = {

	 dataType: 			'json',
	 listLocation: 	'cities',

    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
            return "/asearch/cities?q=" + 
            	phrase + "&format=json&"  ;    
        } 
    },
	
	getValue: function(element)
	{
        return element.name;
   },
    
   template: {
        type: "custom",
        method: function(value, item) 
        {
            return "<div data-item-value='" + item.code + "' >" + value + "</div>";
        }
   },
   list: {
		  onSelectItemEvent: function()        
        {
        
            var selectedItemValue = 
            $(".easy-autocomplete").
            	find("ul li.selected div[data-item-value]").attr("data-item-value");
            //$("#xr").css( "backgroundColor", "red" );	
            //$("#sin_citycode").val("ABC");	
            $("#sin_citycode").val(selectedItemValue).trigger("change");
            $("#only_citycode").val(selectedItemValue).trigger("change");
        }
    }
};
$("#sin_city").easyAutocomplete(home_options);

$( "#sin_citycode" ).change(function()
{
   eac_fetch_categories();   
   $("#xr").css( "backgroundColor", "red" );
});

/*
* FX: eac_fetch_categories: OF Items for A City

*/

function eac_fetch_categories()
{
	var $ci_st_co = $( "#sin_citycode" ).val();
	//$("#xr").html("");
	//$("#xr").append($ci_st_co);

   $('#dropdown_pgroup').material_select('destroy');
   //Materialize Select Destroy
   
   var $progress = "<div class='progress'><div class='indeterminate'></div></div>";
	$("#dpg_progress").append($progress);
   
	$.getJSON( "/biz/search/city_categories/"+$ci_st_co, 
	function( pgroups ) 
	{
	   $("#dropdown_pgroup").html("");
	   var $cityname = "Select";
	   $cityname = pgroups.cityname;
	  	$cn= "<option>" + $cityname + "</option>";
	   
	   //$("#dropdown_pgroup").append($cn);
	   //$("#xr").html("");
	   var $count = 0;
	   $.each( pgroups.items, function( key, val ) 
	   {
	  	   var $gn    = val.groupname || 'ABC';
	  	   $count++;
	  	   $f_b = "<option value='"+$gn+"'>";
	  	   $f_e = "</option>"
	  	   var $xe = $f_b+ $gn +$f_e;
	  	   $("#dropdown_pgroup").append($xe); 

  		});//ForEach
  		

    $('#dropdown_pgroup').material_select();//Materialize Select
	  	     
	});//Function
	
	$("#dpg_progress").html("");//PRogress Indicator
	
};//Function

 $(document).ready(function() {
    $('#dropdown_pgroup').material_select();
    //Materialize Select
  });

$( "#dropdown_pgroup" ).change(function()
{
	eac_fetch_icategory_branches(); 
});

/*
* eac_fetch_icategory_branches: For A City with Product Category

*/

function eac_fetch_icategory_branches()
{
	var $ci_st_co = $( "#sin_citycode" ).val();
	var $in_group = $( "#dropdown_pgroup" ).val();	

	if($ci_st_co)
	{

		   //Materialize Select Destroy
		   $("#home_results").html("");
		
		   var $progress = "<div class='progress'><div class='indeterminate'></div></div>";
			$("#home_results_wait").append($progress);
		
		
			$.getJSON( "/biz/search/city_cat_branch/"+$ci_st_co+"/"+$in_group, 
			function( pg_branches ) 
			{
			   var $cityname = "Select";
			   var $groupname = pg_branches.groupname;
			   var $cityname  = pg_branches.cityname;	   
			  	$cn= "<span> Results for " + $groupname + " in " + $cityname + "</span>";
			   
			   $("#home_results").append($cn);
		
			   var $count = 0;
			   $.each( pg_branches.items, function( key, val ) 
			   {
						var $xe = ex_branch(val);
			  	   	$("#home_results").append($xe);
		  		});//ForEach
			  	     
			});//Function
		   $("#home_results_wait").html("");
		   
	}
	
};//Function

/*
* FX: ex_branch: Handle Branch data for Guest
* using mst_guest_branch_in_business
* branchid,business_name,branch_name,address
* 
*/
function ex_branch(val)
{
		var $xy = Mustache.to_html($mst_guest_branch_in_branches, val);
		return $xy;
}

var $mst_guest_branch_in_branches = 
					  "<div class='row card'>"
					  	+	 "<div class='card-content'>"
						  	  		+ "<div class='card-title'>"
						  	  			+ " <a class='green-text white' "
						  	  					+ " href='g/business/branch/{{branchid}}' > "
						  	  					+ "{{business_name}},{{branch_name}} </a>"
						  	  		+ "</div>"
						  	  		+ "{{address}}"
						  	 + "</div>"
						+ "</div>"		;

/*
*
* Only CityCode to Branches
* Uses Fetch_city_branches
* 
*/
$( "#only_citycode" ).change(function()
{
   eac_fetch_city_branches();   
});

/*
* FX: eac_fetch_city_branches
*
*/

function eac_fetch_city_branches($in_page,$in_url)
{
	$page = $in_page || 1;
	var $ci_st_co = $( "#only_citycode" ).val();
	
	var $div_tm = "#home_results";

	var $url;
	if($in_url)
	{
		$url = $in_url;		
	}
	else
	{
		$url = "/biz/search/city_branches/"+$ci_st_co+"/"+$page;
	}	

	if($ci_st_co)
	{

		$($div_tm).empty();
		$($div_tm).append($progress_clock);
		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
						error: function (xhr,status,error)
 						{
 								$($div_tm).empty();
 								var $me = "<span class='red-text'>No Branches for the City "
 										+ "</span>";
								$($div_tm).empty(); 										
								$($div_tm).append($me);
            		},
						success: function( xdata )
						{
							
								$($div_tm).empty();
								
							   var $x_pg = xdata.page;
								var $h_pgx = {
											next:			xdata.next,
											previous:	xdata.previous,
											page:			$x_pg,
										};
										
								var $xmethod = 		"fetch_city_branches";
								var $div_pgx = 		bgx_page_nf_pg($h_pgx,$xmethod);
							   $($div_tm).append($div_pgx);						      
							
								var $cityname  = xdata.cityname;
								$cn= "<span> Results for " + $cityname + "</span>";
							   $($div_tm).append($cn);
					
						   	var $count = 0;
						   	$.each( xdata.items, function( key, val ) 
						   	{
									var $xe = ex_branch(val);
						  	   	$($div_tm).append($xe);
					  			});//ForEach
					  			
						},
				}	
		);
   }

/*   
	$.getJSON( "/biz/search/city_branches/"+$ci_st_co, 
		function( xdata ) 
		{

			var $cityname  = xdata.cityname;
			$cn= "<span> Results for " + $cityname + "</span>";
		   $("#home_results").append($cn);

	   	var $count = 0;
	   	$.each( xdata.items, function( key, val ) 
	   	{
				var $xe = ex_branch(val);
	  	   	$("#home_results").append($xe);
  			});//ForEach

	});//Function getJSON
	*/
	
};//Function


/* Universal Product AutoComplete for only Name
*	<input id="sin_product" type="text" class="" name="product" placeholder='Product' /> 
*
*/
var product_options = {

    url: function(phrase)
    {
        if (phrase !== "" && phrase.length > 2)
        {
            return "/asearch/product/" +   	phrase  ;    
        } 
    },
	
	getValue: function(element)
	{
        return element.name;
   },
    
    template: {
        type: "custom",
        method: function(value, item) 
        {
            return "<div data-item-value='" + item.name + "' >" + value + "</div>";
        }
    },
};
$("#sin_product").easyAutocomplete(product_options);

/* Universal Product Name AutoComplete END */


//<input class="datepicker_display_only btn-flat black-text"  
//data-value='2016-03-25' DISABLED size='12'>	
 $('.datepicker_display_only').pickadate
 ({
    format: 		'dd-mmm-yyyy',
    formatSubmit: 'yyyy-mm-dd',    
 });

//Css in style.css
$('.timepicker_display_only').pickatime
 ({
    format: 'HH:i a',
    formatLabel: '<b>h</b>:i <!i>a</!i>',
 });


  $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      //stopPropagation: 'true',	
    }
  );
        