/*
	 2016-08-05
	 retail.js, ReserveTable Utilities for BAEL

    This file is part of BAEL
    Copyright (C) 2016,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/

/*
Naming Conventions:
Constant Variables Prefix: rtl_
Functions Prefix :  			rl_
*/

var $rtl_arrow_right = "&#10132;";

var $rtl_bill_pgx = "<input id='fnx_rest' type='hidden' value='rl_display_allbills' />"
							+ "<input id='fnx_rest_item_count' type='hidden' value='10' />"
							+ "<input id='fnx_date' type='hidden' value='' />"
							+ "<input id='fnx_bill_orderid' type='hidden' value='' />"
							+ "<input id='fnx_bill_billnumber' type='hidden' value='' />";
							
var $rtl_bills_div = "<div class='row'>"
							+ "<div id='xr'></div>"
 							+ "<div align='center' id='div_allbills' >"
                		+ "<div class='progress'>"
           				+ "	<div class='indeterminate'></div>"
                		+ "</div></div></div>";

               		
/*
* FX: rl_display_allbills: Display All Bills
* @params: incount,inpage,indate,
* @params: appid from div
*
*/

function rl_display_allbills(incount,inpage,indate)
{

  var $fnx;
  var $appid =  bgx_get_appid();
  if($appid)
  {
 		var $url;
  		if($appid == $c_appid_retail)
  		{
  			$url = "/retail/bill/list";  	
  		}
  		
  		$fnx = $url ;
	}
	else
	{
			//PO
  			Materialize.toast("APP not found",5000);
  			$url = "/retail/bill/list";
	  		$fnx = $url ;
  			
	}

	//Search Input First
	var $x_date = g_choosen_date();
	if($x_date)
	{
			$in_date = $x_date;
	}
	var $search_orderid    = $('#fnx_bill_orderid').val()    || 	$('#i_orderid').val() ;
	var $search_billnumber = $('#fnx_bill_billnumber').val() || 	$('#i_billid').val();;
	Materialize.toast($in_date,2000);	

	var $div_tm = "#txr";
   $($div_tm).empty();

	//Search Options Div  
	$dv_search = g_bills_search($appid);
	$($div_tm).append($dv_search);
   

	var $v_method  = "rl_display_allbills";   	
   var $pgx_ul    = bgx_display_pgx($v_method);
	$($div_tm).append($pgx_ul);	

	$($div_tm).append($rtl_bill_pgx);	
	$($div_tm).append($rtl_bills_div);



	var $in_page  = inpage || 1;
	var $in_count = incount;		

	//Search Options
	var $in_date;
	
	
	
	//var $in_m = $in_rest + "/20/1";	
	var $in_m = $fnx + "/" + $in_page + "/" + $in_count ;
	var $rest_pg_txt = "#rest_pagination_text";

	$.ajax( 
				{ 
				url: $in_m ,
				type: "GET",
				dataType: "json",
				headers: { 
								'indate':      $in_date,
							   'orderid':		$search_orderid,
							   'billnumber':	$search_billnumber,							   
							  },
 				error: function (xhr,status,error) 
 				{
 						var $me = "<span class='red-text'>No Bills for Page:"
 								+ $in_page + "</span>";
 						$('#div_allbills').empty();
 						$('#div_allbills').append($me);
            },
				success: function( data )
				{
					rl_display_bills(data);
					g_choosen_date($in_date);
				},
					//GetJSON Function_data

	});
	//ajax

	

}// Function_dda

/*
* FX: rl_display_bills: Displays bills based on Data
* 
* Display Bills in div: #div_allbills
* @params: data
*
*/
function rl_display_bills(data)
{
   		       var items = [];
   		       
   		       var $idx_first = data.idx_first;
   		       var $idx_last = data.idx_last;
   		       var $idx_str = $idx_first + "-" + $idx_last;
   		       var $rest_pg_txt = "#rest_pagination_text";	  		
   		       $($rest_pg_txt).empty();
   		       $($rest_pg_txt).append($idx_str);
   		       
         		 $.each( data.items, function( key, val ) 
         				{
         			 		var $m_b = rl_div_bill(val);
								items.push($m_b);         			 		
                		}
                );
                //ForEach
		
					//var $m_tc = " </tbody>  </table>";
					//items.push($m_tc);	

  					$('#div_allbills').empty();

  					$( "<div/>",  { "class": "row",html: items.join("") }).
  						appendTo( "#div_allbills" );
  					//
  					g_datepicker_init();//Date box

}

   var $mst_list_abill   = 
	                    "<div class='card col l4 m6 s12 border'>" +
	                    		" <div class='card-content black-text'> "
					      + "<span class='card-title'>" 
					      + 	 "B-{{billid}}</span>"
					      + "<p>{{billdate}}"+ "{{str_otype}}" +"</p>" 
					      + 	"<p><b>O-{{days_order}}</b>, OID-{{orderid}}</p>"				      
					      + "{{#customerid}}"
									+ "<p>C: {{customerid}}</p>"					      
					      + "{{/customerid}}"
					      + "{{^customerid}}"
									+ "<p>U: {{userid}}</p>"					      
					      + "{{/customerid}}"
					      
					      + "{{#str_status}}"
					      		+			     "{{{str_status}}}"
					      + "{{/str_status}}"
					       					      
	                  + "<div class='card-action'>"
	                   	+ "{{{link_btn}}}"	 
					      + 		" </div>"
					      
			      + "</div></div>";


/*
* FX: rl_div_bill: Display A single Bill in bill list.
*
* @params: data.val
* @returns: a String(Mustache).
* REST/FTO have different div for bill
*
*/
function rl_div_bill(val)
{
     var $m_url  			= val.url;
     var $m_txnid		   = val.transactionid;
	  var $m_cancel	= val.cancelled_at;
	  
	  var $order_type = val.order_type;
	  var $str_status;
	  var $str_payment; 
	  var $str_cancel = '<span></span>';
	  if($m_cancel)
	  {
	  		$str_cancel = " <span class='red-text'><b>(X)</b></span> ";
	  		$str_status = $str_cancel;
	  		//Materialize.toast("Cancelled",2000);
	  }        
	  else if($m_txnid > 0)
		{
			$str_payment = "<span class='green-text bold'>PAYMENT</span>";
			$str_status = $str_payment;
		}
	 	else
		{
			$str_status = "<span class='red-text '><b>PENDING</b></span>";;
		}

	   var $str_order ="<p><b>O-{{days_order}}</b>, OID-{{orderid}}</p>";
      var $m_userid			= val.userid ;
      var $m_customerid   = val.customerid;
      var $str_user = "<p>U: {{userid}}</p>";
	                         
		if(!$m_userid && $m_customerid)
		{
		  	$str_user = "<p>C: {{customerid}}</p>";
		}


		var $v_billid = val.billid;
		var $orderid = val.orderid;
		var $fn_click = "onClick=rl_abill('"+$v_billid +"');";
		var $str_otype ="";
		if($order_type == $c_order_cn)
		{
			
			var $fn_click = "onClick=rl_credit_note('"+$v_billid +"','"+$order_type+"');";
			var $str_otype = ", Credit Note";
		}		
		else if($order_type == $c_order_po)
		{
			var $str_otype = ", Purchase";
		}		
			   
		var $info_btn = "<a class='btn-floating waves-effect waves-teal "
					+ " red white-text center' id='abill' "+ $fn_click 
					+ " > " + $rtl_arrow_right + "</a>";

		var $h_abill = {
									'link_btn'	: $info_btn,
									'str_otype'	: $str_otype,
									'str_status'			: $str_status,									
									
									
									'billid'		: val.billid,
									'billdate'	: val.billdate,
									'days_order': val.days_order,
									'orderid'	: val.orderid,
									'customerid': val.customerid,
									'userid'		: val.userid,

									'transactionid': $m_txnid,	
							};
	                         
	
		var $mx_mst = Mustache.to_html($mst_list_abill, $h_abill);
		return $mx_mst;
		
}



/*
* rl_display_neworderbtn: Creates New Order Button in div: #txr
* 
* @requires: appid val
*
*/
//Used in only for FoodTakeout
function rl_display_neworderbtn()
{
	var $div_tm = "#txr";
   $($div_tm).empty();
	
   var $appid = $('#in_appid').val();
	var $fn_click = "onClick=rl_create_neworder();"	   
	var $nbtn = "<a class='btn green' id='retail_neworder' "+ $fn_click +" ><b>New Order</b></a>";

	$($div_tm).append($nbtn);
}

/*
* FX: rl_create_neworder: Creates Retail Order and calls mov_pager(bizorder)
* 
* @params: gets sx_type.
* 
*/
function rl_create_neworder()
{
	var $orderid ;
	var $sx_type  = $("#sx_type").val();
	//console.log($tbn)
	var $in_m = "/retail/order/creation/";
	$("#abc").css( "backgroundColor", "red" );
	   
	$.ajax( 
				{ 
				url: $in_m ,
				type: "POST",
				dataType: "json",
				timeout: 5000,
 				error: function (xhr,status,error) 
 				{
 						$('#abc').append("Error.");
            },
				success: function( data )
				{
						var $oid = data.orderid;
						$orderid = $oid;
					   $("#bizorderid").val($orderid);
						if($orderid )
						{
								$("#sx_type").val('bizorder');
								$("#sx_value").val($orderid);
								move_pager('bizorder');							
						}
				}	,//success
				
	}); 
			   						    
}//Fn end


/*
* FX: rl_display_anorder: Gets data for Bizorder and calls rl_bizorder.
*
* @params: bizorderid
*
*/
function rl_display_anorder($x_bizorderid)
{
	var $url = "/retail/order/";
	var $appid = $('#in_appid').val();

	var $div_tm = "#txr";
   $($div_tm).empty();
   
   var $d_bo = "<span>OID: "+ $x_bizorderid +"</span> <span class='order_count'></span>"
   		+ "<div id='bizorder' class='card-panel yellow-lighten-4'></div>";   
   $($div_tm).append($d_bo);
	
	$.getJSON( $url + $x_bizorderid, 
				function( datafc ) 
				{
 					rl_bizorder(datafc);
				}
				//function datafc
	);

}

var $mst_rt_order_prod =		 
					"<div class='row  collection-item  {{row_color}} '> "
									+ "<div class='col l1 m3 s2 '> [{{count}}]</div>"
			  	   				+ "<div class='col l4 m3 s10 bizorder_item_code'> {{product_name}} </div>"
	  		   					+ "<div class='col l1 m1 s6 bizorder_item_price '>"
										+ " @ </div> "	  	   
	  		   					+ "<div class='col l3 m2 s6 bizorder_item_price '>"
										+ "{{ price }}</div> "	  	   
	  		   					+ "<div class='col l3 m3 s2 bizorder_item_unit'>"
										+ " x {{ units }}</div> "	  	   
					+ "</div>";	
var $mst_rt_addprod_form = 
				"<div class='card row green-text'>"
						+"<div class='col l3 m6 s12 input-field'>"
								+"<input id='new_pc' type='text' placeholder='Search Name' >"
									+"</input>"
							+"</div>"								
						+"<div class='col l3 m6 s12 input-field'>"		
								+"<input id='new_pcode' type='text' placeholder='barcode' >"
								+"</input>"
								+"<label for='new_pcode'>Barcode</label>"
							+"</div>"
						+"<div class='col l2 m6 s12  input-field'>"
								+"<input id='new_pc_unit' type='number' "
										+" min='0'  value='' ></input>"
										+"<label for='new_pc_unit'>Units</label>"
									+ "<input id='new_pc_price' type='hidden' />"
										+	"</div>"
						+ "<button type='button' class='col l3 m6 s12 btn green' {{{add_btn}}} "
							+ " >+ Add Item"
						+ "</button>"
						+ "<div class='col l12 m12 s12' id='new_pc_selected_price'></div>" 
					+ "</div>"
					+ "<div id='multiple_prices'></div>";

var $mst_rt_bill_create = 
						"<div class='row green-text'>"
								+"<label class='col l12 m12 s12 btn blue lighten-3 black-text' {{{mk_btn}}}"
								+" ><b>Make Bill</b></label>"
						+ "</div>";


/*
* FX: rl_bizorder: Displays an order from the Data
* @params: data
*
*/
function rl_bizorder(datafc)
{
		var $div_tm = "#txr";	
	 	var fitems = [];
		var $aitems = [];

		var $div_bo = "#bizorder";		
  		$($div_bo).empty(); 

		var $in_bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();

		var $div_mk_bill;var $div_new_form;

		//Mk Bill Button		
		if($in_bizorderid && datafc.items)
		{
				var $fn_clk_bll = "onClick=rl_make_bill('"+$in_bizorderid+"');"
				var $h_mkbill = {
										mk_btn : $fn_clk_bll,
									 }
				$div_mk_bll = Mustache.to_html($mst_rt_bill_create, $h_mkbill);							
				
		}

		//Add Product Form
	   if($in_bizorderid)
	   {
				var $fn_add_pc = "onClick=rl_add_pc('"+$in_bizorderid+"');"
				var $h_new_form ={
											add_btn: $fn_add_pc,
										} ;
				$div_new_form = Mustache.to_html($mst_rt_addprod_form, $h_new_form);								
				
		}
					
		var $count = 0;
		var $total = datafc.total || 0;		
		var $str_total = "<span class='green-text'>, <b>Total: "+$total +"</b></span>";
		var $a_count = 0;
		
	   $.each( datafc.items, function( key, val ) 
	   {

			 var $count = key + 1;
			 var $row_color = ' white grey darken-4-text ';
			 if( ($count % 2) == 0)
			 {
			 	$row_color = ' white blue-text';
			 }			    

			 var $h_oprod = { row_color:  $row_color,
			 						count:		$count,
			 						price:		val.price,
			 						units:		val.units,
			 						product_name: val.product_name,	 
			 					 };
	   	
			var    $mx_div = Mustache.to_html($mst_rt_order_prod, $h_oprod);
	  	   fitems.push($mx_div);
	  	   $a_count++;
  		});
 
  //1 Add Make Bill	
  if($a_count > 0)
  {
  		$($div_bo).append($div_mk_bll);
  }
  //2 Add New Form
  $($div_bo).append($div_new_form);
  $("#new_pc").easyAutocomplete(br_product_options);

  //3 Add List products Added  
  $( "<div/>", 
  {
    "class": "collection",
    html: fitems.join( "" )
  }).appendTo( $div_bo );
  
  var $bizorder_items = $('.bizorder_item_code').length;
  $( '.order_count' ).html("");
  var $str_count = "[Items: " + $bizorder_items +" ]";
  $( '.order_count' ).append($str_count);
  $( '.order_count' ).append($str_total);	
  
}

/*
* FX: rl_add_pc: Adds a product to the Bizorder
*
*
*/

function rl_add_pc($bizorderid)
{

   var $div_bo = "#bizorder";		
	
	var $prname				= $("#new_pc").val();
	var $prcode				= $("#new_pcode").val();
		
	var $new_pc_unit 		= $("#new_pc_unit").val() || 1;
	var $new_pc_price 	= $("#new_pc_price").val();

	var $new_pc;
	if($prcode)
	{
		$new_pc = $prcode;
	}
	else
	{
		$new_pc = $prname;	
	}	
	
			
	var $url = "/retail/order/product/";
	var $in_m = $url + $bizorderid + "/" + $new_pc + "/" + $new_pc_unit + "/" + $new_pc_price;

	if($new_pc)
	{

		$.ajax( 
				{ 
					url: 			$in_m ,
					type: 		"PUT",
					dataType: 	"json",
 					error: function (xhr,status,error) 
 					{
							bx_toast_empty();
	 						var $error_msg = bx_extract_err_msg(xhr);
							Materialize.toast($error_msg, 5000); 						
            	},
					success: function( data )
					{
						bx_display_error(1);	
						if(data.products)
						{
							rl_display_anorder($bizorderid);
						}
						else if(data.prices)
						{
							rl_display_multiple_prices(data);
						}
					},
					//Function Success
				}
		);
		//ajax
	
	}//If

}

/*
* FX: rl_add_pc_price: Creates a Materialize Toast after the product is added.
* Used in Select Price for Retail.
* 
*/
function rl_add_pc_price($product_code,$price)
{
		var $div_in_price = "#select_price_unit_" + $price ;
		
		var $div_out_pc	 = "#new_pc";
		var $div_out_price = "#new_pc_price";
		var $div_out_unit  = "#new_pc_unit";
		var $div_out_display_price = "#new_pc_selected_price";
		var $div_mprices = "#multiple_prices";
		
		var $in_units = $($div_in_price).val();

		var $emsg = "Adding  " + $product_code + " for price: "+ $price;
		
		$($div_out_pc).val($product_code);
		$($div_out_price).val($price);
		$($div_out_display_price).append("Selected Price: "+$price);

		Materialize.toast($emsg, 2000);
		$($div_mprices).empty();
		
}

/*
* FX: rl_display_multiple_prices: Display Multiple Prices for a product.
*
* @params: data
*
* Adds Mustache String to div:#multiple_prices 
*/

function rl_display_multiple_prices(data)
{
	   $("#multiple_prices").empty();
		var $product_name = data.product_name;
		var $product_code = data.product_code;
		var $bizorderid   = data.bizorderid;		
		
		var $emsg = " ... "+ $product_name +" could not be added. Multiple Prices";
		Materialize.toast($emsg, 1000);

		var $d_items = [];
		var $x_prices = [];
		$x_prices = data.prices; 

		var $str_title = "<div class='card-title red-text'> Multiple Prices for "
				+ $product_name 
					+"</div>"; 
		$d_items.push($str_title);
				
	   $.each( $x_prices, function( key, val ) 
	   {
	   	var $mx_aprice ='a';
	   	
	   	$mx_aprice =	"<div class='row green-text'> "
	   							+ "<div class='col l6 m6 s6 left'> {{price}} </div>"
				   				+ "<div class='col l6 m6 s6 ' >"
	   										+ "<btn class='btn red center' "
	   										+ " onClick=rl_add_pc_price('{{product_code}}','{{price}}'); > " 
	   										+ " Select </btn>"
	   							+"</div>"
								+ "</div>";	
	   	
			var $mx_div = Mustache.to_html($mx_aprice, val);
			$d_items.push($mx_div);

			
			
		});

  $( "<div/>", 
  {
    "class": "card",
    html: $d_items.join( "" )
  }).appendTo( "#multiple_prices" );

		//$("#multiple_prices").append($str_top);
}

/*
* FX: rl_abill: Displays a bill.
*
* @params: billid
*
*/

function rl_abill($billid)
{
		var $dv_bill = '#list_of_bill_items';
		$("#billid").val($billid);
		$("#bizorderid").val("");
		
		var $div_txr = "#txr";
		$($div_txr).empty();		
		
		var $ul_bill_top	= bx_bill_top();
		$($div_entity_more).empty();//Flush This Part.
		$($div_entity_more).append($ul_bill_top);
		
		var $div_bill = "<div id='diner_index'></div>"
				+"<div id='idprint' class=''><div id='list_of_bill_items'></div></div>";
		$($div_txr).append($div_bill);
		
		var $dv_bxd 		= bx_bill_details();
		$($div_bill).append($dv_bxd);

		bx_diner_bill_txns();
			
}

/*
* FX: rl_credit_note: 

*/

function rl_credit_note($billid,$order_type)
{
	
		//To USe: bx_bill_txns(),bx_bill_final_h();
		
		var $dv_bill = '#list_of_bill_items';
		$("#billid").val($billid);
		$("#bizorderid").val("");
		
		//Empty
		var $div_txr = "#txr";
		$($div_txr).empty();	

		var $ul_bill_top	= bx_bill_top();
		$($div_entity_more).empty();//Flush This Part.
		$($div_entity_more).append($ul_bill_top);
			
		
		var $div_bill = "<div id='diner_index'></div>"
				+"<div id='idprint' class=''><div id='list_of_bill_items'></div></div>";
		$($div_txr).append($div_bill);
		
		var $dv_bxd 		= bx_bill_details();
		$($div_bill).append($dv_bxd);

		//This is the Main Stuff using bx_diner_bill_info(data)
		bx_diner_bill_txns($billid,$order_type);
			
}

/*
* FX: rl_make_bill: Makes bill for an Order
* @params: $orderid
*
* Runs rl_abill after getting the data succesfully
*
*/

function rl_make_bill(order_id)
{
   var $in_bizorderid = $( "#bizorderid" ).text() || $( "#bizorderid" ).val();
	var $url = '/retail/order/bill/';
	var $in_m = $url + $in_bizorderid;
	
	var $progress = "Please Wait.<div class='progress'><div class='indeterminate red'></div></div>";
	var $div_id = '#bizorder';
	var $div_id_msg = "Please wait while the bill is generated...." +   $in_bizorderid;
	progress_append($div_id,$div_id_msg);
	
	var $sx_type = $('#sx_type').val();
	
	if($sx_type)
	{

		$.ajax( 
				{ 
					url: 			$in_m ,
					type: 		"POST",
					dataType: 	"json",
 					error: function (xhr,status,error) 
 					{
						var $emsg = "Bill is failure";
						bx_display_error(0,$emsg); 						
            	},
					success: function( data )
					{	
						
						var $div_err = "#rest_messages";
						bx_display_error(1);   	
						//$($div_err).append("Done");
						
						var $bill_id = data.billid;
						$('#sx_type').val('bill');
						$("#billid").val($bill_id);
						$("#sx_value").val($bill_id);
						rl_abill($bill_id); 
					},
					//Function Success
				}
		);
		//ajax
		
	}
	
}

