/*
	 2015-12-25
	 treserve.js, ReserveTable Utilities for BAEL

    This file is part of BAEL
    Copyright (C) 2016,  Tirveni Yadav

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/

/*
* 
Naming Conventions:
Constant Variables Prefix: trv_
Functions Prefix :  			tr_
Functions Prefix for Guest: trg_

xhr: 			XMLHttpRequest

*
*/

var $trv_seconds_refresh = '2000';

var $mx_branch_capacity = 	"<li>"

		+ "<div class='card yellow lighten-3'>"
			+ "<div class='card-content row'>"
				+ "<div class='col l3 m12 s12 card-title red-text'>Timings </div>"

				+ "<div class='col l3 m6 s12'>"
						+ "{{#open}}"	
									+ "{{open}}" +" to "+" {{close }}"
						+ "{{/open}}"																								
						+ "{{^open}}"
									+ "Timings are not available."															
						+ "{{/open}}"																
				+ "</div>"
				  						
				+ "<div class='col l3 m6 s6'>Total Seats : {{total_capacity}}</div>"
				+ "<div class='col l3 m6 s6'>Total Tables: {{total_tables }}</div>"
  			+ "</div>"
  		+ "</div>"
					  			
		  				+ "</li>"
	  						
						+ "<li>"
							+"<div class='row'>"
								+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=tr_display_slots('1','1','{{previous}}');" 	 
									+ "> "
									+ "<< </span>"
								+ "<span class='btn col l4 m4 s6 red'>{{date}}</span>"
								+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=tr_display_slots('1','1','{{next}}');" 	 
									+ "> "
									+ ">> </span>"
	  							+ "</div>"
	  					+ "</li>"
	  				;

/*
* FX: tr_display_slots: Display Time Slots for a Day
*
* Handles Pagination throug Data and mst_branch_capacity
*/
function tr_display_slots($incount,$inpage,$in_url)
{

	var $page =  1;
	var $rows =  30;

	var $in_date = g_choosen_date();
	
	var $div_tm = "#txr";
   $($div_tm).empty();

	var $div_tm = "#txr";
   $($div_tm).empty();

	var $div_id_msg = "Please wait while the Items are fetched....";
	var $base_url = '/reservediner/available'; 

	if($in_url)
	{
		$base_url = $in_url;
		$in_date = "";
	}	

	progress_append($div_tm);

	if($base_url)
	{		
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"GET",
						dataType: 	"json",
						headers: 	{  'indate':$in_date, 
										},
						statusCode: {
    								404: function() {
									$('#txr').append("You have been logged out.");    									
    							},
    					},		
 						error: function (xhr, status, error) 
 						{
 								var $me = "<span class='red-text'>No slots for "
 										+ "</span>";
								$('#txr').append($me);
            		},
						success: function( data )
						{
							$($div_tm).empty();
		  					tr_get_slots(data,$rows,$page);
		  					g_choosen_date(data.date);
		  					
						},
				}	
		);
	}
	
};

/*
* FX: tr_get_slots: Iterate through SlOT data 
* 
*/
//Display Slots 
function tr_get_slots(data,$rows,$page)
{
	  var items 			 = [];
  
  	  var $x_date = data.date;	
	  //items.push($oitm);	  
	  var $mx_div_bc = Mustache.to_html($mx_branch_capacity, data);
	  items.push($mx_div_bc);	
	  
	  var $count = 0;	  
	  $.each( data.slots, function( key, val ) 
	  {
	  	    $count++;
	  		 var $xitem = tr_div_slot(val,$count,$rows,$page);
	  		 items.push($xitem);
  	  }) 

	
	  $( "<ul/>", 
	  {
	    "class": "collapsible popout ",
	    html: items.join( "" )
	  }).appendTo( "#txr" ); 
	
	  
	
	  $(document).ready(function(){
	    $('.collapsible').collapsible({
	      accordion : false 
	    });
	  });

     	if($count == 0)
	  	{
				var $me_err = "<span class='red-text'>No slots available " + $x_date + "</span>";
				$('#txr').append($me_err);
				Materialize.toast($me_err,2000);
   	}	  
		g_timepicker_init();        
	
}


/*
* FX: tr_div_slot: Display a Slot with Availability
*
*/
function tr_div_slot(val,$count,$rows,$page)
{
	var $aitm ;

   var $begin_time			= val.begin_time;
  	var $end_time				= val.end_time;
  	var $pending_capacity	= val.pending_capacity;
  	var $pending_tables		= val.pending_tables;
  	var $open_date				= val.open_date;   	 
  	var $form_id = "form_new_reserve_"+$begin_time;
  	var $booking_permitted = val.booking_permitted;
  	
  	 	
  	var $alt_color = $zebra_b;
  	var $row_count = val.row_count;
  	if($row_count % 2 == 0)
  	{
  		$alt_color = $zebra_a;
  	}

	var $fn_click;		
	$fn_click = "onClick=tr_add_reservation"
					   + "('{{open_date}}"	+ "'," 
						+ "'{{begin_time}}"	+ "'," 
						+ "'{{row_count}}" 	+ "',"
						+ "'" + $rows 			+ "',"						
						+ "'" + $page 			+ "'"						
						+ ");"

	/*$fn_click = " onClick=tr_add_reservation('"
				+ $open_date     
				+ "','" + $begin_time  
				+ "','" +$count
					+ "');" ;*/ 
	
	var $form_inside = "<div class='collapsible-body ' >"
			+ "<div class='row card blue lighten-5' id='form_{{row_count}}' >"
				+ 		"<div class='col l12 m12 s12'>Book New Reservation @ <b>{{begin_time}}</b></div>"
				+ 		"<div class='col l12 m12 s12'>"
				+ 			"Name  <input name=aname type='text' required /></div>"
				+ 		"<div class='col l12 m12 s12'>Email <input name='email' type='text' /></div>"					
				+ 		"<div class='col l12 m12 s12'>Phone<input name='phone' type='text' /></div>"
				+ 		"<div class='col l12 m12 s12'>Seats<input name='pax' min=0 type='number' required /></div>"
				+ 		"<div class='col l12 m12 s12'>Comments<input name='comments' type='text' required /></div>"
				+ 		"<div class='col l12 m12 s12 center'><button class='btn green' "+ $fn_click 
									+ ">+ Add Reservation</button></div>"											
				+ 		"<span class='col l12 m12 s12 xerrmsg' ></span>"												
			+ "</div>"
			+ "</div>";

	var $tag_drop = "&#10095;";
	var $form_booking ="";		
	var $icon_close_booking;	
	var $add_book = "";
	if($booking_permitted > 0)
	{
			$form_booking = $form_inside;
			$add_book =  "<div class='col l1 m4 s4 right'>"
								+ "<span class='btn-floating green center'>+</span>"
							+"</div>";								 	

	}		
	else
	{		
			$icon_close_booking = " icon_close_booking ";
			$tag_drop = "";
	}

	$mx_aitm = "<li>" 
				+ "<div class='collapsible-header waves-effect waves-purple " + $alt_color + "black-text '>"	
				+ "<div class='row '> "
				+ "<div class='col    l1 m1  s1 center tag "+ 
								$icon_close_booking + " '> "+ $tag_drop 
						+ " </div>"
				+ "<div class='col    l3 m5 s10 red-text center '>"
						+ "{{begin_time}}"
						+"</div>"
		
				+ "<div class='col 	 l3 m5 s12 center'>"

						+ "Reservations: "
						+ "{{#booked_tables}}"																 	
								+ "<b>+{{booked_tables}}</b>"
						+"{{/booked_tables}}"
						+"{{^booked_tables}}"
								+ "0"
						+"{{/booked_tables}}"

					+ "</div>"	
					
					+"{{#pending_capacity}}"
					+ "<div class='col l3 m6 s8 center'>"
								+ "Available Seats:<b>{{ pending_capacity }}</b></div>"								 	
 					+"{{/pending_capacity}}"

					+ $add_book
 					
					+ "</div></div>"
				+ $form_booking							
			+ "<li>";

	var $mx_div_aitm = Mustache.to_html($mx_aitm, val);
	
	return $mx_div_aitm;//$aitm;	
	
}

/*
* FX: tr_Add_Reservation: Add Reservations
*
* AJAX POST POST
* 
*/
function tr_add_reservation($in_date,$in_time,$in_count,$rows,$page)
{
	//$("#xyz").append("MeME");
	//$("#xyz").append($form_div);
	var $form_div = "#form_" + $in_count;

		
	var $aname 		= $($form_div+" :input[name='aname']").val();
	
	var $email;
	var $in_email 		= $($form_div+" :input[name='email']").val();
	var $phone 		= $($form_div+" :input[name='phone']").val();	
		
	var $comments 	= $($form_div+" :input[name='comments']").val();	
	var $pax 		= $($form_div+" :input[name='pax']").val();

	var $div_derry = $($form_div).find(".xerrmsg");	

	$email = $in_email || $phone;		
		
	//$("#xyz").append($aname);
	//$($div_derry).append($email);
	var $url = "/reservediner/book/"+$in_date+"/"+$in_time+"/"+$pax;
	
	if($email && $phone && $pax && $aname)
	{
		Materialize.toast("Reservation is being processed", 2000);
	}
	else
	{
		Materialize.toast("Missing Field", 2000);
	}
	
	var $h_out = {  'email':		$email, 
											'phone':		$phone,
											'name': 		$aname, 
											'comments': $comments,	
										};
	//$($div_derry).append($progress_append);
	//Materialize.toast($phone, 2000);
	
	if($email)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"POST",
						dataType: 	"json",
						data: 		$h_out,
 						error: function (xhr, status, error) 
 						{
							var $jr = JSON.parse(xhr.responseText);
						 	var $error_msg = $jr.error;
	 						var $me = "<span class='red-text'>No Reservations made. "
 								+ $error_msg + "</span>";
 								
 							$($div_derry).empty();
 							$($div_derry).append($me);
 						
            		},
						success: function( data )
						{
							Materialize.toast('Added Reservation.', $trv_seconds_refresh)
							$('#zyz').append("Edited.");
							window.setTimeout(function(){         
											tr_display_slots($rows,$page);     
                  			}, $trv_seconds_refresh);	
							
						},
				   }	
		);
		//Ajax
		
	}
	//IF
	
	
}

/*
*
* FX: tr_display_running: GET Reservations running:AJAX GET
* @params: rows_per_page,page
*  
* 
*/
function tr_display_running($inrows,$inpage,$in_url)
{

	var $div_id_bid = "#search_bkx_bookingid";
	var $bookingid = $($div_id_bid).val();
	var $div_id_guest = "#search_bkx_customerid";
	var $customerid = $($div_id_guest).val();

	var $page =  $inpage  || 1;
	var $rows =  $inrows  || 10;

	var $div_tm = "#txr";
   $($div_tm).empty();
   
	bgx_current_display($page);

	var $in_date = g_choosen_date();   
	$($div_tm).append($in_date);

	//Search
	var $bookingid;
	var $customerid;

		
   var $h_sx = {indate:$in_date,bookingid:$bookingid,customerid:$customerid};
	
	var $fn_retry   = 'tr_display_reservations('+$inrows+',' + $inpage + ',' + $in_url + ')';
	var $div_id_msg = "Please wait while the reservations are being fetched....";

	progress_append($div_tm,$div_id_msg,$fn_retry);	
	
	var $base_url = '/reservediner/book/running';
	//$($div_tm).append($base_url);
		
	var $url;
	if($in_url)
	{
		$url = $in_url;
	}
	else if($page && $rows)
	{
		$url = $base_url + "/" + $page + "/" +$rows;
	}	
	//$($div_tm).append($url);
	
	if($base_url)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
						headers: 	$h_sx,
						statusCode: 
						{
    								401: function() 
    								{
										$('#txr').append("Permission Error.");    									
    								},
    					},
						statusCode: 
						{
    								404: function() 
    								{
			 								var $me = "<div class='card-panel '>"
			 									+ "<span class='red-text'>No Reservations for "
 												+ $in_date + ", page:" + $page + ".</span></div>";
   										$($div_tm).empty(); 										
											$('#txr').append($me);
	    							},
    					},		
 						error: function (xhr, status, error) 
 						{
 									var $me = "<span class='red-text'>Error."
 											+ "</span>";
   								$($div_tm).empty(); 										
									$('#txr').append($me);
            		},
						success: function( data )
						{
							$($div_tm).empty();
		  					tr_get_reservations(data);
		  					g_choosen_date(data.date);		  					
						},
				}	
		);
	}
	
};


/*
* FX: tr_display_reservations: GET Reservations AJAX GET 
* @params: rows_per_page,page
*   
*
* 
*/
function tr_display_reservations($inrows,$inpage,$in_url)
{

	var $div_id_bid = "#search_bkx_bookingid";
	var $bookingid = $($div_id_bid).val();
	var $div_id_guest = "#search_bkx_customerid";
	var $customerid = $($div_id_guest).val();

	var $page =  $inpage  || 1;
	var $rows =  $inrows  || 10;

	var $div_tm = "#txr";
   $($div_tm).empty();
   
	bgx_current_display($page);

	var $in_date = g_choosen_date();   
	$($div_tm).append($in_date);

	//Search
	var $bookingid;
	var $customerid;

		
   var $h_sx = {indate:$in_date,bookingid:$bookingid,customerid:$customerid};
	
	var $fn_retry   = 'tr_display_reservations('+$inrows+',' + $inpage + ',' + $in_url + ')';
	var $div_id_msg = "Please wait while the reservations are being fetched....";

	progress_append($div_tm,$div_id_msg,$fn_retry);	
	
	var $base_url = '/reservediner/book/list';
	//$($div_tm).append($base_url);
		
	var $url;
	if($in_url)
	{
		$url = $in_url;
	}
	else if($page && $rows)
	{
		$url = $base_url + "/" + $page + "/" +$rows;
	}	
	//$($div_tm).append($url);
	
	if($base_url)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
						headers: 	$h_sx,
						statusCode: 
						{
    								401: function() 
    								{
										$('#txr').append("You have been logged out.");    									
    								},
    					},
						statusCode: 
						{
    								404: function() 
    								{
			 								var $me = "<div class='card-panel '>"
			 									+ "<span class='red-text'>No Reservations for "
 												+ $in_date + ", page:" + $page + ".</span></div>";
   										$($div_tm).empty(); 										
											$('#txr').append($me);
	    							},
    					},		
 						error: function (xhr, status, error) 
 						{
 									var $me = "<span class='red-text'>Error."
 											+ "</span>";
   								$($div_tm).empty(); 										
									$('#txr').append($me);
            		},
						success: function( data )
						{
							$($div_tm).empty();
		  					tr_get_reservations(data);
						},
				}	
		);
	}
	
};



var $mst_reserve_type_status = 
							"<select name='new_status' class='browser-default'>"
									+	"<option value=''>Change Status</option>"
									+"{{#.}}"
										+	"<option value='{{type}}' >{{name}}</option>"
									+"{{/.}}"
							+ "</select>" ;					

var $str_form_search_bookings_pending = "<li>" 
				+ "<div class='collapsible-header waves-effect waves-purple black-text '>"
					+ "Search"
				+ "</div>"
				+ "<div class='collapsible-body black-text yellow lighten-5'>"				
								+"<div class='row  id='staff_booking_search' >"
									+"<div class='col l4 m4 s6 input-field'> "
										+ "<input id='search_bkx_bookingid' type='text' "
												+ " name='search_bookingid' "
												+ " >"
										+ "<label for='search_bkx_bookingid'>Booking ID</label>"		
									+"</div>"
									+"<div class='col l4 m4 s6  input-field'> "
										+ "<input id='search_bkx_customerid' type='text' "
												+ "  >"
												+ "<label for='search_bkx_customerid'>Customer</label>"												
									+"</div>"
									+"<div class='col l3 m3 s12'> "
													+ "<span class='btn green' onClick=tr_display_reservations(); > "
															+ "Search </span>"
									+"</div>"									
								+ "</div>"
					+"</div>"	
		+ "</li>"	
	;


/*
* FX: tr_get_reservations: 
* Use the Array of Hash (Reservations)
* Params: Hash
*
*/ 
function tr_get_reservations(xdata)
{
	  var items 			 = [];

	  var $in_date = xdata.date;
	  var $count = 0;

	  var $div_add = "#txr";
	  items.push($str_form_search_bookings_pending);		  
	  //$($div_add).append($str_form_search_bookings_pending);

	  var $str_allsts = Mustache.to_html($mst_reserve_type_status,xdata.status_types );
	  
	  $.each( xdata.items, function( key, val ) 
	  {
	  	  $count++;
		  var $mx_div_xbc = tr_div_booked(val,$count,$str_allsts);
		  items.push($mx_div_xbc);	
  	  }) ;


  $( "<ul/>", 
  {
    "class": "collapsible popout panel",
    html: items.join( "" )
  }).appendTo( $div_add ); 

  $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : false 
    });
  });
      
	
}

/*
* FX: tr_Div_booked
* Display a Reservations
* Hash of Reservation,Count,String for DropDown of StatusTypes
* 
*/

function tr_div_booked(val,count,$str_allsts)
{

  	var $alt_color = $zebra_b;
  	if(count % 2 == 0)
  	{
  		$alt_color = $zebra_a;
  	}
  	
  	var $check_timings = '';
  	if(val.is_in_timings ==0 )
  	{
  			$check_timings = "icon_flag red-text";
  	}
	
	if(val.allowed_statuses)
	{
			//Materialize.toast("Allowed",3000);
			$str_allsts = Mustache.to_html($mst_reserve_type_status,val.allowed_statuses );
	}
	
	var $fn_click;		
	$fn_click = " onClick=tr_edit_reservation('{{bookingid}}');" ; 	

	var $mx_div_bform = "<div class='collapsible-body white'>"
			+ "<div class='row card green lighten-4' id='eform_{{bookingid}}' >"
				+ 		"<div class='col l12 m12 s12'>Edit Reservation for Booking: {{bookingid}}"
							+ "</div>"
				+ 	"<div class='col l12 m12 s12'>"
				+ 			"Name  <input name=aname type='text' required "
							+ "value='{{ name }}' " +" /></div>"
				+ 	"<div class='col l12 m12 s12'>Email <input name='email' " 
							+		"value='{{ customerid }}' " + " type='text' /></div>"					
				+ 		"<div class='col l12 m12 s12'>Phone<input name='phone' " 
							+ "value='{{phone}}' " + " type='text' /></div>"
				+ "<div class='col l6 m6 s6'>Seats<input name='pax' min=0 type='number' "
							+" value='{{pax}}' required /></div>"
				+ "<div class='col l6 m6 s6'>Table:<input name='tablenumber' min=0 type='number' "
							+" value='{{tablenumber}}' required /></div>"
				+ "<div class='col l12 m12 s12'>Comments<input name='comments' type='text' "
							+" value='{{comments}}' required /></div>"
				+ "<div class='col l12 m12 s12 card-action green lighten-5'>Current Status: {{str_status}}"
							+" &nbsp; "
							+ $str_allsts							
						+ "</div>"
				+ "<div class='col l12 m12 s12 center'><button class='btn orange' " 
									+ $fn_click 
									+ ">Edit Reservation</button></div>"											
				+ 		"<span class='col l12 m12 s12 xerrmsg' ></span>"												
			+ "</div>"
			+ "</div>";

var $mx_div_booking = "<li>" 
	+ "<div class='collapsible-header waves-effect waves-purple " 
						+ "black-text "+ $alt_color +"' >"
							
			+ "<div class='row '> "
				+ "<div class='col    l1 m1 s6 red-text center tag '> &#10095; </div>"				
					+ "<div class='col l4 m5 s6'>	{{ bookingid }}  "
							+ "{{#tablenumber}} (T-{{ tablenumber }}) {{/tablenumber}} </div>"							
					+ "<div class='col l6 m5 s12 "+ $check_timings +" left '>	{{ booking_start }}  </div>"
						
					+ "<div class='col l6 m5 s12 left'><b>{{ customerid }}</b></div>"								 	
					+ "<div class='col l4 m5 s9 left'>"
								+ "{{#str_status}}"
									+"{{str_status}}"
								+"{{/str_status}}"
								+"{{^str_status}}"
									+"Status Missing"
								+"{{/str_status}}"
								+"</div>"

					+ "<div class='col l1 m1 s1 '>"
							+ "<span class='btn-floating red center'> &rarr; </span></div>"

				+ "</div>"
											
				+"</div>"
				+ $mx_div_bform
			+ "<li>";
	  	
	var $div_s = Mustache.to_html($mx_div_booking, val);
	return $div_s;
}

/*
* FX: tr_edit_reservation: Edit a Reservations
* @Params: bookingid
*/

function tr_edit_reservation($bookingid)
{
	var $form_div = "#eform_" + $bookingid;
		
	var $aname 		= $($form_div+" :input[name='aname']").val();
	var $email 		= $($form_div+" :input[name='email']").val();
	var $pax 		= $($form_div+" :input[name='pax']").val();
	var $comments	= $($form_div+" :input[name='comments']").val();	
	var $phone 		= $($form_div+" :input[name='phone']").val();	
	var $div_derry = $($form_div).find(".xerrmsg");

	var $tablenumber 		= $($form_div+" :input[name='tablenumber']").val();	
	var $new_status 		= $($form_div+" :input[name='new_status']").val();

	var $current_page = $("#api_page_current").val() || 1;		
		
	//$("#xyz").append($aname);
	//$($div_derry).append($email);
	var $url = "/reservediner/book/"+$bookingid;
	
	var $h_out = 	{  
											'email':$email, 
											'phone':$phone,
											'name': $aname,
											'tablenumber': $tablenumber, 		
											'pax': $pax,
											'comments': $comments,		
											'new_status':$new_status,									 
										};
	
	
	
	if($email)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"PUT",
						dataType: 	"json",
						data: 	$h_out,
 						error: function (xhr, status, error) 
 						{
							var $jr = JSON.parse(xhr.responseText);
						 	var $error_msg = $jr.error;
 							var $me = "<span class='red-text'>No Update.  " 
 								+ $error_msg + "</span>";
 							$($div_derry).append($me);
 						
            		},
						success: function( data )
						{
							Materialize.toast('Updated Reservation.', $trv_seconds_refresh)
							$('#zyz').append("Edited.");
							window.setTimeout(function(){         
											tr_display_reservations(10,$current_page);     
                  			}, $trv_seconds_refresh);	
						},
				   }	
		);
		//Ajax
		
	}
	//IF
	
}

/**************************************/
/*
* FX: tr_display_bookings_end: GET Reservations AJAX GET
* @params: rows_per_page,page
*  
*
* 
*/
function tr_display_bookings_end($inrows,$inpage,$in_url)
{

	var $page =  $inpage  || 1;
	var $rows =  $inrows  || 10;

	var $div_tm = "#txr";
   $($div_tm).empty();

	bgx_current_display($page);

	var $in_date = g_choosen_date();   
	$($div_tm).append($in_date);

	var $fn_retry   = 'tr_display_reservations('+$inrows+',' + $inpage + ',' + $in_url + ')';
	var $div_id_msg = "Please wait while the reservations are being fetched....";
	progress_append($div_tm,$div_id_msg,$fn_retry);	
	
	var $base_url = '/reservediner/book/complete';
	//$($div_tm).append($base_url);
		
	var $url;
	if($in_url)
	{
		$url = $in_url;
	}
	else if($page && $rows)
	{
		$url = $base_url + "/" + $page;
	}	
	//$($div_tm).append($url);
	
	if($base_url)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"GET",
						dataType: 	"json",
						headers: 	{  'indate':$in_date, 
										},
						statusCode: 
						{
    								401: function() 
    								{
										$('#txr').append("You have been logged out.");    									
    								},
    					},
						statusCode: 
						{
    								404: function() 
    								{
			 								var $me = "<div class='card-panel '>"
			 									+ "<span class='red-text'>No Reservations for "
 												+ $in_date + ", page:" + $page + ".</span></div>";
   										$($div_tm).empty(); 										
											$('#txr').append($me);
	    							},
    					},		
 						error: function (xhr, status, error) 
 						{
 									var $me = "<span class='red-text'>Error."
 											+ "</span>";
   								$($div_tm).empty(); 										
									$('#txr').append($me);
            		},
						success: function( xdata )
						{
							$($div_tm).empty();
		  					tr_get_booking_end(xdata);
						},
				}	
		);
	}
	
};

/*
* FX: tr_get_booking_end
* Use the Array of Hash (Reservations)
* Params: Hash
*
*/ 
function tr_get_booking_end(xdata)
{
	  var items 			 = [];

	  var $in_date = xdata.date;
	  var $count = 0;

	  $.each( xdata.items, function( key, val ) 
	  {
	  	  $count++;
		  var $mx_div_xbc = tr_div_booked_end(val,$count);
		  items.push($mx_div_xbc);	
  	  }) ;


  $( "<ul/>", 
  {
    "class": "collapsible popout panel",
    html: items.join( "" )
  }).appendTo( "#txr" ); 

  $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : false 
    });
  });
      
	
}

/*
* FX: tr_div_booked_end: Display a Reservations
* Hash of Reservation,Count,String for DropDown of StatusTypes
* 
*/

function tr_div_booked_end(val,count)
{

  	var $alt_color = $zebra_b;
  	if(count % 2 == 0)
  	{
  		$alt_color = $zebra_a;
  	}

	var $mx_div_bexp = "<div class='collapsible-body white '>"
			+ "<div class='row card  black-text ' >"
				+ 		"<div class='col l12 m12 s12'>Booking ID: {{bookingid}}"
							+ "</div>"
				+ 	"<div class='col l12 m12 s12 '>"
				+ 			"Name  <input disabled name=aname type='text' "
							+ "value='{{ name }}' " +" /></div>"
				+ 	"<div class='col l12 m12 s12'>Email <input name='email' " 
							+		"value='{{ customerid }}' " + " type='text' /></div>"					
				+ 		"<div class='col l12 m12 s12'>Phone<input  disabled name='phone' " 
							+ "value='{{phone}}' " + " type='text' /></div>"
				+ "<div class='col l6 m6 s6'>Seats<input  disabled name='pax'  "
							+" value='{{pax}}' required /></div>"
				+ "<div class='col l6 m6 s6'>Table:<input  disabled  name='tablenumber' "
							+" value='{{tablenumber}}' required /></div>"
				+ "<div class='col l12 m12 s12'>Comments<input  disabled  name='comments' type='text' "
							+" value='{{comments}}' required /></div>"
				+ 		"<span class='col l12 m12 s12 xerrmsg' ></span>"												
			+ "</div>"
			+ "</div>";


var $mx_div_booking =
	"<li>" 
				+ "<div class='collapsible-header waves-effect waves-purple " 
						+ "black-text "+ $alt_color +"' >"	
				+ "<div class='row '> "
				+ "<div class='col    l1 m4 s6 red-text center tag '> &#10095; </div>"				
					+ "<div class='col l1 m8 s6'>	{{ bookingid }}  </div>"	
					+ "<div class='col l3 m8 s12'>	{{ booking_start }}  </div>"	
					+ "<div class='col l3 m6 s6 center'><b>{{ customerid }}</b></div>"								 	
					+ "<div class='col l2 m6 s6 center'>"
								+ "{{#str_status}}"
									+"{{str_status}}"
								+"{{/str_status}}"
								+"{{^str_status}}"
									+"Status Missing"
								+"{{/str_status}}"
								+"</div>"
					+ "</div></div>"
						+ $mx_div_bexp
			+ "<li>";
	  	
	var $div_s = Mustache.to_html($mx_div_booking, val);
	return $div_s;
}

/*******Start Branch Timings**********/

/*
*  FX: trg_display_timings: Display Timings for the Day for a Branch
*
* Handles Pagination throug Data and Moustache
*
*/
function trg_display_timings($incount,$inpage,$in_url)
{

	var $page =  1;
	var $rows =  30;

	var $in_start_date 	= $("#in_from_date").val();
	var $choosen_date 	= g_choosen_date();
	var $select_date     = $("#select_date").val();
	
	var $in_date  			= $select_date || $in_start_date || $choosen_date  ;
	var $branchid 			= bgx_get_branchid();
	//Materialize.toast($in_date,4000);

   		
	var $div_tm = "#txr";
	var $div_tm_msg = "#txr_msg";
   //$($div_tm).empty();
   $($div_tm_msg).empty();

	var $div_id_msg = "Please wait for the timings....";
	var $base_url = '/biz/timings' + "/" + $branchid + "/" + $in_date;
	
	var $fail_msg = "Timings are not available for " + $in_date + "."; 

	if($in_url)
	{
		$base_url = $in_url;
		$in_date = "";
	}	

	progress_append($div_tm_msg);

	if($in_url || ($branchid && $in_date) )
	{		
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"GET",
						dataType: 	"json",
						headers:
						{ 
							 'indate':$in_date, 
						},
 						error: function (xhr, status, error) 
 						{
 								bx_progress_empty($div_tm_msg);
								$($div_tm_msg).append($fail_msg);
            		},
						success: function( xdata )
						{
							bx_progress_empty($div_tm_msg);
							$($div_tm).empty();
							
							trg_get_timex(xdata);
		  					g_choosen_date(xdata.date);
		  					
						},
				}	
		);
	}
	
	
};

/*
* FX: trg_get_timex: Timings for Guest
*
* 
*/
function trg_get_timex(xdata,$rows,$page)
{
	  var items 			 = [];

		tra_info_timex_close();

	  var $div_pgx = Mustache.to_html($mst_time_pgx, xdata);
	  items.push($div_pgx);	
	  
	  var $count = 0;	  
	  $.each( xdata.timings, function( key, val ) 
	  {
	  	    $count++;
	  	    val.is_staff = xdata.is_staff;//To handles edits/deletes
			 var $x_item = Mustache.to_html($mst_atimex, val);	  	    
	  		 items.push($x_item);
  	  }) 


  $( "<ul/>", 
  {
    "class": "",
    html: items.join( "" )
  }).appendTo( "#txr" ); 

  

  $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : false 
    });
  });
        

  g_timepicker_init();        
	
}
/* Branch Timings for Admin */
/* this has Pagination through data.next and data.previous */
var $mst_time_pgx = 	"<div class='row '>"

						+ "<div class='col l12 m12 s12 card-title center-align'>"
							   	+ "<input name='timing_type' value='{{timing_type}}' type='hidden' id='timing_type' />"
								+ "{{weekday}}, {{loc_date}}, "
													+ "<input id='timex_day_of_week' type='hidden' value='{{day_of_week}}' />"
													+ "<input id='timex_type' type='hidden' value='{{timing_type}}' />"								
													+ "<input id='timex_day_name' type='hidden' value='{{weekday}}' />"								
								+ "{{#is_closed}}"
													+ "<input id='timex_is_closed' type='hidden' value='1' />"
											+ "<span class='red-text'>Closed  </span>"								
													+ "{{#is_staff}}"
															+ "<span class='red-text btn-floating red white-text' "
															+ "onClick=tra_info_timex('{{date}}',1); > &rarr; </span>"
													+ "{{/is_staff}}"											
									+ "{{/is_closed}}"
									+ "{{^is_closed}}"
											+ "<span class='green-text'>Open</span>"								
									+ "{{/is_closed}}"
							+ "</div>"
										
									
						+ "<div class='row '>"
							+ "<span class='btn col l4 m4 s3 yellow lighten-4 white black-text' "
									+ 	"onClick=trg_display_timings('1','1','{{previous}}');" 	 
									+ "> "
									+ "<< </span>"
							+ "<span class='btn col l4 m4 s6 white black-text'>{{date}}</span>"
							+ "<span class='btn col l4 m4 s3 yellow lighten-4 black-text' "
									+ " onClick=trg_display_timings('1','1','{{next}}'); " 	 
									+ " > "
									+ ">> </span>"
  						+ "</div>"
 						+ "<div id='edittimer_c'></div>"
	  				;


var $mst_atimex = "<div class='row collection-item'>"
								+ "<div class='col s6'>{{open}} to {{close}}</div>"
								+ "<div class='col s4'>{{name}}</div>"
								+ "{{#is_staff}}"
								+ "<div class='col s2'>"
									+"<span class='btn-floating center red' " 
											+ " onClick=tra_info_timex"
											+"('{{date}}',0,'{{open}}','{{close}}'); >"
											+"&rarr;</span>"
								+ "</div>"
								+ "{{/is_staff}}"
							+ "</div>";	


/*******End Branch Timings**********/


/******************* DELETE fn and form & ADD fn and forms Start ******/


/**** Begin: Add Timing  ****/
/*
* FX: tr_admin_Collect_time
* 
* collect Data for Adding DT,DTDay,DTHoliday,DTWeekDay
* 
*/
function tr_admin_collect_time($branchid,$form_div)
{
		
	   //Dates:from,till,select
		//var $from_date 		= $("#from_date").val();
		//var $till_date 		= $("#till_date").val();
		var $select_date		= $($form_div+" :input[name='open_date']").val();
		
		//Time: Open,Close,BookingPeriod			
		var $open				= $($form_div+" :input[name='begin_time']").val();
		var $close				= $($form_div+" :input[name='end_time']").val();		
		var $booking_period	= $($form_div+" :input[name='booking_period']").val();
		var $name				= $($form_div+" :input[name='timing_name']").val();		
		
		//Number: 1-7
		var $day_of_week					= $($form_div+" :input[name='day_of_week']").val();
		var $timing_wd_closed			= $($form_div+" :input[name='timing_wd_closed']").val();
		
		var $h_new = {
							name:					$name,
			
							open_date: 		$select_date,
							opening_time:		$open,
							closing_time:		$close,
							booking_period:	$booking_period,

							branchid:			$branchid,

							day_of_week:		$day_of_week,
							is_closed:			$timing_wd_closed,
							
						 };
						 		
		return $h_new;				
}
/*
* FX: tr_add_timing: Adds A Timing for a Branch.
*
*/
function tr_add_timing()
{
		var $form_div	= "#timings_add";
		var $branchid	= bgx_get_branchid();
		var $h_new		= 	tr_admin_collect_time($branchid,$form_div);

		var $from_date 		= $("#in_from_date").val();
		var $till_date 		= $("#in_till_date").val();

		if($branchid && $from_date && $till_date )
		{

				var $base_url = '/corp/timing';
				var $url = $base_url + "/" +$branchid+"/"+$from_date+"/"+$till_date;
				
				$.ajax( 
							{ 
								url: 			$url ,
								type: 		"POST",
								data: 		$h_new,     					
								dataType: 	"json",
								statusCode: {
		    								404: function() {
											$('#txr').append("You have been logged out.");    									
		    							},
		    					},		
		 						error: function (xhr,status,error) 
		 						{
		 								var $jr = JSON.parse(xhr.responseText);
		 							 	var $error_msg = $jr.error_msg;
		 							   var $me_err = "<span class='red-text'>Timing cannot be added." 
		 										+ $error_msg + "</span>";
										Materialize.toast($me_err,2000);
		            		},
								success: function( data )
								{
									var $msg = "Timing Added.Refreshing Calendar.";
									Materialize.toast($msg,2000);		 
									trg_display_timings(); 		
									txa_add_timers();												
								},
						}	
				);

			
		}
		
	
}

/*
* FX: tr_Add_timing_weekday: Adds A Timing Weekday for a Branch.
*
*/
function tr_add_timing_weekday()
{
		var $form_div	= "#timings_weekday_add";
		var $branchid	= bgx_get_branchid();
		var $h_new		= tr_admin_collect_time($branchid,$form_div);

		var $from_date 		= $("#in_from_date").val();
		var $till_date 		= $("#in_till_date").val();

		if($branchid && $from_date && $till_date )
		{

				var $base_url = '/corp/timing/weekday';
				var $url = $base_url + "/" +$branchid+"/"+$from_date+"/"+$till_date;
				
				$.ajax( 
							{ 
								url: 			$url ,
								type: 		"POST",
								data: 		$h_new,     					
								dataType: 	"json",
								statusCode: {
		    								404: function() {
											$('#txr').append("You have been logged out.");    									
		    							},
		    					},		
		 						error: function (xhr,status,error) 
		 						{
		 								var $jr = JSON.parse(xhr.responseText);
		 							 	var $error_msg = $jr.error_msg;
		 							   var $me_err = "<span class='red-text'>Weekday Timing cannot be added." 
		 										+ $error_msg + "</span>";
										Materialize.toast($me_err,2000);
		            		},
								success: function( data )
								{
									var $msg = "Timing for weekday added. Refreshing calendar.";
									Materialize.toast($msg,2000);		 
									trg_display_timings(); 	
									txa_add_timers();				
								},
						}	
				);

			
		}
		
	
}

/*
* FX: tr_add_timing_holiday: Adds A Holiday for a Branch.
*
*/
function tr_add_timing_holiday()
{
		var $form_div	= "#timings_holiday_add";
		var $branchid	= bgx_get_branchid();
		var $h_new		= tr_admin_collect_time($branchid,$form_div);

		var $from_date 		= $("#in_from_date").val();
		var $till_date 		= $("#in_till_date").val();

		if($branchid && $from_date && $till_date )
		{

				var $base_url = '/corp/timing/holiday';
				var $url = $base_url + "/" +$branchid+"/"+$from_date+"/"+$till_date;
				
				$.ajax( 
							{ 
								url: 			$url ,
								type: 		"POST",
								data: 		$h_new,     					
								dataType: 	"json",
								statusCode: {
		    								404: function() {
											$('#txr').append("You have been logged out.");    									
		    							},
		    					},		
		 						error: function (xhr,status,error) 
		 						{
		 								var $jr = JSON.parse(xhr.responseText);
		 							 	var $error_msg = $jr.error_msg;
		 							   var $me_err = "<span class='red-text'>Holiday cannot be added." 
		 										+ $error_msg + "</span>";
										Materialize.toast($me_err,2000);
		            		},
								success: function( data )
								{
									var $msg = "Holiday Added .Refreshing calendar.";
									Materialize.toast($msg,2000);		 
									trg_display_timings(); 	
									txa_add_timers();				
								},
						}	
				);

			
		}
		
	
}

/*
* FX: tr_add_timing_specialday
* Adds A Timing Special for a Branch.
*
*/
function tr_add_timing_specialday()
{
		var $form_div	= "#timings_specialday_add";
		var $branchid	= bgx_get_branchid();
		var $h_new		= 	tr_admin_collect_time($branchid,$form_div);

		var $from_date 		= $("#in_from_date").val();
		var $till_date 		= $("#in_till_date").val();

		if($branchid && $from_date && $till_date )
		{

				var $base_url = '/corp/timing/specialday';
				var $url = $base_url + "/" +$branchid+"/"+$from_date+"/"+$till_date;
				
				$.ajax( 
							{ 
								url: 			$url ,
								type: 		"POST",
								data: 		$h_new,     					
								dataType: 	"json",
								statusCode: {
		    								404: function() {
											$('#txr').append("You have been logged out.");    									
		    							},
		    					},		
		 						error: function (xhr,status,error) 
		 						{
		 								var $jr = JSON.parse(xhr.responseText);
		 							 	var $error_msg = $jr.error_msg;
		 							   var $me_err = "<span class='red-text'>Specialday Timing cannot be added." 
		 										+ $error_msg + "</span>";
										Materialize.toast($me_err,2000);
		            		},
								success: function( data )
								{
									var $msg = "Timing for specialday added. Refreshing calendar.";
									Materialize.toast($msg,2000);		 
									trg_display_timings(); 	
									txa_add_timers();				
								},
						}	
				);

			
		}
		
	
}


/**** END: Add Timing  ****/


/*
* FX: tra_info_timex_close: Closes The Timex Info
* Empties the edittime_c from the page.
* 
*/
function tra_info_timex_close()
{
	var $div_add;
	$div_add = "#edittimer_c";
   $($div_add).empty();

}

/*
* FX: tra_del_dinertime: deletes Regular Time
*
*/
function tra_del_dinertime($h_timex)
{
	var $opening_time = $h_timex.open;
	var $closing_time = $h_timex.close;
	var $in_date		= $h_timex.date;
	var $in_msg		   = $h_timex.message; 
	var $branchid = bgx_get_branchid();
	var $base_url;

	var $from_date 		= $("#in_from_date").val();
	var $till_date 		= $("#in_till_date").val();

	var $slash = "/";
	var $base_url = '/corp/timing';
	var $url =  $base_url + $slash 
					+$branchid + $slash 
					+ $from_date + $slash + $till_date + $slash 
					+ $opening_time + $slash + $closing_time;
	var $method_type = 'DELETE';				


	if($from_date && $till_date && $opening_time && $closing_time && $in_msg && $branchid )
	{		
		var $x_type = 'Timing';
		tra_timex_del_base($x_type,$url,$h_timex);	
	}
	
		
}

/*
* FX: tra_del_weekdaytime: deletes Weekday Time
*
*/
function tra_del_weekdaytime($h_timex)
{
	var $opening_time = $h_timex.open || "00:00";
	var $closing_time = $h_timex.close || "00:00";
	var $in_date		= $h_timex.date;
	var $in_msg		   = $h_timex.message; 
	var $day_of_week	= $h_timex.day_of_week;
	
	var $branchid = bgx_get_branchid();
	var $base_url;

	var $from_date 		= $("#in_from_date").val();
	var $till_date 		= $("#in_till_date").val();

	var $slash = "/";
	var $base_url = '/corp/timing/weekday';
	var $url =  $base_url + $slash 
					+ $branchid + $slash 
					+ $from_date + $slash + $till_date + $slash 
					+ $opening_time + $slash + $closing_time + $slash
					+ $day_of_week;
	var $method_type = 'DELETE';				

	if($from_date && $till_date && $opening_time && $closing_time && $in_msg && $branchid && $day_of_week )
	{		
		var $x_type = 'Weekday timing';
		tra_timex_del_base($x_type,$url,$h_timex);	
	}
	
		
}


/*
* tra_del_specialdaytime: deletes Weekday Time
*
*/
function tra_del_specialdaytime($h_timex)
{
	var $opening_time = $h_timex.open;
	var $closing_time = $h_timex.close;
	var $in_date		= $h_timex.date;
	var $in_msg		   = $h_timex.message; 
	
	var $branchid = bgx_get_branchid();
	var $base_url;

	var $from_date 		= $("#in_from_date").val();
	var $till_date 		= $("#in_till_date").val();

	var $slash = "/";
	var $base_url = '/corp/timing/specialday';
	var $url =  $base_url + $slash 
					+ $branchid + $slash 
					+ $from_date + $slash + $till_date + $slash 
					+ $opening_time + $slash + $closing_time + $slash
					+ $in_date;
	var $method_type = 'DELETE';				

	if($opening_time && $closing_time && $in_date && $in_msg && $branchid)
	{		
		var $x_type = 'Special day timing';
		tra_timex_del_base($x_type,$url,$h_timex);	
	}
	
		
}



/*
* FX: tra_del_holiday: deletes Holiday
*
*/
function tra_del_holiday($h_timex)
{
	var $in_date		= $h_timex.date;
	var $in_msg		   = $h_timex.message; 
	
	var $branchid = bgx_get_branchid();
	var $base_url;

	var $from_date 		= $("#in_from_date").val();
	var $till_date 		= $("#in_till_date").val();

	var $slash = "/";
	var $base_url = '/corp/timing/holiday';
	var $url =  $base_url + $slash 
					+ $branchid + $slash 
					+ $from_date + $slash + $till_date + $slash 
					+ $in_date;
	var $method_type = 'DELETE';				

	if($from_date && $till_date && $in_date && $in_msg && $branchid )
	{		
		var $x_type = 'Holiday';
		tra_timex_del_base($x_type,$url,$h_timex);	
	}
	
		
}

/*
* FX: tra_timex_del_base: This fn is used for deleting all timex.
*
* @params: type,url,h_timex
*/
function tra_timex_del_base($x_type,$in_url,$h_timex)
{

	var $method_type = 'DELETE';
	
	if($in_url)
	{
		$.ajax( 
					{ 
						url: 			$in_url ,
						type: 		$method_type,
						dataType: 	"json",
 						error: function (xhr, status, error) 
 						{
 								var $me = "<span class='red-text'>No Changes."
 										+ "</span>";
            		},
						success: function( xdata )
						{
								var $msg_del = $x_type + "deleted";
								Materialize.toast($msg_del,1000);
								tra_info_timex_close();//Close the Times Section
								trg_display_timings();
								
						},
				}	
		);
	}

	
}

/*
* FX: tra_timex_delete: Delete The Timex Info
*
* @params: date,is_closed,open,close
*/
function tra_timex_delete($in_date,$is_closed,$in_open,$in_close)
{
   $in_msg = $("#timex_msg_delete").val();
   if(!$in_msg)
   {
   		var $err_msg = "Reason is required." 
		  	Materialize.toast($err_msg,3000);
		  	return;
	}	


  $in_day_num		= $("#timex_day_of_week").val();
  $in_day_name		= $("#timex_day_name").val();
  $is_closed		= $("#timex_is_closed").val() || 0;
  $in_timing_type = $("#timex_type").val();  
  
  var $h_timex = {
  							date:  				$in_date,
  							open:  				$in_open,
  							close: 				$in_close,	
  							day_of_week: 		$in_day_num,
  							day_name: 			$in_day_name,
  							is_closed:			$is_closed,
  							message:				$in_msg,
  						};

	if($in_timing_type == 'DINER')
	{
		tra_del_dinertime($h_timex);
	}
	else if	($in_timing_type == 'WEEKDAY')
	{
		tra_del_weekdaytime($h_timex);
	}
	else if	($in_timing_type == 'DAY' && $is_closed > 0)
	{
		tra_del_holiday($h_timex);
	}
	else if	($in_timing_type == 'DAY')
	{
		tra_del_specialdaytime($h_timex);
	}
  						
  	return;					
  	
}


/*
* FX: tra_info_timex
* Info a Timing.
* @params: date,is_closed,open,close
* Handles Div. Creates a Section for the Timing.
*   
*/
function tra_info_timex($in_date,$is_closed,$in_open,$in_close)
{

	var $div_add;
	$div_add = "#edittimer_c";
   $($div_add).empty();

  $in_day_num		= $("#timex_day_of_week").val();
  $in_day_name		= $("#timex_day_name").val();
  $is_closed		= $("#timex_is_closed").val() || 0;
  $in_timing_type = $("#timex_type").val();  
  
  
  var $h_timex = {
  							date:  				$in_date,
  							open:  				$in_open,
  							close: 				$in_close,	
  							day_of_week: 		$in_day_num,
  							day_name: 			$in_day_name,
  							is_closed:			$is_closed,
  						};

	var $div_atx;
	if($in_timing_type == 'DINER')
	{
		$div_atx = Mustache.to_html($mst_timex_edit,$h_timex);
	}
	else if	($in_timing_type == 'WEEKDAY')
	{
		$div_atx = Mustache.to_html($mst_timex_edit_weekday,$h_timex);
	}
	else if	($in_timing_type == 'DAY' && $is_closed > 0)
	{
		$div_atx = Mustache.to_html($mst_timex_edit_holiday,$h_timex);
	}
	else if	($in_timing_type == 'DAY')
	{
		$div_atx = Mustache.to_html($mst_timex_edit_day,$h_timex);
	}
	else
	{
		var $msg = "Use Edit Branch timings.";	
		Materialize.toast($msg,3000);
	}
	
  	//Materialize.toast($div_add,5000);	
	$($div_add).append($div_atx);
	
}	  				

var $str_timex_sub_delete =	"<div class='col l12 m12 s12 right-align'>"
						 						+ "<input type='text' id='timex_msg_delete' placeholder='Reason for Deleting the time' name /> "
		 								+ "</div>"			  
			+"<div class='col l6 m6 s6 center'>"
			  					+ "<span class='btn red white-text' " 
		    					+ " onClick=tra_timex_delete('{{date}}','{{is_closed}}','{{open}}','{{close}}'); id='abc' > " 
								+ "Delete </span> "
					+"</div>"
			+	"<div class='col l6 m6 s6 right-align'>"
							+ "<span class='btn center green' onClick=tra_info_timex_close(); >X</span>"
			+ "</div>";

var $mst_timex_edit =  "<div class='card row yellow lighten-3'>"
							 +"<div class='col l12 m12 s12 center '>"
										    + " Timing {{#name}}, {{name}}{{/name}}: {{open}} to {{close}}"
						    		+ "</div>"
						    	+ $str_timex_sub_delete	
					 		 + "</div>";

var $mst_timex_edit_weekday = "<div class='card row yellow lighten-3'>"
										+	"<div class='col l12 m12 s12 center '>"
										    + "Timing {{day_name}} {{#name}}, {{name}}{{/name}} :"
										    + "  {{open}} to {{close}}"
						    				 + "</div>"
	  						    	   + $str_timex_sub_delete	
						    			+ "</div>";

var $mst_timex_edit_day =  "<div class='card row yellow lighten-3'>"
										 + "<div class='col l12 m12 s12 center '>"
										    + "Timing {{date}} :"
										    + "  {{open}} to {{close}}"
						    					+ "</div>"
	  						    	   + $str_timex_sub_delete	
						    		+ "</div>";

var $mst_timex_edit_holiday = "<div class='card row yellow lighten-3'>" 
										+ "<div class='col l12 m12 s12 center '>"
										    + " {{date}} {{#name}}, {{name}}{{/name}} : Closed"
						    			+ "  	</div>"
	  						    	   + $str_timex_sub_delete	
						    		+ "</div>";

	  				
/*
* FX: txa_add_timers: Admin Add Timings for a Branch Reservations
* Adds forms 
*/	  				
function txa_add_timers($xdata)
{
	var $div_add = "#addtimer_c";
	$($div_add).empty();

	tra_info_timex_close();//Empty edit form	
	
	//Empty Hash
	var $h_empty = {open:'',close:'',name:'',booking_period:'',date:'',day_of_week:''};
	
	var $div_addtx = Mustache.to_html($mst_add_timers,$h_empty);
	$($div_add).append($div_addtx);

	g_datepicker_init();	
	
}

/* Sub input for Timex edit Forms */
var $mst_timex_open_close=  "<div class='col l12 m12 s12 center input-field'>"
									    + "  		<input type='text' id='ta_begin_time' class='timepicker' name='begin_time' " 
									    + " />"
									    + "  			<label for='ta_begin_time' >Open</label>"
									    + "  	</div>"
									    + "<div class='col l12 m12 s12 center input-field'>"
									    + "  		<input type='text' id='ta_end_time' class='timepicker' name='end_time' "
									    + " />"
									    + "  			<label for='ta_end_time' >Close</label>"
									    + "  	</div>";

var $mst_timex_name	=  " <div class='col l12 m12 s12 center input-field'>"
									    + "  		<input type='text' id='ta_name' name='timing_name'	/>"
									    + "  			<label for='ta_name' >Name</label>"
									    + "  	</div>";
									    
var $mst_is_closed	=  " <div class='col l12 m12 s12 center input-field'>"
									    +	"			<select  name='timing_wd_closed' class='browser-default' >"
									    + "					<option value='0' >Open</option>"
									    + " 							<option value='1' >Closed</option>"
									    + "					</select>"
									    + "  	</div>"; 
									    
var $mst_timex_weekday = "<div class='col l12 m12 s12 center input-field'>"
									    +	"			<select  name='day_of_week' class='browser-default' >"
									    + "					<option value='' >Select Day</option>"
									    + " 							<option value='1' >Monday</option>"
									    + "							<option value='2' >Tuesday</option>"
									    + "							<option value='3' >Wednesday</option>"
									    + "							<option value='4' >Thursday</option>"
									    + "							<option value='5' >Friday</option>"
									    + "							<option value='6' >Saturday</option> "
									    + "							<option value='7' >Sunday</option>"
									    + "					</select>"
									    + "		</div>";		
var $mst_timex_booking_period = "<div class='col l12 m12 s12 center input-field'>"
									    + "  		<input type='text' id='ta_booking_period' name='booking_period'	/>"
									    + "  			<label for='ta_booking_period' >Booking Period (HH:MM)</label>"
									    + "  	</div>";

var $mst_timex_select_date			    = "<div class='col l12 m12 s12 center input-field'>"
									    + "  		<input type='text' id='ta_sd' Placeholder='yyyy--mm-dd' "
									    					+  " name='open_date' class='datepicker' 	/>"
									    + "  			<label for='ta_sd' >Date</label>"
									    + "  	</div>";
									    

/*
* Mustache Template for Adding Timings for Branch Reservations.
* 
*/
var $mst_add_timers = "<li> "
  	    			+ " <div class='collapsible-header  waves-effect btn lime lighten-3 black-text'>"
	   			+ " <div class='row'>"
               + "            <div class='col l1 m1 s1 left tag'>&#10095;</div>"
	   			+ "				<div class='col l10 m10 s10'> Add Timings (Slot) </div>"
	   			+ "		</div>"
	   			+ " 	    				</div>"
 	    			+ "<div class='collapsible-body yellow lighten-4' >"
							+ "<div class='card-panel' id='timings_add'>"
								 + "<div class='row'>"
									    + $mst_timex_name								 
									    + $mst_timex_open_close
									    + $mst_timex_booking_period									    
										 + "  <div class='col l12 m12 s12 center'>"
										 + "    				<button class='btn green' onClick='tr_add_timing();'>Save Timing (Slot)</button>"
										 + "      	</div>"
								 + "</div><!-- Row--> "
							+ "</div><!--Card-Panel -->"
 	    			+ "</div>"
			+ "</li>"
	 		+ "<li>" 
 	    			+ "<div class='collapsible-header  waves-effect btn lime lighten-3 black-text'>"
	   			+ "	<div class='row'>"
               + "            <div class='col l1 m1 s1 left tag'> &#10095;</div>"
	   			+ "				<div class='col l10 m10 s10'> Add Timings for a Weekday </div>"
	   			+ "		</div>"
 	    			+ "	</div>"
 	    			+ "<div class='collapsible-body yellow lighten-4' >" 	
							+ "<div class='card-panel' id='timings_weekday_add'>"
								 + "<div class='row'>"
									    + $mst_timex_name
									    + $mst_is_closed										    								 
									    + $mst_timex_open_close
									    + $mst_timex_booking_period									    
									    + $mst_timex_weekday
										 + "  <div class='col l12 m12 s12 center'>"
										 + "    				<button class='btn green' onClick='tr_add_timing_weekday();'>Save Weekday Timing</button>"
										 + "      	</div>"
								 + "</div><!-- Row--> "
							+ "</div><!--Card-Panel -->"					
 	    			+ "</div>"
			+ "</li>"
	 		+ "<li>" 
 	    			+ "<div class='collapsible-header  waves-effect btn lime lighten-3 black-text'>"
	   			+ "	<div class='row'>"
               + "           <div class='col l1 m1 s1 left tag'> &#10095;</div>"
	   			+ "				<div class='col l10 m10 s10'> Add Holiday </div>"
	   			+ "		</div>"
 	    			+ "	</div>"
 	    			+ "<div class='collapsible-body yellow lighten-4' >" 	
					+ "		"
							+ "<div class='card-panel' id='timings_holiday_add'>"
								 + "<div class='row'>"
								 		 + $mst_timex_select_date
									    + $mst_timex_name								 		 
										 + "  <div class='col l12 m12 s12 center'>"
										 + "    				<button class='btn green' onClick='tr_add_timing_holiday();'>Save Holiday</button>"
										 + "      	</div>"
								 + "</div><!-- Row--> "
							+ "</div><!--Card-Panel -->"								
					
 	    			+ "</div>"
			+ "</li>"
	 		+ "<li>" 
 	    			+ "<div class='collapsible-header  waves-effect btn lime lighten-3 black-text'>"
	   			+ "	<div class='row'>"
               + "            <div class='col l1 m1 s1 left tag'> &#10095;</div>"
	   			+ "				<div class='col l10 m10 s10'> Add Special Day </div>"
	   			+ "		</div>"
 	    			+ "	</div>"
 	    			+ "<div class='collapsible-body yellow lighten-4' >" 	
							+ "<div class='card-panel' id='timings_specialday_add'>"
								 + "<div class='row'>"
								 		 + $mst_timex_select_date
									    + $mst_timex_name								 		 
									    + $mst_timex_open_close
									    + $mst_timex_booking_period									    
										 + "  <div class='col l12 m12 s12 center'>"
										 + "    				<button class='btn green' onClick='tr_add_timing_specialday();'>Save Special Day Timing</button>"
										 + "      	</div>"
								 + "</div><!-- Row--> "
							+ "</div><!--Card-Panel -->"								
 	    			+ "</div>"
			+ "</li>"; 
	  				
/******************* DELETE fn and form & ADD fn and forms Start END ******/


/*******Begin Guest Bookings List**********/


/*
* FX: trg_display_slots: Display Time Slots for day for GuestUsers
*
* @params: Div for adding form
*/
function trg_display_slots($in_div)
{

	var $page =  1;
	var $rows =  30;

	var $in_date = g_choosen_date();
	
	var $div_tm = $in_div;
   $($div_tm).empty();
   
   var $userid = bgx_get_userid();
   if(!$userid)
   {
   		var $msg = "Please Log in to  make reservations";
   		//sbx_display_error(1,$msg);
   		Materialize.toast($msg,3000);
   }

	var $branchid = bgx_get_branchid();
	var $div_id_msg = "Please wait while the Timings are fetched....";
	var $base_url = '/g/reservediner/available';
	
	if($branchid)
	{
			$base_url = $base_url + "/" + $branchid + "/" + $in_date;
	} 

	progress_append($div_tm);

	if($base_url)
	{		
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"GET",
						dataType: 	"json",
						headers: 	{  'indate':$in_date, 
										},
						statusCode: 
						{
    								404: function() {
									$('#txr').append("You have been logged out.");    									
    							},
    					},		
 						error: function (xhr, status, error) 
 						{
 								var $me = "<span class='red-text'>No slots for "
 										+ "</span>";
								$('#txr').append($me);
            		},
						success: function( xdata )
						{
							$($div_tm).empty();
					 	   var $div_select = Mustache.to_html($mstg_slot_dropdown, xdata.slots);
					 	   $($div_tm).append($div_select);
					 	   //$("#add_date").append(xdata.idate);
		  					g_choosen_date(xdata.date);
		  					;
						},
				}	
		);
	}
	
};

var $mstg_slot_dropdown = 
							"<div class='row card amber lighten-5'><form  id='form_booking'>"
								+ "<div class='col l6 m6 s12 black-text' >"	
									+ "<input class='xindate datepicker_display_only'  disabled />"
								+ "</div>"
								+ "<div class='col l6 m6 s12'>"
									+ "<select name='aslot' class='browser-default' id='select_time'>"
										+	"<option value=''>Select Time</option>"
											+"{{#.}}"
												+	"<option value='{{.}}' >{{.}}</option>"
											+"{{/.}}"
										+ "</select>"
									+ "</div>"
								+ "<div class='col l12 m12 s12 input-field left-align'>"
										+ "<input name='pax' type='number' id='pax' min='1' /> "
										+ "<label for='pax'>Number of guests</label>"
									+ "</div>"	 
								+ "<div class='col l12 m12 s12 input-field'>"
										+ "<textarea name='comments' class='materialize-textarea' data-length='120' "
											+" id='comments' maxlength='200' ></textarea> "
										+ "<label for='comments'>Comment</label>"								
										+ "</div>"
								+ "<div class='col l12 m12 s12 center'>"
										+ "<a class='btn green' onClick='trg_make_booking()' >Make Reservation</a>  "
										+ "</div>"
								+ "</form>"
								+"</div>"		;					



/*
* FX: trg_make_booking:  Add Reservations
*
* AJAX POST POST
* 
*/
function trg_make_booking()
{
	var $guest_form_div = "#form_booking";

	var $branchid = bgx_get_branchid();	
	var $phone 		= $($guest_form_div+" :input[name='phone']").val();	
	var $comments 	= $($guest_form_div+" :input[name='comments']").val();	
	var $pax 		= $($guest_form_div+" :input[name='pax']").val();
	var $time 		= $($guest_form_div+" :input[name='aslot']").val();	
	var $in_date	= g_choosen_date();
   var $userid		= bgx_get_userid();

   if(!$userid)
   {
   		var $msg = "Please log in to make reservations.";
   		Materialize.toast($msg,3000);
   		return;
   }
   if(!$in_date || !$time)
   {
   		var $msg = "Date and Time are required to make reservations";
   		Materialize.toast($msg,3000);
   		return;
   }
	
	Materialize.toast($in_date,2000);
	
	var $div_derry = $($guest_form_div).find(".xerrmsg");
		
	var $url = "/g/reservediner/book/"+$branchid;

	var $in_fill = {  
											'phone':		$phone,
											'comments': $comments,
											'pax':		$pax,
											'time':		$time,
											'in_date':		$in_date,		
										};	
	
	
	if($pax && $time && $userid)
	{		
		$.ajax( 
					{ 
						url: 			$url ,
						type: 		"POST",
						dataType: 	"json",
						data: 		$in_fill,
 						error: function (xhr, status, error) 
 						{
							var $jr = JSON.parse(xhr.responseText);
						 	var $error_msg = $jr.error;
 							$($div_derry).append($error_msg);
 							Materialize.toast($me,5000);
            		},
						success: function( data )
						{
							Materialize.toast('Added Reservation.', $trv_seconds_refresh)
							trg_display_booking(data, $guest_form_div);
						},
				   }	
		);
		//Ajax
		
	}
	//IF
	
	
}

/*
* FX: trg_display_booking: Display Booking Data to the Guest
*
* @params: data,div
* @name: trg_display_booking
* For Guest
*/

function trg_display_booking(data,$in_div)
{
	var $guest_form_div = "#form_booking";
	var $div_add = $in_div || $guest_form_div;

	var $branchid 		   = data.branchid;	
	var $bookingid 		= data.bookingid;
	var $guest	   		= data.guests;
	var $booking_date 	= data.date;
	var $booking_time 	= data.time;
	var $loc_date_time	= data.date_time;
	var $booking_start 	= data.booking_start;
	var $biz_name			= data.business_name;
	var $str_status   	= data.str_status;
	var $guests				= data.guests;
	var $comments			= data.comments;
	var $biz_url			= data.biz_url;
	var $biz_address		= data.biz_address;
	var $is_future			= data.is_future;
	var $count				= data.count;

	var $row_color = $zebra_a;	
	if(($count % 2) == 0)
	{
		$row_color = $zebra_b;
	}
	
	var $is_editable;
	if($is_future > 0)
	{
			$is_editable = 1;
	}
	
	var $branch_link;
	if($biz_url)
	{
			var $branch_link = "<a class='btn orange center' "
				+" target='_blank' href="+ $biz_url +"> "+ $biz_name +" </a>";
	}
	
	var $str_dt_time;	
	if($booking_start)
	{
			$str_dt_time = $booking_start;
	}
	else
	{
			$str_dt_time = $booking_date + " " + $booking_time;
	}
	
	var $h_bkx;
	$h_bkx = {
					bookingid: 			$bookingid,
					branchid:			$branchid,
					str_date_time:		$str_dt_time,
					loc_date_time:		$loc_date_time,
					str_status:			$str_status,
					biz_name:			$biz_name,	
					guests:				$guests,	
					comments:			$comments,	
					biz_url:				$biz_url,
					biz_address:		$biz_address,
					branch_link:		$branch_link,	
					is_editable:			$is_editable,	
					count		:			$count,
					row_color:			$row_color,
				};

	var $str_x ;
	if($in_div)
	{
		$str_x = Mustache.to_html($mstg_guest_abooking_card, $h_bkx);
	}
	else
	{
		$str_x = Mustache.to_html($mstg_guest_abooking, $h_bkx);		
	}	

	$($div_add).empty();				  
	$($div_add).append($str_x);
	
	return $str_x;
}

/* 
* Used by trg_display_booking
*
*/
var $mstg_guest_abooking_card = "<div class='card'>"
						+ "<div class='card-content row'>"
						 
								+ "<div class='card-title col l12'> "
									+ "<span class='activator material-icons left icon_menu_more'> </span>" 
									+ "Booking ID {{bookingid}}" 
								+ "</div>"
								+ "<div class='col l6 m6 s12'>" 
										+ " {{str_date_time}} " 
								+ "</div>"
								+ "<div class='col l6 m6 s12'>" 
									+ "{{biz_name}}"
								+ "</div>"
								+ "<div class='col l6 m6 s12'>" 
										+ "Status " 
										+ " {{str_status}} " 
								+ "</div>" 
						+ "</div>"
						+ "<div class='card-reveal row'>"
							+ "<div class='card-title col l12 m12 s12'>"
										+ "<span class='close left icon_close'></span>"
										+ "{{bookingid}} "
									+ "</div>"
							+ "<div  class='col l12 m12 s12'>"
								+ "Guests {{guests}}"
								+"</div>"
							+ "<div  class='col l12 m12 s12'>"
								+ "Comments {{comments}}"
								+"</div>"
							+ "<div  class='col l8 m8 s12'>"
								+ "<input id='cancel_reason_{{bookingid}}' placeholder='Reason' > "
								+ "<input id='branch_{{bookingid}}' type='hidden' value='{{branchid}}'> "
								+ "<span class='btn red' onClick=trg_guest_booking_cancel('{{bookingid}}'); "
										+ " >Cancel</span>"
								+"</div>"
						+ "</div>"
						
			+ "</div>";

var $mstg_guest_abooking = "<li>"
					+ "<div class='collapsible-header {{row_color}} '>"
							+ "<div class='row'>"
								+ "<div class='col l1 m1 s1 center tag'> &#10095;</div>"
								+ "<div class='col l5 m6 s10'>" 
										+ "{{loc_date_time}}" 
								+ "</div>"
								+ "<div class='col l6 m5 s12'> "
									+ "@ {{{branch_link}}}" 
								+ "</div>"
								+ "{{#biz_address}}"
								+ "<div class='col l12 m12 s12'>" 
									+ "Address: {{biz_address}}"
								+ "</div>"
								+ "{{/biz_address}}"
								
							+ "</div>"
							+ "<div class='row'>"
								+ "<div  class='col l3 m12 s12'>"
									+ "Guests: {{guests}}"
									+"</div>"
								+ "<div class='col l3 m6 s12'>" 
										+ "{{str_status}} " 
								+ "</div>"
								+ "<div class='col l5 m6 s12'>" 
										+ " Booking ID: {{bookingid}}  "
										+ "<a class='btn-floating orange center-align' "
										+ "href='/g/cart/tablebooking/{{bookingid}}' > &rarr; </a> "
								+ "</div>"
								+ "<div class='col l1 m12 s12 btn white red-text icon_menu_more'>" 
										+ "More " 
								+ "</div>"
								 
							+ "</div>"							
								
					+ "</div>"
					+ "<div class='collapsible-body card-panel {{row_color}}'>"

							+ "<div class='row'>"
								+ "<div class='col l5 m6 s12'>" 
										+ " Booking ID: {{bookingid}}  "
								+ "</div>"

							+ "<div  class='col l12 m12 s12'>"
									+ "Comments {{comments}}"
								+"</div>"
								
							+ "</div>"							
						+ "{{#is_editable}}"
						+ "<div class='row border'>"
							+ "<input id='branch_{{bookingid}}' type='hidden' value='{{branchid}}'> "
							+ "<div  class='col l8 m8 s12 input-field'>"
									+ "<input id='cancel_reason_{{bookingid}}' name='cancel_reason' type='text' > "
									+ "<label for='cancel_reason'>Reason for cancellation</label>"
							+ "</div>"
							+ "<div  class='col l4 m4 s12'>"
								+ "<button class='btn red' "
										+ " onclick=trg_guest_booking_cancel('{{bookingid}}'); "
										+ " >Cancel</button>"
							+ "</div>"	
						+ "</div>"
						+ "{{/is_editable}}"
							
					+ "</div>"
			+ "</li>";



/*
* FX: trg_empty_form
* @name: trg_empty_form: Empty the Reservation Form
* @params: div
*
*/
function trg_empty_form($in_div)
{
	var $guest_form_div_top = "#reserve_status";
	var $div_x = $in_div || $guest_form_div_top;
	
	$($div_x).empty();

}


/*
* FX: trg_guest_booking_cancel: Cancellation Booking by Guest
*
*
*/
function trg_guest_booking_cancel($bookingid)
{
	
	var $branchid = $("#branch_"+$bookingid).val();
	//Materialize.toast($branchid,5000);
	
	var $reason = $("#cancel_reason_"+$bookingid).val();
	var $base_url = "/g/reservediner/book/"+$branchid+"/"+$bookingid;

	var $h_dx = {comments:$reason};
	var $pageid =  $("#current_page").val() || 1;

	if($branchid && $bookingid && $reason)
	{		
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"DELETE",
						dataType: 	"json",
						data: $h_dx,
						statusCode: {
    								404: function() {
									$('#txr').append("You have been logged out.");    									
    							},
    					},		
 						error: function (xhr, status, error) 
 						{
 								var $me = "<span class='red-text'>No changes."
 										+ "</span>";
								Materialize.toast($me,5000); 										
            		},
						success: function( xdata )
						{
							var $msg = "Your cancellation request has been accepted";	
							Materialize.toast($msg,5000);	
							trg_guest_bookings($pageid);
						},
				}	
		);
	}	
	else if(!$reason)
	{
		Materialize.toast("Reason is required for cancellation",4000);	
	}
}

/*
* FX: trg_guest_bookings: Display Bookings for Guest
*
* Handles Pagination throug Data and Moustache
*
*/
function trg_guest_bookings($inpage,$in_url)
{

	var $page =  $inpage || 1;

	var $in_date  = g_choosen_date();
		
	var $div_tm = "#list_bookings";

	var $div_id_msg = "Reservations are being fetched....";
	
	var $base_url;
	if($in_url)
	{
		$base_url = $in_url;
		$in_date = "";
	}	
	else
	{
		var $base_url = '/g/reservediner/list' + "/" + $page; 
	}

	progress_append($div_tm);

	if($base_url)
	{		
		$.ajax( 
					{ 
						url: 			$base_url ,
						type: 		"GET",
						dataType: 	"json",
 						error: function (xhr, status, error) 
 						{
 							
 								var $me = "<span class='red-text'>No table reservations "
 										+ "</span>";
								$($div_tm).empty(); 										
								$($div_tm).append($me);
            		},
						success: function( xdata )
						{
								$($div_tm).empty();
								trg_all_bookings(xdata,$div_tm);
						},
				}	
		);
	}
	
};

/*
* FX: trg_all_bookings: Foreach Loop through All the bookings.
*
* Has next and previous for pgx.
*
*/
function trg_all_bookings(xdata,$in_div)
{
	   var items 			 = [];
  
  		//Pgx part
  		var $div_pgx = "#list_bookings_top";
		var $h_pgx = {
											next:			xdata.next,
											previous:	xdata.previous,
											page:			xdata.page,
						};
										
 	   var $xmethod = 		"trg_guest_bookings";
		var $str_pgx = 		bgx_page_nf_pg($h_pgx,$xmethod);
		$($div_pgx).empty();
	   $($div_pgx).append($str_pgx);	
	  
	  var $count = 0;	  
	  $.each( xdata.items, function( key, val ) 
	  {
	  		 val.count = $count;	
			 var $x_item =	trg_display_booking(val);
	  		 items.push($x_item);
	  		 $count++;
  	  }) 


  $( "<ul/>", 
  {
    "class": "collapsible accordion",
    html: items.join( "" )
  }).appendTo( $in_div ); 

  $(document).ready(function(){
    $('.collapsible').collapsible({
      accordion : false 
    });
  });
        

	
}

/*
*  
* this has Pagination through data.next and data.previous 
*/
var $mst_gbookings_pgx = 	"<li><div class='card row yellow lighten-3'>"
						+ "<li><div class='row'>"
							+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=trg_guest_bookings('1','{{previous}}');"							
									+ "> "
									+ "<< </span>"
							+ "<span class='btn col l4 m4 s6 red'>{{page}}</span>"
							+ "<input id='current_page' value='{{page}}' type='hidden' />"
							+ "<span class='btn col l4 m4 s3 white black-text' "
									+ 	"onClick=trg_guest_bookings('1','{{next}}');" 	 
									+ "> "
									+ ">> </span>"
	  						+ "</div></li>"
	  				;
	  				
	  				
	  				
/*******End Guest Bookings List**********/
